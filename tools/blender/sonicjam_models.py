import bpy
import json

from bpy_extras.io_utils import ImportHelper
from bpy.props import StringProperty
from bpy.types import Operator

def convert_index(idx):
    i = idx if idx >= 0 else -i - 8
    return int((i /2 + i) / 12 + 1)


def import_mesh(collection, model_json):
    name = model_json["name"]

    mesh = bpy.data.meshes.new(name)
    verts = [(v[0], -v[1], v[2]) for v in model_json["verts"]]
    faces = []

    for f in model_json["faces"]:
        a = f[0]
        b = f[1]
        c = f[2]
        d = f[3]

        face = (a, b, c, d)
        if a < 0:
            face = ( (-a - 8), b, c, d )
        if c < 0:
            face = (a, b, (-c - 8), d)

        face = [ int(i / 8) for i in face ]
        faces.append(face)

    mesh.from_pydata(verts, [], faces)

    #if not mesh.validate(verbose=True):
    #    print("Invalid mesh data {}".format(name))
    #    return

    obj = bpy.data.objects.new(name, mesh)
    collection.objects.link(obj)


def apply_positions(collection, positions):
    for i, p in enumerate(positions):
        obj = collection.objects[i]
        print(obj.location)
        print(p)
        obj.location.x += p[0] * 5
        obj.location.y += p[1] * 5
        obj.location.z += p[2] * 5
        print(obj.location)


class ImportSonicJam(Operator, ImportHelper):
    """Importer for Sonic Jam models"""

    bl_idname = "import.sonic_jam"
    bl_label = "Import Sonic Jam Model"

    filename_ext = ".json"
    filter_glob: StringProperty(
        default = "*.json",
        options = {'HIDDEN'},
        maxlen = 255
    )


    def execute(self, context):
        file = open(self.filepath, 'r')
        content = json.load(file)

        collection = bpy.data.collections.new(content["name"])
        context.scene.collection.children.link(collection)

        for model in content["models"]:
            import_mesh(collection, model)

        apply_positions(collection, content["positions"])
        file.close()

        return {'FINISHED'}


def menu_func_import(self, context):
    self.layout.operator(ImportSonicJam.bl_idname, text="Import Sonic Jam model")


def register():
    bpy.utils.register_class(ImportSonicJam)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    pass


def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    bpy.utils.unregister_class(ImportSonicJam)
    pass


if __name__ == '__main__':
    register()


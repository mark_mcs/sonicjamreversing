#!/bin/bash

MK_FILE=model_objects.mk
SCRIPT_DIR=$(dirname $(realpath $0))

index_files=()
echo -n "MODEL_OBJS =" > ${MK_FILE}

list_arg=""
if [[ "$1" == "list" ]]; then
    shift
    list_arg="--model-count=$1"
    shift
fi

exp_type_arg="-t asm"
if [[ "$1" == "obj" ]]; then
    exp_type_arg="-t obj"
    shift
elif [[ "$1" == "c" ]]; then
    exp_type_arg="-t c"
    shift
fi

vertex_type_arg=""
model_type="xpdata"
name=""
name_arg=""
positions_arg=""

while (("$#")); do
    if [[ "$1" == "count" ]]; then
        shift
        list_arg="--model-count=$1"
    elif [[ "$1" == "count_off" ]]; then
        list_arg=""
    elif [[ "$1" == "xpdata" ]] || [[ "$1" == "pdata" ]]; then
        model_type=$1
    elif [[ "$1" == "short_vertex" ]]; then
        vertex_type_arg="--vertex-type=short"
    elif [[ "$1" == "long_vertex" ]]; then
        vertex_type_arg=""
    elif [[ "$1" == "name" ]]; then
        shift
        name=$1
        name_arg="-n $1"
    elif [[ "$1" == "positions_on" ]]; then
        positions_arg="-p"
    elif [[ "$1" == "positions_off" ]]; then
        positions_arg=""
    else
        addr=$1
        addr_upper=$(echo $addr | tr '[:lower:]' '[:upper:]')
        index_files+=(${addr_upper})

        echo "Dump address ${addr}"

        python3 $SCRIPT_DIR/extract_model.py -f $SCRIPT_DIR/../../../SonicJam/museum.mus \
            -m ${model_type} \
            -a 0x$1 \
            ${exp_type_arg} \
            ${positions_arg} \
            ${vertex_type_arg} \
            ${name_arg} \
            ${list_arg}

        echo " \\" >> ${MK_FILE}
        if [[ $name != "" ]]; then
            echo -n "    \$(BUILD_DIR)/obj/models/${name}.o" >> ${MK_FILE}
        else
            echo -n "    \$(BUILD_DIR)/obj/models/model_0x${addr}.o" >> ${MK_FILE}
        fi
        name_arg=""
        name=""
    fi

    shift
done

#echo >> "${INDEX_FILE}"
#echo "MODEL_LIST_${addr_upper}:" >> ${INDEX_FILE}
#for a in ${index_files[@]}; do
#    echo "    .long MODEL_${a}_model" >> ${INDEX_FILE}
#done

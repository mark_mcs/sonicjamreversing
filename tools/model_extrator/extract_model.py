import argparse
import json
import os
import struct
import sys

from constants import *

import model
from writers.cwriter import CModelWriter
from writers.objwriter import ObjModelWriter
from writers.asmwriter import AsmModelWriter
from writers.jsonwriter import JsonModelWriter


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--file', type=argparse.FileType('rb'), required = True)
    parser.add_argument('-m', '--model-type', choices=['xpdata', 'pdata'], required = True)
    parser.add_argument('-a', '--address', required = True)
    parser.add_argument('-o', '--out')
    parser.add_argument('-t', '--output-type', choices=['json', 'asm', 'obj', 'c'], default='asm')
    parser.add_argument('-c', '--model-count', type=int)
    parser.add_argument('-n', '--name')
    parser.add_argument('-p', '--with-positions', action='store_true')
    parser.add_argument('--vertex-type', choices=['short','long'], default='long')
    return parser.parse_args()


class dump_params:
    def __init__(self):
        self.short_vertices = False
        self.has_normals = False
        self.model_count = 0
        self.has_position_data = False


def read_list_of_model_pointers(buf, offs, count):
    ptrs = []
    end = offs + 4 * count
    while offs < end:
        p = struct.unpack_from('>I', buf, offs)[0]
        p -= LOAD_ADDRESS
        ptrs.append(p)
        offs += 4
    return ptrs


def dump_model(buf, offs, params, writer):
    m = model.ModelData.from_buffer(buf, offs, params)
    writer.write_model(m, params)


def dump_model_group(buf, offset, params, writer):
    base_address = offset + LOAD_ADDRESS
    ptrs = read_list_of_model_pointers(buf, offset, params.model_count)
    last_model_ptr = 0

    writer.begin_model_group()
    for p in ptrs:
        last_model_ptr = p if p > last_model_ptr else last_model_ptr
        dump_model(buf, p, params, writer)

    # if there's a gap between the last model and the pointer list we should
    # preserve that data some how
    last_model_ptr += 24 if params.has_normals else 20

    if last_model_ptr < offset:
        start = last_model_ptr
        writer.write_raw_data(buf[start:offset])

    writer.end_model_group()
    # if the group includes positioning vectors dump that here
    if params.has_position_data:
        # move past the model pointers
        offset = offset + 4 * params.model_count
        positions = []
        for i in range(0, params.model_count):
            x = struct.unpack_from('>i', buf, offset)[0] / 65536
            y = struct.unpack_from('>i', buf, offset + 4)[0] / 65536
            z = struct.unpack_from('>i', buf, offset + 8)[0] / 65536
            positions.append(model.Vec3D(x, y, z))
            offset += 12
        writer.write_model_positions(positions)



if __name__ == '__main__':
    args = parse_args()

    buf = args.file.read()
    src_address = int(args.address, 0) - LOAD_ADDRESS

    label = 'model_{}'.format(args.address)
    if args.name != None:
        label = args.name

    writer = None
    if args.output_type == 'asm':
        writer = AsmModelWriter(label, label)
    elif args.output_type == 'obj':
        writer = ObjModelWriter(label)
    elif args.output_type == 'c':
        writer = CModelWriter(label, label)
    elif args.output_type == 'json':
        writer = JsonModelWriter(label, label)
    else:
        sys.exit()

    params = dump_params()
    params.short_vertices = args.vertex_type == 'short'
    params.has_normals = args.model_type == 'xpdata'
    params.has_position_data = args.with_positions

    writer.begin_write()
    if args.model_count != None:
        params.model_count = args.model_count
        dump_model_group(buf, src_address, params, writer)
    else:
        dump_model(buf, src_address, params, writer)

    writer.end_write()


import struct

from constants import *

class Vec3D:
    def __init__(self, x, y, z, is_short=False):
        self.x = x
        self.y = y
        self.z = z
        self.is_short = is_short


class Polygon:
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d


class PolygonAttrs:
    def __init__(self):
        self.flags = 0
        self.sort = 0
        self.char_pattern = 0
        self.cmdpmod = 0
        self.colour = 0
        self.gouraud_table = 0
        self.cmdctrl = 0
        self.field7 = 0
        self.field8 = 0

    def from_buffer(buf, offset):
        attrs = PolygonAttrs()
        attrs.flags = buf[offset]
        attrs.sort = buf[offset + 1]
        attrs.char_pattern = struct.unpack_from('>H', buf, offset + 2)[0]
        attrs.cmdpmod = struct.unpack_from('>H', buf, offset + 4)[0]
        attrs.colour = struct.unpack_from('>H', buf, offset + 6)[0]
        attrs.gouraud_table = struct.unpack_from('>H', buf, offset + 8)[0]
        attrs.cmdctrl = struct.unpack_from('>H', buf, offset + 10)[0]
        attrs.field7 = struct.unpack_from('>H', buf, offset + 12)[0]
        attrs.field8 = struct.unpack_from('>H', buf, offset + 14)[0]
        return attrs


class ModelData:
    def __init__(self):
        self.start_address = 0
        self.vertex_list = []
        self.polygon_list = []
        self.attrs_list = []
        self.normals_list = []

    def from_buffer(buf, offset, params):
        lowest_address = 0xFFFFFFFF

        model = ModelData()

        vertex_ptr = struct.unpack_from('>I', buf, offset)[0]
        vertex_count = struct.unpack_from('>I', buf, offset + 4)[0]
        if params.short_vertices:
            model.vertex_list = ModelData.read_vec3d_short_list(buf, vertex_ptr - LOAD_ADDRESS, vertex_count)
        else:
            model.vertex_list = ModelData.read_vec3d_list(buf, vertex_ptr - LOAD_ADDRESS, vertex_count)

        polygon_ptr = struct.unpack_from('>I', buf, offset + 8)[0]
        polygon_count = struct.unpack_from('>I', buf, offset + 12)[0]
        model.polygon_list = ModelData.read_polygon_list(buf, polygon_ptr - LOAD_ADDRESS, polygon_count)

        attrs_ptr = struct.unpack_from('>I', buf, offset + 16)[0]
        if attrs_ptr != 0:
            model.attrs_list =  ModelData.read_attrs_list(buf, attrs_ptr - LOAD_ADDRESS, polygon_count)

        lowest_address = min(vertex_ptr, polygon_ptr, attrs_ptr)

        if params.has_normals:
            normals_ptr = struct.unpack_from('>I', buf, offset + 20)[0]
            model.normals_list = ModelData.read_vec3d_list(buf, normals_ptr - LOAD_ADDRESS, vertex_count)
            lowest_address = min(lowest_address, normals_ptr)

        model.start_address = lowest_address
        return model


    def read_vec3d_short_list(buf, offset, count):
        vecs = []
        end = offset + 2 * 3 * count
        while offset < end:
            x = struct.unpack_from('>h', buf, offset)[0]
            y = struct.unpack_from('>h', buf, offset + 2)[0]
            z = struct.unpack_from('>h', buf, offset + 4)[0]
            vecs.append(Vec3D(x, y, z))
            offset += 2 * 3
        return vecs


    def read_vec3d_list(buf, offset, count):
        vecs = []
        end = offset + 4 * 3 * count
        while offset < end:
            x = struct.unpack_from('>i', buf, offset)[0]
            y = struct.unpack_from('>i', buf, offset + 4)[0]
            z = struct.unpack_from('>i', buf, offset + 8)[0]
            vecs.append(Vec3D(x, y, z))
            offset += 4 * 3
        return vecs


    def read_polygon_list(buf, offset, count):
        polys = []
        end = offset + 2 * 4 * count
        while offset < end:
            a = struct.unpack_from('>h', buf, offset)[0]
            b = struct.unpack_from('>h', buf, offset + 2)[0]
            c = struct.unpack_from('>h', buf, offset + 4)[0]
            d = struct.unpack_from('>h', buf, offset + 6)[0]
            polys.append(Polygon(a, b, c, d))
            offset += 2 * 4
        return polys

    def read_attrs_list(buf, offset, count):
        attrs = []
        end = offset + 16 * count
        while (offset < end):
            attrs.append(PolygonAttrs.from_buffer(buf, offset))
            offset += 16
        return attrs


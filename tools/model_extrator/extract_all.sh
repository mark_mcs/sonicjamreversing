#!/bin/bash

SCRIPT_DIR=$(dirname $(realpath $0))

${SCRIPT_DIR}/extract_model_list.sh \
    c \
    positions_on \
    count 15 \
    name model_sonic_00 \
    06082680 \
    name model_sonic_01 \
    06085488 \
    name model_sonic_02 \
    06088290 \
    count 1 \
    name model_sonic_jumping \
    06089da8 \
    count_off \
    short_vertex \
    pdata \
    name model_tails_billboard \
    06089de8 \
    xpdata \
    count 15 \
    long_vertex \
    name model_tails \
    0608d37c \
    name model_tails_low_detail \
    0608e214 \
    count_off \
    pdata \
    name model_arch \
    0608ea5c \
    count 2 \
    name model_baru \
    0608eedc \
    count 21 \
    0608f7b4 \
    count 2 \
    short_vertex \
    name model_check \
    0608f9ec \
    count 1 \
    long_vertex \
    name model_exit \
    0608fe60 \
    count 2 \
    0608ff3c \
    short_vertex \
    count_off \
    name model_ring_shadow \
    0608ffa8 \
    count 2 \
    name model_palmtree \
    060903ac \
    name model_ki \
    06090754 \
    long_vertex \
    name model_bane \
    06090ba4 \
    short_vertex \
    count_off \
    06090e1c \
    06090e90 \
    count 2 \
    name model_totem2 \
    06091190 \
    long_vertex \
    count 1 \
    name model_f_iwa \
    06091544 \
    count 2 \
    name model_mission_platform \
    060918e8 \
    short_vertex \
    count 3 \
    name model_monitor \
    06091ccc \
    long_vertex \
    count 1 \
    name model_pol1 \
    06091f40 \
    name model_pol2 \
    06092194 \
    xpdata \
    name model_big_ring \
    0609303c \
    pdata \
    name model_kamome \
    06093284 \
    count 10 \
    name model_flickyb \
    060938e0 \
    name model_flickyg \
    06093c24 \
    name model_flickyp \
    06093f68 \
    name model_flickyr \
    060942ac \
    name model_flickyy \
    060945f0 \
    name model_monkey \
    06094d60 \
    count 3 \
    name model_sound \
    06095c74 \
    count 1 \
    name model_sound_in \
    06095d98 \
    count 5 \
    name model_gallery \
    06096ba4 \
    count 1 \
    name model_gall_in_group \
    06096ce8 \
    count 3 \
    name model_history \
    06097b90 \
    count 1 \
    name model_hist_in \
    06097cb4 \
    count 5 \
    name model_chara \
    06098a78 \
    count 2 \
    06098b94 \
    06098dac \
    count 3 \
    name model_movie \
    06099b50 \
    count 1 \
    name model_movie_in \
    06099c74 \
    0609a0f0 \
    0609a500 \
    0609abec \
    0609b26c \
    0609b664 \
    0609ba50 \
    0609bec0 \
    0609c2d0 \
    0609c5c0 \
    0609caa8 \
    0609ce64 \
    0609d4e4 \
    0609d9c0 \
    0609e070 \
    0609e360 \
    0609e5a8 \
    0609e79c \
    0609ec6c \
    0609f220 \
    0609f750 \
    0609fbf0 \
    0609fd90 \
    0609ffd8 \
    060a00d0 \
    060a057c \
    060a0908 \
    060a0ed4 \
    060a156c \
    060a197c \
    060a1b94 \
    count_off \
    060a1d9c \
    060a208c \
    060a2340 \
    060a261c \
    060a298c \
    060a2c60 \
    060a2efc \
    060a3344 \
    060a36ec \
    060a390c \
    060a3b58 \
    count 1 \
    060a3dd4 \
    060a43dc \
    060a454c \
    060a48fc \
    060a4e50 \
    060a4fb4 \
    060a5214 \
    060a55e8 \
    060a5c44 \
    060a6174 \
    060a66c8 \
    060a6ca0 \
    060a72e4 \
    060a7790 \
    060a7eac \
    060a83c4 \
    060a87e0 \
    060a8ba8 \
    060a8e68 \
    060a93f8 \
    060a9808 \
    060a9c6c \
    060aa0d0 \
    060aa630 \
    060aa998 \
    060aad54 \
    060aaf9c \
    060ab328 \
    060ab5e8 \
    060aca0c \
    060ad2f0 \
    060ad484 \
    060adbdc \
    060adf5c \
    060ae570 \
    count_off \
    model rope_bridge \
    060aec4c \
    060aecd8 \
    060af220 \
    060af834 \
    060afe34 \
    060afe34 \
    060b03f4 \
    count 1 \
    name model_card \
    060b06c4

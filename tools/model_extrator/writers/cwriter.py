from writers.modelwriter import ModelWriter

class CModelWriter(ModelWriter):

    def __init__(self, filename, label_prefix):
        self._header_file = None
        self._source_file = None
        self._filename = filename
        self._label_prefix = label_prefix
        self._model_var_type = 'xmodel'
        self._model_var_names = []
        self._data_count = 0

    def begin_write(self):
        self._header_file = open('{}.h'.format(self._filename), 'w')
        self._source_file = open('{}.c'.format(self._filename), 'w')
        self._header_file.write('#pragma once\n\n')
        self._header_file.write('#include "geometry.h"\n\n')
        self._source_file.write('#include "{}.h"\n\n'.format(self._filename))


    def end_write(self):
        self._header_file.close()
        self._source_file.close()


    def end_model_group(self):
        buf = '\n{} * {}_group[] = {{'.format(self._model_var_type, self._label_prefix)
        for v in self._model_var_names:
            buf += '\n    &{},'.format(v)
        buf = buf[0:len(buf)-1]
        buf += '\n};'
        self._source_file.write(buf)

        self._header_file.write('\nextern {} * {}_group[];'.format( \
                self._model_var_type, self._label_prefix))


    def write_raw_data(self, databuf):
        buf = '\nchar {}_data_{:02x}[] = {{'.format(self._label_prefix, self._data_count)
        chunks = len(databuf) // 8
        for i in range(0, chunks):
            buf += '\n    0x{:02x}, 0x{:02x}, 0x{:02x}, 0x{:02x}, 0x{:02x}, 0x{:02x}, 0x{:02x}, 0x{:02x},'.format(\
                    databuf[i*8],   databuf[i*8+1], databuf[i*8+2], databuf[i*8+3], \
                    databuf[i*8+4], databuf[i*8+5], databuf[i*8+6], databuf[i*8+7])
        rest = chunks * 8
        for i in range(rest, len(databuf)):
            buf += '\n    0x{:02x},'.format(databuf[i])

        buf += '\n};'
        self._source_file.write(buf);
        self._header_file.write('\nextern char {}_data_{:02x}[];'.format(self._label_prefix, self._data_count))
        self._data_count += 1


    def write_model_positions(self, positions):
        buf = '\n\nvec3 {}_positions[] = {{'.format(self._label_prefix)
        for p in positions:
            buf += '\n    POS_TO_FIXED({}, {}, {}),'.format(p.x, p.y, p.z)
        buf += '\n};'
        self._source_file.write(buf);
        self._header_file.write('\nextern vec3 {}_positions[];'.format(self._label_prefix))


    def write_model(self, model, params):
        has_attrs = len(model.attrs_list) > 0
        count = len(self._model_var_names)
        prefix = '{}_{:02}'.format(self._label_prefix, count)
        buf = ''

        if params.short_vertices:
            buf = self._write_vec3s_list(buf, '{}_verts'.format(prefix), model.vertex_list)
        else:
            buf = self._write_vec3_list(buf, '{}_verts'.format(prefix), model.vertex_list)

        buf += 'polygon {}_faces[] = {{'.format(prefix)
        for f in model.polygon_list:
            buf += '\n    {{ {}, {}, {}, {} }},'.format(f.a, f.b, f.c, f.d)
        buf = buf[0:len(buf)-1]
        buf += '};\n\n'

        if params.has_normals:
            buf = self._write_vec3_list(buf, '{}_normals'.format(prefix), model.normals_list)

        if has_attrs:
            buf += 'polygon_attr {}_attrs[] = {{'.format(prefix)
            for a in model.attrs_list:
                buf += ('\n    {{ 0x{:02x}, 0x{:02x}, 0x{:04x}, 0x{:04x}, ' + \
                       '0x{:04x}, 0x{:04x}, 0x{:04x}, 0x{:04x}, 0x{:04x} }},') \
                        .format(a.flags, a.sort, a.char_pattern, \
                            a.cmdpmod, a.colour, a.gouraud_table, \
                            a.cmdctrl, a.field7, a.field8)
            buf = buf[0:len(buf)-1]
            buf += '\n};\n\n'

        buf = self._write_model_var(buf, model, params.short_vertices, count)
        self._source_file.write(buf)


    def _write_model_var(self, buf, model, short_vertices, count):
        if short_vertices:
            self._model_var_type = 'model16'
        elif len(model.normals_list) > 0:
            self._model_var_type = 'xmodel'
        else:
            self._model_var_type = 'model'

        prefix = '{}_{:02}'.format(self._label_prefix, count)
        var_name = '{}_model_{:02}'.format(self._label_prefix, len(self._model_var_names))
        self._model_var_names.append(var_name)

        self._header_file.write('extern {} {};\n'.format(self._model_var_type, var_name))

        buf += '{} {} = {{\n'.format(self._model_var_type, var_name)
        buf += '    {}_verts,\n'.format(prefix)
        buf += '    {},\n'.format(len(model.vertex_list))
        buf += '    {}_faces,\n'.format(prefix)
        buf += '    {},\n'.format(len(model.polygon_list))
        if len(model.attrs_list) > 0:
            buf += '    {}_attrs'.format(prefix)
        else:
            buf += '    NULL'

        if len(model.normals_list) > 0:
            buf += ',\n    {}_normals'.format(prefix)
        buf += '\n};\n\n'
        return buf


    def _write_vec3_list(self, buf, var_name, vtx_list):
        buf += 'vec3 {}[] = {{'.format(var_name)
        for v in vtx_list:
            x = v.x / 65536
            y = v.y / 65536
            z = v.z / 65536
            buf += '\n    POS_TO_FIXED({}, {}, {}),'.format(x, y, z)
        buf = buf[0:len(buf)-1]
        buf += '\n};\n\n'
        return buf


    def _write_vec3s_list(self, buf, var_name, vtx_list):
        buf += 'vec3s {}[] = {{'.format(var_name)
        for v in vtx_list:
            x = (v.x + 0x10000) & 0xFFFF
            y = (v.y + 0x10000) & 0xFFFF
            z = (v.z + 0x10000) & 0xFFFF
            buf += '\n    {{ 0x{:04x}, 0x{:04x}, 0x{:04x} }},'.format(x, y, z)
        buf = buf[0:len(buf)-1]
        buf += '\n};\n\n'
        return buf



from writers.modelwriter import ModelWriter

class ObjModelWriter(ModelWriter):

    def __init__(self, filename):
        self._filename = filename
        self._count = 0

    def begin_model_group(self):
        pass

    def end_model_group(self):
        pass

    def write_model(self, model, params):
        f = open('{}_{:02d}.obj'.format(self._filename, self._count), 'w')

        for v in model.vertex_list:
            f.write('v {} {} {}\n'.format(\
                    self._convert_fixed(v.x), \
                    self._convert_fixed(v.y), \
                    self._convert_fixed(v.z)))

        for p in model.polygon_list:
            f.write('f {} {} {} {}\n'.format(\
                    self._convert_index(p.a), \
                    self._convert_index(p.b), \
                    self._convert_index(p.c), \
                    self._convert_index(p.d)))
        self._count = self._count + 1
        f.close()


    def _convert_index(self, i):
        i = i if i >= 0 else -i -8
        return int((i / 2 + i) / 12 + 1)

    def _convert_fixed(self, f):
        return f/65536.0



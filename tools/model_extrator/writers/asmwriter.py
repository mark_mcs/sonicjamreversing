from writers.modelwriter import ModelWriter

def AsmModelWriter(ModelWriter):

    def __init__(self, filename, label_prefix):
        self._filename = filename
        self._label_prefix = label_prefix
        self._index_file = None
        self._label_list = []


    def begin_model_group(self):
        self._file = open('{}.S'.format(self._filename), 'w')


    def end_model_group(self):
        self._index_file.write('    .long 0x0000FFFF\n')
        self._index_file.write('    .long 0xFFFEFFFF\n\n')
        self._index_file.write('{}_model_group:\n'.format(self._label_prefix))
        for l in self._label_list:
            self._index_file.write('    .long "{}"\n'.format(l))
        self._index_file.write('    .long 0x00000000\n')
        self._index_file.write('    .long 0x00000000\n')
        self._index_file.write('    .long 0x00000000\n')
        self._index_file.close()
    

    def write_model(self, model, params):
        f = self._file
        prefix = self._label_prefix

        f.write('.org 0x{:08x}\n'.format(model.start_address))
        f.write('{}_verts:\n'.format(prefix))
        for v in model.vertex_list:
            x = (v.x + (1<<32)) & 0xFFFFFFFF
            y = (v.y + (1<<32)) & 0xFFFFFFFF
            z = (v.x + (1<<32)) & 0xFFFFFFFF
            f.write('    .long 0x{:08x}, 0x{:08x}, 0x{:08x}\n'.format(x, y, z))

        f.write('{}_polys:\n'.format(prefix))
        for p in model.polygon_list:
            a = (p.a + (1<<16)) & 0xFFFF
            b = (p.b + (1<<16)) & 0xFFFF
            c = (p.c + (1<<16)) & 0xFFFF
            d = (p.d + (1<<16)) & 0xFFFF
            f.write('    .word 0x{:04x}, 0x{:04x}, 0x{:04x}, 0x{:04x}\n' \
                    .format(a, b, c, d))

        if len(model.normals_list) > 0:
            f.write('{}_normals:\n'.format(prefix))
            for n in model.normals_list:
                x = (v.x + (1<<32)) & 0xFFFFFFFF
                y = (v.y + (1<<32)) & 0xFFFFFFFF
                z = (v.x + (1<<32)) & 0xFFFFFFFF
                f.write('    .long 0x{:08x}, 0x{:08x}, 0x{:08x}\n'.format(x, y, z))

        f.write('{}_attrs:\n'.format(prefix))
        for a in model.attrs_list:
            f.write('    .byte 0x{:02x}\n'.format(a.flags))
            f.write('      .byte 0x{:02x}\n'.format(a.sort))
            f.write('      .word 0x{:04x}\n'.format(a.char_pattern))
            f.write('      .word 0x{:04x}\n'.format(a.cmdpmod))
            f.write('      .word 0x{:04x}\n'.format(a.colour))
            f.write('      .word 0x{:04x}\n'.format(a.gouraud_table))
            f.write('      .word 0x{:04x}\n'.format(a.cmdctrl))
            f.write('      .word 0x{:04x}\n'.format(a.field7))
            f.write('      .word 0x{:04x}\n'.format(a.field8))

        f.write('{}_model:\n'.format(prefix))
        f.write('    .long {}_verts\n'.format(prefix))
        f.write('    .long {}\n'.format(len(model.vertex_list)))
        f.write('    .long {}_polys\n'.format(prefix))
        f.write('    .long {}\n'.format(len(model.polygon_list)))
        f.write('    .long {}_attr\n'.format(prefix))
        if len(model.normals_list) > 0:
            f.write('    .long {}_normals\n'.format(prefix))

        self._label_list.append('{}_model'.format(prefix))



from writers.modelwriter import ModelWriter
import json

class JsonModelWriter(ModelWriter):

    def __init__(self, filename, label_prefix):
        self._filename = filename
        self._label_prefix = label_prefix
        self._indent_step = 0
        self._indent_str = ''
        self._model_count = 0
        self._file = open('{}.json'.format(filename), 'w')


    def begin_write(self):
        self._writeln('{')
        self._indent(1)
        self._writeln('"name": "{}",'.format(self._label_prefix))


    def end_write(self):
        self._indent(-1)
        self._writeln('}')
        self._file.close()

    
    def begin_model_group(self):
        self._writeln('"models": [')
        self._indent(1)


    def end_model_group(self):
        self._indent(-1)
        self._writeln(']')


    def write_model(self, model, params):
        if self._model_count > 0: self._writeln(',')

        self._writeln('{')
        self._indent(1)

        self._writeln('"name": "model_{}",'.format(self._model_count))
        self._writeln('"verts": [')
        self._indent(1)
        self._write_vec3_list(model.vertex_list)
        self._indent(-1)
        self._writeln('],')

        self._writeln('"faces": [')
        self._indent(1)
        faces = ['{}[{}, {}, {}, {}]'.format(self._indent_str, f.a, f.b, f.c, f.d) for f in model.polygon_list]
        self._file.write(',\n'.join(faces))
        self._file.write('\n')
        self._indent(-1)
        self._writeln(']')

        self._indent(-1)
        self._writeln('}')

        self._model_count += 1


    def write_model_positions(self, positions):
        self._writeln(',')
        self._writeln('"positions": [')
        self._indent(1)

        ps = ['{}[{}, {}, {}]'.format(self._indent_str, p.x, p.y, p.z) for p in positions]
        self._file.write(',\n'.join(ps))
        self._file.write('\n')
        self._indent(-1)
        self._writeln(']')


    def _indent(self, step):
        self._indent_step += step
        self._indent_str = '    ' * self._indent_step

    
    def _writeln(self, line):
        self._file.write('{}{}\n'.format(self._indent_str, line))


    def _write_vec3_list(self, verts):
        vert_list = [ '{}[ {}, {}, {} ]'.format(self._indent_str, v.x / 65536, v.y / 65536, v.z / 65536)
                     for v in verts ]
        self._file.write(',\n'.join(vert_list))
        self._file.write('\n')



class ModelWriter:
    """
    Interface that should be implemented by objects that want to transform and
    export model data that is extracted from the Sonic Jam data files.
    """

    def begin_model_group(self) -> None:
        """
        Called when the extractor encounters the start of a model group
        structure (i.e. a list of model pointers).
        """
        pass

    def end_model_group(self) -> None:
        """
        Called when the extractor has processed all of the model pointers in a group
        """
        pass

    def write_model(self, model, params) -> None:
        """
        Called for each model that the extractor finds. The params object
        contains flags or options that may be used by the writer to change the
        way it exports the data. E.g. the writer may want to print 16-bit verts
        if the 'short_vertices' option was specified.
        """
        pass
    
    def write_model_positions(self, positions) -> None:
        """
        Called when the extractor encounters a set of model poisitioning verts.
        """
        pass

    def write_raw_data(self, databuf) -> None:
        """
        Called when the extractor encounters some unknown binary data that may
        need to be preserved in order to maintain the extact binary structure of
        the original source data.
        """
        pass

    def begin_write(self) -> None:
        """
        Called at the start of processing, before the extractor has started
        reading data.
        """
        pass

    def end_write(self) -> None:
        """
        Called as the final action of the extractor once it has  reached the end
        of its processing.
        """
        pass



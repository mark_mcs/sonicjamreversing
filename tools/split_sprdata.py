import struct
import os

texture_table = 0x06072a24 - 0x6040000

sprdata_file = '../../SonicJam/sprdata.mus'
sprdata_base = 0xd040

sonicworld_file = '../../SonicJam/museum.mus'
texture_list_base = 0x25104
texture_list_entry_count = 475

texture_entry_def = '>HHHH'
texture_entry_size = struct.calcsize(texture_entry_def)

class Range:
    def __init__(self, start, end, range_type):
        self.start = start
        self.end = end
        self.range_type = range_type


class AddressRange:
    def __init__(self, start, end):
        self.ranges = [
            Range(start, end, 'unknown')
        ]

    def classify(self, start, end, range_type):
        for i, r in enumerate(self.ranges):
            if r.start == start:
                old_end = r.end

                r.range_type = range_type
                r.end = end

                if end != old_end:
                    self.ranges.insert(i+1, Range(end, old_end, 'unknown'))
                return

            elif r.start < start and r.end >= end:
                old_end = r.end
                r.end = start

                self.ranges.insert(i+1, Range(start, end, range_type))
                if end != old_end:
                    self.ranges.insert(i+2, Range(end, old_end, 'unknown'))
                return

        print("error classifying range: {:x} {:x}".format(start, end))



class texture_definition:
    def __init__(self):
        self.cmdsrca = 0
        self.cmdsize = 0
        self.cmdcolr = 0
        self.field3 = 0
        self.extracted = False

    def from_file(file):
        tex = texture_definition()
        buf = file.read(8)
        tex.cmdsrca = struct.unpack_from('>H', buf, 0)[0]
        tex.cmdsize = struct.unpack_from('>H', buf, 2)[0]
        tex.cmdcolr = struct.unpack_from('>H', buf, 4)[0]
        tex.field3 = struct.unpack_from('>H', buf, 6)[0]
        return tex


def build_texture_list(file):
    file.seek(texture_list_base)
    tl = []
    for i in range(0, texture_list_entry_count):
        tl.append(texture_definition.from_file(file))

    return tl


with open(sprdata_file, 'rb') as f:
    texture_blob = f.read()

with open(sonicworld_file, 'rb') as sw:
    address_range = AddressRange(0xd040, 0xd040 + 438312)
    texture_list = build_texture_list(sw)

    for i, t in enumerate(texture_list):
        pattern_addr = t.cmdsrca * 8
        pattern_size = int((t.cmdsize >> 8) * 8 * (t.cmdsize & 0xFF) / 2)
        address_range.classify(pattern_addr, pattern_addr + pattern_size, 'texture')

        palette_addr = t.cmdcolr * 8
        palette_size = 16 * 2 * 8
        address_range.classify(palette_addr, palette_addr + palette_size, 'palette')

    for a in address_range.ranges:
        print ("0x{:x} -> 0x{:x} => {}".format(a.start, a.end, a.range_type))



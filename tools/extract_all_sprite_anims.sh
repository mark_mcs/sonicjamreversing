#!/bin/bash

SCRIPT_DIR=$(dirname $(realpath $0))
ASSET_DIR=${SCRIPT_DIR}/../assets/sprite_anims
FILE=../../SonicJam/museum.mus
INDEX_FILE=${ASSET_DIR}/index.S

cat /dev/null > ${INDEX_FILE}

function dump_frames() {
    local addr=$1
    local label=$2
    local outfile=$3

    echo "    .global ${label}" > ${outfile}
    echo "${label}:" >> ${outfile}
    python3 ${SCRIPT_DIR}/dump_sprite_animation.py -f ${FILE} -a ${addr} >> ${outfile}

    echo "    .include \"${outfile}\"" >> ${INDEX_FILE}
}

function dump_frames_unknown() {
    local addr=$1
    dump_frames $addr "anim_frames_${addr}" ${ASSET_DIR}/anim_${addr}.S
}

function dump_anim_seq() {
    local addr=$1
    local label=$2
    local outfile=$3

    echo "    .global ${label}" > ${outfile}
    echo "${label}:" >> ${outfile}
    python3 ${SCRIPT_DIR}/dump_anim_sequence.py -f ${FILE} -a ${addr} >> ${outfile}

    echo "    .include \"${outfile}\"" >> ${INDEX_FILE}
}

function dump_anim_seq_unknown() {
    local addr=$1
    dump_anim_seq $addr "anim_seq_${addr}" ${ASSET_DIR}/anim_seq_${addr}.S
}

mkdir -p ${ASSET_DIR}

dump_frames 0x0607257c ring_sprite_info_table ${ASSET_DIR}/ring_anim_frames.S
dump_anim_seq 0x0607263c ring_animation_sequence ${ASSET_DIR}/ring_anim_seq.S
dump_frames 0x0607264e ring_sparkle_sprite_info_table ${ASSET_DIR}/ring_sparkle_anim_frames.S
dump_anim_seq 0x06072836 big_ring_anim_sequence ${ASSET_DIR}/big_ring_anim_seq.S
dump_frames_unknown 0x06072850
dump_anim_seq_unknown 0x060728b4
dump_frames_unknown 0x060728c4
dump_anim_seq_unknown 0x06072924
dump_frames_unknown 0x0607292e
dump_anim_seq_unknown 0x06072976
dump_frames_unknown 0x06072980
dump_anim_seq_unknown 0x06072a14
dump_frames_unknown 0x06072a24
dump_anim_seq_unknown 0x06072ab4
dump_frames_unknown 0x06072ac2
dump_frames_unknown 0x06072b6a
dump_anim_seq_unknown 0x06072d32
dump_frames_unknown 0x06072d5a
dump_frames_unknown 0x06072da4
dump_frames_unknown 0x06072e04
dump_anim_seq_unknown 0x06072e64
dump_frames_unknown 0x06072e6e
dump_anim_seq_unknown 0x06072ece
dump_frames_unknown 0x06072ed8
dump_anim_seq_unknown 0x06072f38
dump_frames_unknown 0x06072f42
dump_frames_unknown 0x06073c8e
dump_frames_unknown 0x0607420a
dump_frames_unknown 0x06074862
dump_frames_unknown 0x06076ab6
dump_frames_unknown 0x06077e6e
dump_frames_unknown 0x0607aa16
dump_frames_unknown 0x0607aa46
dump_frames_unknown 0x0607aa76
dump_frames_unknown 0x0607aaee

BEGIN {
    pre_indent = "";
    directive_indent = "    "
    label_indent = "";
    code_indent = "    ";
    comment_indent = "    "
    last_label = "";

    skipping = 0;
    raw_insert_mode = 0;

    outfile = "main.S"
}

    function change_file(name) {
        if (match(name, /(.*)\//, path)) {
            system("mkdir -p " path[1])
        }
        outfile=name
    }

    function print_file_header() {
        print "! *******************************************************************************" > outfile;
        print "! * GENERATED FILE                                                              *" >> outfile;
        print "! * Changes to this file will be overwritten by the Ghidra export               *" >> outfile;
        print "! *******************************************************************************" >> outfile;
        printf("\n") >> outfile;
        print directive_indent ".include \"bios.S\"" >> outfile;
        print directive_indent ".include \"defines.S\"" >> outfile;
        print directive_indent ".include \"kernel_exports.S\"" >> outfile;
        print directive_indent ".include \"platform.S\"" >> outfile;
        print directive_indent ".include \"sound_ids.S\"" >> outfile;
        print directive_indent ".include \"system_variables.S\"" >> outfile;
        print directive_indent ".include \"variables.S\"" >> outfile;
    }

    function sanitize_label(label) {
        gsub(/\[/, "", label);
        gsub(/\]/, "", label);
        gsub(/\./, "_", label);
        gsub(/\?/, "", label);
        gsub(/:/, "_", label);
        return label;
    }

    function format_mnemonic(mnemonic) {
        # ghidra puts a leading _ on delay slot instrs
        if (match (mnemonic, /^_(.*)/, m)) {
            return m[1];
        }

        # convert data definitions
        if (mnemonic == "undefined1" || mnemonic == "db" || mnemonic == "??") {
            # unidentified data
            return ".byte";
        }
        if (mnemonic == "undefined2" || mnemonic == "dw" || mnemonic == "short" || mnemonic == "ushort") {
            return ".word";
        }
        if (mnemonic == "undefined4" || mnemonic == "int" || mnemonic == "uint" || mnemonic == "FIXED") {
            return ".long";
        }
        if (mnemonic == "ds") {
            return ".string";
        }
        if (mnemonic == "addr") {
            return ".long";
        }

        return mnemonic;
    }

    function format_single_operand(opcode, op, op_idx) {
        # convert raw hex address
        op = gensub(/^([0-9a-fA-F]+)h$/, "0x\\1", "g", op);

        #if (match(op, /^(-?(0x)?[0-9a-fA-F]+)$/, m) && match(opcode, /^\./) == 0) {
        #    return sprintf("#%s", m[1]);
        #}

        # strip any variable refs
        gsub(/->/, "", op);
        gsub(/=>[[:alnum:]_\.:\[\]\-]+(\+(0x)?[0-9a-fA-F]+)?/, "", op);

        op = sanitize_label(op);

        # immediate operands need to be prefixed with a hash except for directives
        if (op_indx == 0 \
               && match(opcode, /^(mov|add|and(\.b)?|or(\.b)?|cmp.*|tst(\.b)?|xor(\.b)?|trapa)$/) \
               && match(op, /^#/) == 0 \
               && match(op, /^[rR][0-9]{1,2}$/) == 0) {
            return sprintf("#%s", op);
        }

        # reformat any pc relative addressing to the gnu as format
        op = gensub(/@\((.+),pc\)/, "\\1", "g", op);
        return op;
    }

    function format_operands(opcode, operands) {

        # match operands that look like "(... , ...), ..." since they contain
        # an extra ',' that would interfere with the next match. this keeps it
        # easier than trying to use lookahead and making a regex mess
        if (match(operands, /^(@\(.*\)),(.*)$/, m)) {
            lhs = m[1];
            rhs = m[2];
            lhs = format_single_operand(opcode, lhs, 0);
            rhs = format_single_operand(opcode, rhs, 1);
            return sprintf("%s, %s", lhs, rhs);
        }

        if (match(operands, /^(.*),(@\(.*\))$/, m)) {
            lhs = m[1];
            rhs = m[2];
            lhs = format_single_operand(opcode, lhs, 0);
            rhs = format_single_operand(opcode, rhs, 1);
            return sprintf("%s, %s", lhs, rhs);
        }

        if (match(operands, /^(.*),(.*)$/, m)) {
            lhs = m[1];
            rhs = m[2];
            lhs = format_single_operand(opcode, lhs, 0);
            rhs = format_single_operand(opcode, rhs, 1);
            return sprintf("%s, %s", lhs, rhs);
        }
        return format_single_operand(opcode, operands, 0);
    }


    # check for processing directives before we start looking at source code
    match ($0, /@begin_insert_assembly/) {
        raw_insert_mode = 1;
        next;
    }

    match ($0, /@end_insert_assembly/) {
        raw_insert_mode = 0;
        next;
    }

    raw_insert_mode > 0 {
        if (match($0, /^[[:space:]]{29}(.*)$/, m)) {
            $0 = m[1];
        }
        print $0 >> outfile;
        next;
    }

    match ($0, /@set_output_file[[:space:]]+"(.*)"/, m) {
        change_file(m[1]);
        print_file_header();
        next;
    }

    match ($0, /@begin_skip_export/) {
        skipping = 1;
        next;
    }

    match ($0, /@end_skip_export/) {
        skipping = 0;
        next;
    }

    match ($0, /@include_file[[:space:]]+"(.*)"/, m) {
        print pre_indent directive_indent ".include \"" m[1] "\"" >> outfile;
        next;
    }

    match ($0, /@incbin_file[[:space:]]+"(.*)"/, m) {
        print pre_indent directive_indent ".incbin \"" m[1] "\"" >> outfile;
        next;
    }

    skipping != 0 { next; }

    # match labels:
    #   starts in column 30
    #   only valid identifier chars (including .[])
    #   followed by whitespace to end of line
    #   optional 'XREF' at the end
    match ($0, /^[[:space:]]{29}([[:alpha:]_][[:alnum:]_\.\[\]\?:]*)[[:space:]]*(XREF.*)?$/, m) {
        lbl = m[1];
        lbl = sanitize_label(lbl);
        last_label = lbl;
        next;
    }

    # preserve plate comments:
    #   nothing but spaces from the start of line
    #   until the first *
    #   then anything up to the lat *
    #   then whitespace until the end of line
    match ($0, /^[[:space:]]+(\*.*\*)[[:space:]]*$/, m) {
        print pre_indent "! " m[1] >> outfile;
        next;
    }

    # match a line that has an address
    #   has a :: near the start
    #   followed by the address
    #   then a space
    #   then some number of bytes that encode the value at the address
    #   then the actual instruction
    #   then 
    #       end of line
    #   or
    #       at least 10 spaces
    #       followed by whatever until the end of line
    match ($0, /^.*::([0-9a-fA-F]{8})[[:space:]].{16}([^[:space:]]+)([[:space:]]*([^[:space:]]+))?.*$/, m) {
        address = m[1];
        opcode = m[2];
        operands = length(m) > 3 ? m[4] : "";

        if (last_label != "" ) {
            print "" >> outfile;
            #print pre_indent directive_indent ".org 0x" address >> outfile;
            print pre_indent directive_indent ".global " last_label >> outfile;
            print pre_indent label_indent last_label ":" >> outfile;
            last_label = "";
        }

        opcode = format_mnemonic(opcode);
        printf "%s%s%-12s%s\n", pre_indent, code_indent, opcode, format_operands(opcode, operands) >> outfile;
        next;
    }

    # preserve pre-/post-comments
    #   nothing but spaces from the start of line
    #   then printable characters
    #   then nothing but spaces to end of line
    match ($0, /^[[:space:]]{29}([^[:space:]].*)([[:space:]]{2:})?$/, m) {
        print pre_indent comment_indent "! " m[1] >> outfile;
        next;
    }

    # swallow anything else as it's likely ghidra annotations

    #{ print "X" $0 }


import struct

f = open('../../SonicJam/museum.mus', 'rb')
buf = f.read()
f.close

base_addr = 0x060854c4 - 0x6040000
f.seek(base_addr)

for i in range(0, 14):
    x = struct.unpack_from('>i', buf, base_addr)[0] / 65536.0
    y = struct.unpack_from('>i', buf, base_addr + 4)[0] / 65536.0
    z = struct.unpack_from('>i', buf, base_addr + 8)[0] / 65536.0
    
    print('    POS_TO_FIXED({}, {}, {}),'.format(x, y, z))
    base_addr = base_addr + 12

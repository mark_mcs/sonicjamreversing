import struct
import argparse

LOAD_ADDRESS = 0x06040000


class sprite_def:
    def __init__(self):
        self.texture_id = 0
        self.x_offset = 0
        self.y_offset = 0
        self.cmdpod = 0
        self.field4 = 0
        self.type = 0
        self.field6 = 0
        self.field7 = 0
        self.field8 = 0
        self.field9 = 0
        self.field10 = 0
        self.field11 = 0
        self.field12 = 0
        self.field13 = 0
        self.field14 = 0
        self.field15 = 0

    def parse_from(buf, offs):
        spr = sprite_def()
        spr.texture_id = struct.unpack_from('>H', buf, offs)[0]
        offs += 2
        spr.x_offset = struct.unpack_from('>H', buf, offs)[0]
        offs += 2
        spr.y_offset = struct.unpack_from('>H', buf, offs)[0]
        offs += 2
        spr.cmdpmod = struct.unpack_from('>H', buf, offs)[0]
        offs += 2
        spr.field4 = buf[offs]
        offs += 1
        spr.type = buf[offs]
        offs += 1
        spr.field6 = buf[offs]
        offs += 1
        spr.field7 = buf[offs]
        offs += 1
        spr.field8 = buf[offs]
        offs += 1
        spr.field9 = buf[offs]
        offs += 1
        spr.field10 = buf[offs]
        offs += 1
        spr.field11 = buf[offs]
        offs += 1
        spr.field12 = buf[offs]
        offs += 1
        spr.field13 = buf[offs]
        offs += 1
        spr.field14 = buf[offs]
        offs += 1
        spr.field15 = buf[offs]
        return spr


class anim_frames:
    def __init__(self):
        self.frame_ids = []
        self.frame_lists = []

    
def parse_sprite_list_at(buf, addr):
    sprites = []
    while True:
        marker = struct.unpack_from('>H', buf, addr)[0]
        if marker == 0xFFFF:
            return sprites
        sprites.append(sprite_def.parse_from(buf, addr))
        addr += 20
    return sprites


def parse_anim_frames(buf, addr):
    end_of_offsets = 0xFFFFFFFF
    offs = addr

    anim = anim_frames()

    frame_ptrs = []
    # read the list of offsets to find the start of the sprite definitions
    while offs < end_of_offsets:
        ptr = addr + struct.unpack_from('>H', buf, offs)[0]
        offs += 2
        if ptr < end_of_offsets:
            end_of_offsets = ptr
        frame_ptrs.append(ptr)

    sorted_frame_ptrs = list(sorted(set(frame_ptrs)))
    # convert each pointer offset to an id number
    for p in frame_ptrs:
        anim.frame_ids.append(sorted_frame_ptrs.index(p))

    # parse each frame list in the order defined in the source binary
    for offs in sorted_frame_ptrs:
        anim.frame_lists.append(parse_sprite_list_at(buf, offs))
    
    return anim


def dump_to_assembly(anim):
    # reconstruct the offset word list from the frame ids
    frame_list_offsets = []
    offset = len(anim.frame_ids) * 2
    frame_list_offsets.append(offset)
    for l in anim.frame_lists:
        offset += len(l) * 20 + 2
        frame_list_offsets.append(offset)
    
    for i in anim.frame_ids:
        print('.dc.w 0x{:04x}'.format(frame_list_offsets[i]))

    for i, frame_list in enumerate(anim.frame_lists):
        print('    ! frame {}'.format(i))

        for frame in frame_list:
            print('.dc.w 0x{:04x}'.format(frame.texture_id))
            print('    .dc.w 0x{:04x}'.format(frame.x_offset))
            print('    .dc.w 0x{:04x}'.format(frame.y_offset))
            print('    .dc.w 0x{:04x}'.format(frame.cmdpmod))
            print('    .dc.b 0x{:02x}'.format(frame.field4))
            print('    .dc.b 0x{:02x}'.format(frame.type))
            print('    .dc.b 0x{:02x}'.format(frame.field6))
            print('    .dc.b 0x{:02x}'.format(frame.field7))
            print('    .dc.b 0x{:02x}'.format(frame.field8))
            print('    .dc.b 0x{:02x}'.format(frame.field9))
            print('    .dc.b 0x{:02x}'.format(frame.field10))
            print('    .dc.b 0x{:02x}'.format(frame.field11))
            print('    .dc.b 0x{:02x}'.format(frame.field12))
            print('    .dc.b 0x{:02x}'.format(frame.field13))
            print('    .dc.b 0x{:02x}'.format(frame.field14))
            print('    .dc.b 0x{:02x}'.format(frame.field15))
        print('.dc.w 0xFFFF')

#def dump_to_json(anim):
#    pass

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', required = True)
parser.add_argument('-a', '--address', required = True)

args = parser.parse_args()

f = open(args.file, 'rb')
buf = f.read()
f.close()

anim = parse_anim_frames(buf, int(args.address, 0) - LOAD_ADDRESS)

dump_to_assembly(anim)

import struct

LOAD_ADDRESS = 0x06040000
BASE_ADDRESS = 0x060439e4

f = open('../../SonicJam/museum.mus', 'rb')
buf = f.read()
f.close()

offs = BASE_ADDRESS - LOAD_ADDRESS
pointers = []


print('    .org 0x{:08x}'.format(BASE_ADDRESS))
print('g_level_grid_cells:')

for i in range(0, 7*8):
    pointers.append(struct.unpack_from('>I', buf, offs + i * 4)[0])

uniq_ptrs = list(sorted(set(pointers)))

for i in range(0, 7*8):
    idx = uniq_ptrs.index(pointers[i])
    print('    .long _g_level_grid_cell_list_{:02d}'.format(idx))

for i in range(0, len(uniq_ptrs)):
    offs = uniq_ptrs[i] - LOAD_ADDRESS
    print('_g_level_grid_cell_list_{:02d}:'.format(i))
    while True:
        list_ptr = struct.unpack_from('>I', buf, offs)[0]
        offs += 4
        if list_ptr == 0:
            print('    .long 0')
            break
        else:
            print('    .long model_0x{:08x}_model_00'.format(list_ptr))


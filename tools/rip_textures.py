import struct
import os

texture_table = 0x06072a24 - 0x6040000

sprdata_file = '../../SonicJam/sprdata.mus'
sprdata_base = 0xd040

sonicworld_file = '../../SonicJam/museum.mus'
texture_list_base = 0x25104
texture_list_entry_count = 475

texture_entry_def = '>HHHH'
texture_entry_size = struct.calcsize(texture_entry_def)


class texture_definition:
    def __init__(self):
        self.cmdsrca = 0
        self.cmdsize = 0
        self.cmdcolr = 0
        self.field3 = 0
        self.extracted = False

    def from_file(file):
        tex = texture_definition()
        buf = file.read(8)
        tex.cmdsrca = struct.unpack_from('>H', buf, 0)[0]
        tex.cmdsize = struct.unpack_from('>H', buf, 2)[0]
        tex.cmdcolr = struct.unpack_from('>H', buf, 4)[0]
        tex.field3 = struct.unpack_from('>H', buf, 6)[0]
        return tex


def extract_clut_texture(texture_def, texture_blob, blob_base, outfile):
    w = (texture_def.cmdsize & 0xFF00) >> 5
    h = texture_def.cmdsize & 0xFF

    texture_base = texture_def.cmdsrca * 8
    begin = texture_base - blob_base
    end = int(begin + w * h / 2)

    imgdata = texture_blob[begin:end]
    #palette = [0] * 16
    palette = [0] * 16 * 8      # 8 palettes of 16 colours
    
    tgabuffer = bytearray()
    # id length
    tgabuffer.append(0)
    # colour map type
    tgabuffer.append(1)
    # image type
    tgabuffer.append(1)

    # collect the palette
    palette_base = texture_def.cmdcolr * 8 - blob_base
    for i in range(0, len(palette)):
        value = (texture_blob[palette_base + i * 2] << 8) \
                | texture_blob[palette_base + i * 2 + 1]
        value = 0x8000 | ((value & 0x7C00) >> 10) | (value & 0x3E0) | ((value & 0x1F) << 10)
        palette[i] = value

    # first entry index
    tgabuffer.append(0)
    tgabuffer.append(0)
    # num elements
    tgabuffer += struct.pack('<H', len(palette))
    # bits per pixel
    tgabuffer.append(16)
    # image spec
    tgabuffer += struct.pack('<H', 0)
    tgabuffer += struct.pack('<H', 0)
    tgabuffer += struct.pack('<H', w)
    tgabuffer += struct.pack('<H', h)
    tgabuffer.append(8)
    tgabuffer.append(0)

    # write the colour map
    for c in palette:
        tgabuffer += struct.pack('<H', c)
    
    # write image data
    for y in range(h-1, -1, -1):
        for x in range(0, w//2):
            p = imgdata[y * w//2 + x]
            tgabuffer.append(p >> 4)
            tgabuffer.append(p & 0xF)

    outfile.write(tgabuffer)


def extract_texture(i, texture, texture_blob, blob_base):
    if not os.path.isdir('texture_dump'):
        os.mkdir('texture_dump')
    
    with open('texture_dump/{:04X}.tga'.format(i), 'wb') as f:
        extract_clut_texture(texture, texture_blob, blob_base, f)


def build_texture_list(file):
    file.seek(texture_list_base)
    tl = []
    for i in range(0, texture_list_entry_count):
        tl.append(texture_definition.from_file(file))

    return tl


with open(sprdata_file, 'rb') as f:
    texture_blob = f.read()

with open(sonicworld_file, 'rb') as sw:
    texture_list = build_texture_list(sw)

    for i, t in enumerate(texture_list):
        extract_texture(i, t, texture_blob, sprdata_base)

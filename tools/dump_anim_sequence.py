import argparse

LOAD_ADDRESS = 0x06040000

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', required = True)
parser.add_argument('-a', '--address', required = True)

args = parser.parse_args()

f = open(args.file, 'rb')
buf = f.read()
f.close()

addr = int(args.address, 0) - LOAD_ADDRESS
end = False
count = 0

while True:
    v = buf[addr]
    addr += 1

    if count == 0:
        count = 1
        print('    .dc.b 0x{:02x}'.format(v), end='')
        if v > 0x80: end = True

    elif count == 1:
        count = 0
        print(', 0x{:02x}'.format(v))
        if end: break



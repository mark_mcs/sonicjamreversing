CROSS_DIR = /opt/cross/bin
AS = $(CROSS_DIR)/sh-elf-gcc
CC = $(CROSS_DIR)/sh-elf-gcc
CXX = $(CROSS_DIR)/sh-elf-g++
LD = $(CROSS_DIR)/sh-elf-gcc
AR = $(CROSS_DIR)/sh-elf-ar
OBJCOPY = $(CROSS_DIR)/sh-elf-objcopy

GLOBAL_INCLUDE_DIR=$(PROJECT_ROOT)/include

ASFLAGS ?= -m2 -nostdlib -c -I$(GLOBAL_INCLUDE_DIR)
CFLAGS ?= -m2 -nostdlib -std=c99 -c -I$(GLOBAL_INCLUDE_DIR)
CXXFLAGS ?= -m2 -ffreestanding -c -I$(GLOBAL_INCLUDE_DIR)
LDFLAGS ?= -nostartfiles -L-Map=$(BUILD_DIR)/layout.map

#!/bin/bash

if [ $1 = "shell" ]; then
    /bin/bash
else
    cd /build
    make $*
fi

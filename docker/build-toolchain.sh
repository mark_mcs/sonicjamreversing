HOST_ARCH=$(uname -m)
export SRCDIR=$(pwd)/source
export BUILDDIR=$(pwd)/build
export TARGETMACH=sh-elf
export BUILDMACH=$HOST_ARCH-linux-gnu
export HOSTMACH=$HOST_ARCH-linux-gnu
export INSTALLDIR=/opt/cross
export SYSROOTDIR=$INSTALLDIR/sysroot
export ROOTDIR=$(pwd)
export DOWNLOADDIR=$(pwd)/download
export PROGRAM_PREFIX=sh-

./build-elf.sh

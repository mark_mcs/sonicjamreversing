export PROJECT_DIR=.
export BUILD_DIR=build

include mk/config.mk
include mk/model_objects.mk

LDFLAGS += -e _start

ORIGINAL_CHECKSUM=7b9b1493e7e0145cb929df095709bc3ac4549c9d776030e92c70a730114906e3

$(BUILD_DIR)/museum.mus: models main $(BUILD_DIR)/museum.elf
	$(OBJCOPY) -O binary $(BUILD_DIR)/museum.elf $@
	@sha256sum -c --status museum.mus.sha256 || (echo "*** CHECKSUM MISMATCH ***" && exit -1)

.phony: main
main:
	$(MAKE) -C src

.phony: models
models:
	$(MAKE) -C assets/models

.phony: export
export: 
	pushd private/tmp && awk -f ../../tools/format_raw_dump.awk ../raw_dump/0loader.txt && popd

clean:
	-rm -rf $(OBJ_DIR)
	-rm -rf $(BUILD_DIR)


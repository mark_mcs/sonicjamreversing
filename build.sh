#!/bin/bash

docker run --rm -it \
    -v `pwd`:/build \
    --user $(id -u ${USER}):$(id -g $USER) \
    markmcs/saturn:sonicjam-build-v1.0 $*

! *******************************************************************************
! * GENERATED FILE                                                              *
! * Changes to this file will be overwritten by the Ghidra export               *
! *******************************************************************************

    .include "bios.S"
    .include "defines.S"
    .include "kernel_exports.S"
    .include "platform.S"
    .include "sound_ids.S"
    .include "system_variables.S"
    .include "variables.S"

    .global obj_exit_init
obj_exit_init:
    mov.l       @r14, r0
    ! put the model data at 0x20
    mov.l       @r0+, r1
    mov.l       r1, @(0x20,r14)
    mov.l       PTR_obj_exit_main_SONICWORLD__0605d5ec, r2
    rts         
    mov.l       r2, @-r4

    .global PTR_obj_exit_main_SONICWORLD__0605d5ec
PTR_obj_exit_main_SONICWORLD__0605d5ec:
    .long       obj_exit_main
    .byte       0x60
    .byte       0xE2
    .byte       0x61
    .byte       0x06
    .byte       0x1E
    .byte       0x18
    .byte       0xD2
    .byte       0x01
    .byte       0x00
    .byte       0x0B
    .byte       0x24
    .byte       0x26
    .byte       0x06
    .byte       0x05
    .byte       0xD6
    .byte       0xC6
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_exit_main()

    .global obj_exit_main
obj_exit_main:
    mov.l       @(GBR_CAM_SCENE_TARGET_OBJ,gbr), r0
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    sts         mach, r3
    add         #-0x10, r4
    mov.w       @(0xa,r4), r0
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605d63c
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605d69c, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605d6a0, r0
    jmp         @r0

    .global LAB_SONICWORLD__0605d63c
LAB_SONICWORLD__0605d63c:
    add         #0x10, r4
    sts.l       pr, @-r15

    .global LAB_SONICWORLD__0605d640
LAB_SONICWORLD__0605d640:
    mov.b       @(GBR_FRAME_PHASE,gbr), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__0605d6c0
    mov.l       DAT_SONICWORLD__0605d6a4, r0
    mov.l       @(0x30,r14), r4
    mov.l       @(0x38,r14), r5
    add         r0, r4
    add         r0, r5
    shlr16      r4
    shlr8       r4
    shlr2       r4
    shlr16      r5
    shlr8       r5
    shlr2       r5
    shll        r5
    mov.l       PTR_visible_map_regions_SONICWORLD__0605d6a8, r3
    add         r5, r3
    mov.w       @r3+, r0
    cmp/pz      r0
    bf          LAB_SONICWORLD__0605d6c0
    exts.b      r0, r2
    swap.b      r0, r0
    exts.b      r0, r0
    cmp/hs      r0, r4
    bf          LAB_SONICWORLD__0605d6c0
    cmp/hs      r4, r2
    bf          LAB_SONICWORLD__0605d6c0
    mov.l       r14, @-r15
    mov.l       PTR_slPushMatrix_SONICWORLD__0605d6ac, r13
    jsr         @r13
    mov         r14, r1
    mov.l       @(0x30,r14), r4
    mov.l       @(0x34,r14), r5
    mov.l       PTR_slTranslate_SONICWORLD__0605d6b0, r13
    jsr         @r13
    mov.l       @(0x38,r14), r6
    mov.l       PTR_slRotY_SONICWORLD__0605d6b4, r13
    jsr         @r13
    mov.w       @(0x1a,r14), r0
    mov.l       PTR_sw_dispatch_xform_and_light_task_SONICWORLD__0605d6b8, r13
    jsr         @r13
    mov.l       @(0x20,r14), r2
    mov.l       @r15+, r14
    mov.l       PTR_slPopMatrix_SONICWORLD__0605d6bc, r13
    jmp         @r13
    lds.l       @r15+, pr

    .global PTR_DAT_SONICWORLD__0605d69c
PTR_DAT_SONICWORLD__0605d69c:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605d6a0
PTR_obj_destructor_SONICWORLD__0605d6a0:
    .long       obj_destructor

    .global DAT_SONICWORLD__0605d6a4
DAT_SONICWORLD__0605d6a4:
    .long       0x10000000

    .global PTR_visible_map_regions_SONICWORLD__0605d6a8
PTR_visible_map_regions_SONICWORLD__0605d6a8:
    .long       visible_map_regions

    .global PTR_slPushMatrix_SONICWORLD__0605d6ac
PTR_slPushMatrix_SONICWORLD__0605d6ac:
    .long       slPushMatrix

    .global PTR_slTranslate_SONICWORLD__0605d6b0
PTR_slTranslate_SONICWORLD__0605d6b0:
    .long       slTranslate

    .global PTR_slRotY_SONICWORLD__0605d6b4
PTR_slRotY_SONICWORLD__0605d6b4:
    .long       slRotY

    .global PTR_sw_dispatch_xform_and_light_task_SONICWORLD__0605d6b8
PTR_sw_dispatch_xform_and_light_task_SONICWORLD__0605d6b8:
    .long       sw_dispatch_xform_and_light_task

    .global PTR_slPopMatrix_SONICWORLD__0605d6bc
PTR_slPopMatrix_SONICWORLD__0605d6bc:
    .long       slPopMatrix

    .global LAB_SONICWORLD__0605d6c0
LAB_SONICWORLD__0605d6c0:
    lds.l       @r15+, pr
    rts         
    nop         
    .byte       0xC6
    .byte       0x78
    .byte       0x51
    .byte       0x0C
    .byte       0x52
    .byte       0xEC
    .byte       0x32
    .byte       0x18
    .byte       0x2F
    .byte       0x26
    .byte       0x52
    .byte       0xEE
    .byte       0x50
    .byte       0x0E
    .byte       0x32
    .byte       0x08
    .byte       0x2F
    .byte       0x26
    .byte       0x00
    .byte       0x28
    .byte       0x63
    .byte       0xF3
    .byte       0x62
    .byte       0xF3
    .byte       0x02
    .byte       0x3F
    .byte       0x02
    .byte       0x3F
    .byte       0x7F
    .byte       0x08
    .byte       0x03
    .byte       0x0A
    .byte       0x74
    .byte       0xF0
    .byte       0x85
    .byte       0x45
    .byte       0x40
    .byte       0x28
    .byte       0x33
    .byte       0x06
    .byte       0x8B
    .byte       0x08
    .byte       0x85
    .byte       0x44
    .byte       0xD3
    .byte       0x1C
    .byte       0x60
    .byte       0x0D
    .byte       0x33
    .byte       0x0C
    .byte       0x84
    .byte       0x30
    .byte       0xC9
    .byte       0xFE
    .byte       0x80
    .byte       0x30
    .byte       0xD0
    .byte       0x1A
    .byte       0x40
    .byte       0x2B
    .byte       0x74
    .byte       0x10
    .byte       0x4F
    .byte       0x22
    .byte       0xC4
    .byte       0x09
    .byte       0x20
    .byte       0x08
    .byte       0x89
    .byte       0x3D
    .byte       0xD0
    .byte       0x17
    .byte       0x54
    .byte       0xEC
    .byte       0x55
    .byte       0xEE
    .byte       0x34
    .byte       0x0C
    .byte       0x35
    .byte       0x0C
    .byte       0x44
    .byte       0x29
    .byte       0x44
    .byte       0x19
    .byte       0x44
    .byte       0x09
    .byte       0x45
    .byte       0x29
    .byte       0x45
    .byte       0x19
    .byte       0x45
    .byte       0x09
    .byte       0x45
    .byte       0x00
    .byte       0xD3
    .byte       0x12
    .byte       0x33
    .byte       0x5C
    .byte       0x60
    .byte       0x35
    .byte       0x40
    .byte       0x11
    .byte       0x8B
    .byte       0x2C
    .byte       0x62
    .byte       0x0E
    .byte       0x60
    .byte       0x08
    .byte       0x60
    .byte       0x0E
    .byte       0x34
    .byte       0x02
    .byte       0x8B
    .byte       0x27
    .byte       0x32
    .byte       0x42
    .byte       0x8B
    .byte       0x25
    .byte       0x2F
    .byte       0xE6
    .byte       0xDD
    .byte       0x0D
    .byte       0x4D
    .byte       0x0B
    .byte       0x61
    .byte       0xE3
    .byte       0x54
    .byte       0xEC
    .byte       0x55
    .byte       0xED
    .byte       0xDD
    .byte       0x0B
    .byte       0x4D
    .byte       0x0B
    .byte       0x56
    .byte       0xEE
    .byte       0xDD
    .byte       0x0B
    .byte       0x4D
    .byte       0x0B
    .byte       0x85
    .byte       0xED
    .byte       0xDD
    .byte       0x0A
    .byte       0x4D
    .byte       0x0B
    .byte       0x52
    .byte       0xE8
    .byte       0x6E
    .byte       0xF6
    .byte       0xDD
    .byte       0x09
    .byte       0x4D
    .byte       0x2B
    .byte       0x4F
    .byte       0x26
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x0C
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x04
    .byte       0x2F
    .byte       0xA0
    .byte       0x10
    .byte       0x00
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x05
    .byte       0x1C
    .byte       0xE8
    .byte       0x06
    .byte       0x04
    .byte       0x1F
    .byte       0xDA
    .byte       0x06
    .byte       0x04
    .byte       0x21
    .byte       0x92
    .byte       0x06
    .byte       0x04
    .byte       0x20
    .byte       0xB0
    .byte       0x06
    .byte       0x04
    .byte       0x58
    .byte       0xC6
    .byte       0x06
    .byte       0x04
    .byte       0x20
    .byte       0x04
    .byte       0x4F
    .byte       0x26
    .byte       0x00
    .byte       0x0B
    .byte       0x00
    .byte       0x09

    .global LAB_SONICWORLD__0605d78e
LAB_SONICWORLD__0605d78e:
    mov.l       @(GBR_CAM_SCENE_TARGET_OBJ,gbr), r0
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    sts         mach, r3
    add         #-0x10, r4
    mov.w       @(0xa,r4), r0
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605d7ca
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605d834, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605d838, r0
    jmp         @r0

    .global LAB_SONICWORLD__0605d7ca
LAB_SONICWORLD__0605d7ca:
    add         #0x10, r4
    sts.l       pr, @-r15
    mov.b       @(GBR_FRAME_PHASE,gbr), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__0605d8a0
    mov.l       DAT_SONICWORLD__0605d83c, r0
    mov.l       @(0x30,r14), r4
    mov.l       @(0x38,r14), r5
    add         r0, r4
    add         r0, r5
    shlr16      r4
    shlr8       r4
    shlr2       r4
    shlr16      r5
    shlr8       r5
    shlr2       r5
    shll        r5
    mov.l       PTR_visible_map_regions_SONICWORLD__0605d840, r3
    add         r5, r3
    mov.w       @r3+, r0
    cmp/pz      r0
    bf          LAB_SONICWORLD__0605d8a0
    exts.b      r0, r2
    swap.b      r0, r0
    exts.b      r0, r0
    cmp/hs      r0, r4
    bf          LAB_SONICWORLD__0605d8a0
    cmp/hs      r4, r2
    bf          LAB_SONICWORLD__0605d8a0
    mov.l       r14, @-r15
    mov.l       PTR_slPushMatrix_SONICWORLD__0605d844, r13
    jsr         @r13
    mov         r14, r1
    mov.l       @(0x30,r14), r4
    mov.l       @(0x34,r14), r5
    mov.l       PTR_slTranslate_SONICWORLD__0605d848, r13
    jsr         @r13
    mov.l       @(0x38,r14), r6
    mov.l       PTR_slRotY_SONICWORLD__0605d84c, r13
    jsr         @r13
    mov.w       @(0x1a,r14), r0
    mov.l       @(0x28,r14), r13

    .global LAB_SONICWORLD__0605d81e
LAB_SONICWORLD__0605d81e:
    mov.w       @r13+, r0
    cmp/eq      #-0x2, r0
    bt          LAB_SONICWORLD__0605d894
    cmp/eq      #-0x1, r0
    bf          LAB_SONICWORLD__0605d854
    mov.l       PTR_slPopMatrix_SONICWORLD__0605d850, r0
    jsr         @r0
    nop         
    bra         LAB_SONICWORLD__0605d81e
    nop         
    .byte       0x00
    .byte       0x00

    .global PTR_DAT_SONICWORLD__0605d834
PTR_DAT_SONICWORLD__0605d834:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605d838
PTR_obj_destructor_SONICWORLD__0605d838:
    .long       obj_destructor

    .global DAT_SONICWORLD__0605d83c
DAT_SONICWORLD__0605d83c:
    .long       0x10000000

    .global PTR_visible_map_regions_SONICWORLD__0605d840
PTR_visible_map_regions_SONICWORLD__0605d840:
    .long       visible_map_regions

    .global PTR_slPushMatrix_SONICWORLD__0605d844
PTR_slPushMatrix_SONICWORLD__0605d844:
    .long       slPushMatrix

    .global PTR_slTranslate_SONICWORLD__0605d848
PTR_slTranslate_SONICWORLD__0605d848:
    .long       slTranslate

    .global PTR_slRotY_SONICWORLD__0605d84c
PTR_slRotY_SONICWORLD__0605d84c:
    .long       slRotY

    .global PTR_slPopMatrix_SONICWORLD__0605d850
PTR_slPopMatrix_SONICWORLD__0605d850:
    .long       slPopMatrix

    .global LAB_SONICWORLD__0605d854
LAB_SONICWORLD__0605d854:
    mov.l       PTR_slPushMatrix_SONICWORLD__0605d888, r4
    jsr         @r4
    mov         r0, r12
    mov.l       @(0x24,r14), r0
    mov         #0xc, r2
    mulu.w      r2, r12
    sts         macl, r2
    add         r2, r0
    mov.l       @r0+, r4
    mov.l       @r0+, r5
    mov.l       PTR_slTranslate_SONICWORLD__0605d88c, r2
    jsr         @r2
    mov.l       @r0+, r6
    mov.l       @(0x20,r14), r0
    shll2       r12
    mov.l       @(r0,r12), r2
    tst         r2, r2
    bt          LAB_SONICWORLD__0605d81e
    mov.l       r14, @-r15
    mov.l       PTR_sw_dispatch_xform_and_light_task_SONICWORLD__0605d890, r10
    jsr         @r10
    mov.l       r13, @-r15
    mov.l       @r15+, r13
    bra         LAB_SONICWORLD__0605d81e
    mov.l       @r15+, r14
    .byte       0x00
    .byte       0x00

    .global PTR_slPushMatrix_SONICWORLD__0605d888
PTR_slPushMatrix_SONICWORLD__0605d888:
    .long       slPushMatrix

    .global PTR_slTranslate_SONICWORLD__0605d88c
PTR_slTranslate_SONICWORLD__0605d88c:
    .long       slTranslate

    .global PTR_sw_dispatch_xform_and_light_task_SONICWORLD__0605d890
PTR_sw_dispatch_xform_and_light_task_SONICWORLD__0605d890:
    .long       sw_dispatch_xform_and_light_task

    .global LAB_SONICWORLD__0605d894
LAB_SONICWORLD__0605d894:
    mov.l       @r15+, r14
    mov.l       PTR_slPopMatrix_SONICWORLD__0605d89c, r13
    jmp         @r13
    lds.l       @r15+, pr

    .global PTR_slPopMatrix_SONICWORLD__0605d89c
PTR_slPopMatrix_SONICWORLD__0605d89c:
    .long       slPopMatrix

    .global LAB_SONICWORLD__0605d8a0
LAB_SONICWORLD__0605d8a0:
    lds.l       @r15+, pr
    rts         
    nop         
    .byte       0xC6
    .byte       0x78
    .byte       0x51
    .byte       0x0C
    .byte       0x52
    .byte       0xEC
    .byte       0x32
    .byte       0x18
    .byte       0x2F
    .byte       0x26
    .byte       0x52
    .byte       0xEE
    .byte       0x50
    .byte       0x0E
    .byte       0x32
    .byte       0x08
    .byte       0x2F
    .byte       0x26
    .byte       0x00
    .byte       0x28
    .byte       0x63
    .byte       0xF3
    .byte       0x62
    .byte       0xF3
    .byte       0x02
    .byte       0x3F
    .byte       0x02
    .byte       0x3F
    .byte       0x7F
    .byte       0x08
    .byte       0x03
    .byte       0x0A
    .byte       0x74
    .byte       0xF0
    .byte       0x85
    .byte       0x45
    .byte       0x40
    .byte       0x28
    .byte       0x33
    .byte       0x06
    .byte       0x8B
    .byte       0x08
    .byte       0x85
    .byte       0x44
    .byte       0xD3
    .byte       0x1E
    .byte       0x60
    .byte       0x0D
    .byte       0x33
    .byte       0x0C
    .byte       0x84
    .byte       0x30
    .byte       0xC9
    .byte       0xFE
    .byte       0x80
    .byte       0x30
    .byte       0xD0
    .byte       0x1C
    .byte       0x40
    .byte       0x2B
    .byte       0x74
    .byte       0x10
    .byte       0x4F
    .byte       0x22
    .byte       0xC4
    .byte       0x09
    .byte       0x20
    .byte       0x08
    .byte       0x89
    .byte       0x63
    .byte       0xD0
    .byte       0x19
    .byte       0x54
    .byte       0xEC
    .byte       0x55
    .byte       0xEE
    .byte       0x34
    .byte       0x0C
    .byte       0x35
    .byte       0x0C
    .byte       0x44
    .byte       0x29
    .byte       0x44
    .byte       0x19
    .byte       0x44
    .byte       0x09
    .byte       0x45
    .byte       0x29
    .byte       0x45
    .byte       0x19
    .byte       0x45
    .byte       0x09
    .byte       0x45
    .byte       0x00
    .byte       0xD3
    .byte       0x14
    .byte       0x33
    .byte       0x5C
    .byte       0x60
    .byte       0x35
    .byte       0x40
    .byte       0x11
    .byte       0x8B
    .byte       0x52
    .byte       0x62
    .byte       0x0E
    .byte       0x60
    .byte       0x08
    .byte       0x60
    .byte       0x0E
    .byte       0x34
    .byte       0x02
    .byte       0x8B
    .byte       0x4D
    .byte       0x32
    .byte       0x42
    .byte       0x8B
    .byte       0x4B
    .byte       0x2F
    .byte       0xE6
    .byte       0xDD
    .byte       0x0F
    .byte       0x4D
    .byte       0x0B
    .byte       0x61
    .byte       0xE3
    .byte       0x54
    .byte       0xEC
    .byte       0x55
    .byte       0xED
    .byte       0xDD
    .byte       0x0D
    .byte       0x4D
    .byte       0x0B
    .byte       0x56
    .byte       0xEE
    .byte       0xDD
    .byte       0x0D
    .byte       0x4D
    .byte       0x0B
    .byte       0x85
    .byte       0xED
    .byte       0x5D
    .byte       0xEA
    .byte       0x60
    .byte       0xD5
    .byte       0x88
    .byte       0xFE
    .byte       0x89
    .byte       0x35
    .byte       0x88
    .byte       0xFF
    .byte       0x8B
    .byte       0x15
    .byte       0xD0
    .byte       0x09
    .byte       0x40
    .byte       0x0B
    .byte       0x00
    .byte       0x09
    .byte       0xAF
    .byte       0xF6
    .byte       0x00
    .byte       0x09
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x0C
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x04
    .byte       0x2F
    .byte       0xA0
    .byte       0x10
    .byte       0x00
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x05
    .byte       0x1C
    .byte       0xE8
    .byte       0x06
    .byte       0x04
    .byte       0x1F
    .byte       0xDA
    .byte       0x06
    .byte       0x04
    .byte       0x21
    .byte       0x92
    .byte       0x06
    .byte       0x04
    .byte       0x20
    .byte       0xB0
    .byte       0x06
    .byte       0x04
    .byte       0x20
    .byte       0x04
    .byte       0xD4
    .byte       0x0B
    .byte       0x44
    .byte       0x0B
    .byte       0x6C
    .byte       0x03
    .byte       0x50
    .byte       0xE9
    .byte       0xE2
    .byte       0x0C
    .byte       0x2C
    .byte       0x2E
    .byte       0x02
    .byte       0x1A
    .byte       0x30
    .byte       0x2C
    .byte       0x64
    .byte       0x06
    .byte       0x65
    .byte       0x06
    .byte       0xD2
    .byte       0x07
    .byte       0x42
    .byte       0x0B
    .byte       0x66
    .byte       0x06
    .byte       0x50
    .byte       0xE8
    .byte       0x4C
    .byte       0x08
    .byte       0x02
    .byte       0xCE
    .byte       0x2F
    .byte       0xE6
    .byte       0xDA
    .byte       0x05
    .byte       0x4A
    .byte       0x0B
    .byte       0x2F
    .byte       0xD6
    .byte       0x6D
    .byte       0xF6
    .byte       0xAF
    .byte       0xCE
    .byte       0x6E
    .byte       0xF6
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x04
    .byte       0x1F
    .byte       0xDA
    .byte       0x06
    .byte       0x04
    .byte       0x21
    .byte       0x92
    .byte       0x06
    .byte       0x04
    .byte       0x58
    .byte       0xC6
    .byte       0x6E
    .byte       0xF6
    .byte       0xDD
    .byte       0x01
    .byte       0x4D
    .byte       0x2B
    .byte       0x4F
    .byte       0x26
    .byte       0x06
    .byte       0x04
    .byte       0x20
    .byte       0x04
    .byte       0x4F
    .byte       0x26
    .byte       0x00
    .byte       0x0B
    .byte       0x00
    .byte       0x09
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_sibu_init()

    .global obj_sibu_init
obj_sibu_init:
    mov.l       @r14, r0
    mov.l       @(0x0,r0), r1
    mov.l       @(0x4,r0), r2
    mov.l       r1, @(0x2c,r14)
    mov.l       r2, @(0x20,r14)
    mov.b       @(0x8,r14), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__0605d9d0
    shll8       r0
    bra         LAB_SONICWORLD__0605d9d2
    shlr2       r0

    .global LAB_SONICWORLD__0605d9d0
LAB_SONICWORLD__0605d9d0:
    mov.w       DAT_SONICWORLD__0605d9e2, r0

    .global LAB_SONICWORLD__0605d9d2
LAB_SONICWORLD__0605d9d2:
    mov.w       r0, @(0x14,r14)
    xor         r0, r0
    mov.w       r0, @(0x12,r14)
    mov         #0x1, r0
    mov.w       r0, @(0xe,r14)
    mov.l       PTR_obj_sibu_main_SONICWORLD__0605d9e4, r0
    rts         
    mov.l       r0, @-r4

    .global DAT_SONICWORLD__0605d9e2
DAT_SONICWORLD__0605d9e2:
    .word       0x0100

    .global PTR_obj_sibu_main_SONICWORLD__0605d9e4
PTR_obj_sibu_main_SONICWORLD__0605d9e4:
    .long       obj_sibu_main
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_sibu_main()

    .global obj_sibu_main
obj_sibu_main:
    mov.l       @(0x1e0,gbr), r0
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    sts         mach, r3
    add         #-0x10, r4
    mov.w       @(0xa,r4), r0
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605da24
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605da80, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605da84, r0
    jmp         @r0

    .global LAB_SONICWORLD__0605da24
LAB_SONICWORLD__0605da24:
    add         #0x10, r4
    mov.l       r14, @-r15
    sts.l       pr, @-r15
    mov.b       @(0x9,gbr), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__0605daa6
    mov.l       DAT_SONICWORLD__0605da88, r0
    mov.l       @(0x30,r14), r4
    mov.l       @(0x38,r14), r5
    add         r0, r4
    add         r0, r5
    shlr16      r4
    shlr8       r4
    shlr2       r4
    shlr16      r5
    shlr8       r5
    shlr2       r5
    shll        r5
    mov.l       PTR_visible_map_regions_SONICWORLD__0605da8c, r3
    add         r5, r3
    mov.w       @r3+, r0
    cmp/pz      r0
    bf          LAB_SONICWORLD__0605daa0
    exts.b      r0, r2
    swap.b      r0, r0
    exts.b      r0, r0
    cmp/hs      r0, r4
    bf          LAB_SONICWORLD__0605daa0
    cmp/hs      r4, r2
    bf          LAB_SONICWORLD__0605daa0
    mov.l       PTR_slPushMatrix_SONICWORLD__0605da90, r0
    jsr         @r0
    nop         
    mov.l       @(0x30,r14), r4
    mov.l       @(0x34,r14), r5
    mov.l       PTR_slTranslate_SONICWORLD__0605da94, r0
    jsr         @r0
    mov.l       @(0x38,r14), r6
    mov.l       PTR_draw_sprite_object_scaled_SONICWORLD__0605da98, r0
    jsr         @r0
    mov         r14, r1
    lds.l       @r15+, pr
    mov.l       PTR_slPopMatrix_SONICWORLD__0605da9c, r0
    jmp         @r0
    mov.l       @r15+, r14
    .byte       0x00
    .byte       0x00

    .global PTR_DAT_SONICWORLD__0605da80
PTR_DAT_SONICWORLD__0605da80:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605da84
PTR_obj_destructor_SONICWORLD__0605da84:
    .long       obj_destructor

    .global DAT_SONICWORLD__0605da88
DAT_SONICWORLD__0605da88:
    .long       0x10000000

    .global PTR_visible_map_regions_SONICWORLD__0605da8c
PTR_visible_map_regions_SONICWORLD__0605da8c:
    .long       visible_map_regions

    .global PTR_slPushMatrix_SONICWORLD__0605da90
PTR_slPushMatrix_SONICWORLD__0605da90:
    .long       slPushMatrix

    .global PTR_slTranslate_SONICWORLD__0605da94
PTR_slTranslate_SONICWORLD__0605da94:
    .long       slTranslate

    .global PTR_draw_sprite_object_scaled_SONICWORLD__0605da98
PTR_draw_sprite_object_scaled_SONICWORLD__0605da98:
    .long       draw_sprite_object_scaled

    .global PTR_slPopMatrix_SONICWORLD__0605da9c
PTR_slPopMatrix_SONICWORLD__0605da9c:
    .long       slPopMatrix

    .global LAB_SONICWORLD__0605daa0
LAB_SONICWORLD__0605daa0:
    lds.l       @r15+, pr
    rts         
    mov.l       @r15+, r14

    .global LAB_SONICWORLD__0605daa6
LAB_SONICWORLD__0605daa6:
    lds.l       @r15+, pr
    mov.l       @(0x20,r14), r5
    tst         r5, r5
    bt          LAB_SONICWORLD__0605dabc
    mov         r14, r4
    mov.l       PTR_obj_process_animation_tick_SONICWORLD__0605dab8, r0
    jmp         @r0
    mov.l       @r15+, r14
    ! -- Flow Override: CALL_RETURN (COMPUTED_CALL_TERMINATOR)
    .byte       0x00
    .byte       0x00

    .global PTR_obj_process_animation_tick_SONICWORLD__0605dab8
PTR_obj_process_animation_tick_SONICWORLD__0605dab8:
    .long       obj_process_animation_tick

    .global LAB_SONICWORLD__0605dabc
LAB_SONICWORLD__0605dabc:
    rts         
    mov.l       @r15+, r14
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_h_daiza_init()

    .global obj_h_daiza_init
obj_h_daiza_init:
    mov.l       @r14, r0
    mov.l       @(0x0,r0), r1
    mov.l       @(0xc,r0), r2
    mov.l       r1, @(0x20,r14)
    mov.l       r2, @(0x28,r14)
    mov.l       PTR_obj_h_daiza_main_SONICWORLD__0605dad0, r0
    rts         
    mov.l       r0, @-r4

    .global PTR_obj_h_daiza_main_SONICWORLD__0605dad0
PTR_obj_h_daiza_main_SONICWORLD__0605dad0:
    .long       obj_h_daiza_main
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_h_daiza_main()

    .global obj_h_daiza_main
obj_h_daiza_main:
    mov.l       @(0x1e0,gbr), r0
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    sts         mach, r3
    add         #-0x10, r4
    mov.w       @(0xa,r4), r0
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605db10
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605db20, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605db24, r0
    jmp         @r0

    .global LAB_SONICWORLD__0605db10
LAB_SONICWORLD__0605db10:
    add         #0x10, r4
    sts.l       pr, @-r15
    mov.l       PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605db28, r0
    jsr         @r0
    mov.l       @(0x28,r14), r7
    bra         LAB_SONICWORLD__0605d640
    nop         
    .byte       0x00
    .byte       0x00

    .global PTR_DAT_SONICWORLD__0605db20
PTR_DAT_SONICWORLD__0605db20:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605db24
PTR_obj_destructor_SONICWORLD__0605db24:
    .long       obj_destructor

    .global PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605db28
PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605db28:
    .long       sw_obj_collide_and_adjust_target
    .byte       0x60
    .byte       0xE2
    .byte       0x51
    .byte       0x00
    .byte       0x52
    .byte       0x03
    .byte       0x1E
    .byte       0x18
    .byte       0x1E
    .byte       0x2A
    .byte       0xD0
    .byte       0x01
    .byte       0x00
    .byte       0x0B
    .byte       0x24
    .byte       0x06
    .byte       0x06
    .byte       0x05
    .byte       0xDB
    .byte       0x40
    .byte       0xC6
    .byte       0x78
    .byte       0x51
    .byte       0x0C
    .byte       0x52
    .byte       0xEC
    .byte       0x32
    .byte       0x18
    .byte       0x2F
    .byte       0x26
    .byte       0x52
    .byte       0xEE
    .byte       0x50
    .byte       0x0E
    .byte       0x32
    .byte       0x08
    .byte       0x2F
    .byte       0x26
    .byte       0x00
    .byte       0x28
    .byte       0x63
    .byte       0xF3
    .byte       0x62
    .byte       0xF3
    .byte       0x02
    .byte       0x3F
    .byte       0x02
    .byte       0x3F
    .byte       0x7F
    .byte       0x08
    .byte       0x03
    .byte       0x0A
    .byte       0x74
    .byte       0xF0
    .byte       0x85
    .byte       0x45
    .byte       0x40
    .byte       0x28
    .byte       0x33
    .byte       0x06
    .byte       0x8B
    .byte       0x08
    .byte       0x85
    .byte       0x44
    .byte       0xD3
    .byte       0x07
    .byte       0x60
    .byte       0x0D
    .byte       0x33
    .byte       0x0C
    .byte       0x84
    .byte       0x30
    .byte       0xC9
    .byte       0xFE
    .byte       0x80
    .byte       0x30
    .byte       0xD0
    .byte       0x05
    .byte       0x40
    .byte       0x2B
    .byte       0x74
    .byte       0x10
    .byte       0x4F
    .byte       0x22
    .byte       0xD0
    .byte       0x04
    .byte       0x40
    .byte       0x0B
    .byte       0x57
    .byte       0xEA
    .byte       0xAD
    .byte       0x5B
    .byte       0x00
    .byte       0x09
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x0C
    .byte       0x00
    .byte       0x00
    .byte       0x06
    .byte       0x04
    .byte       0x2F
    .byte       0xA0
    .byte       0x06
    .byte       0x04
    .byte       0x33
    .byte       0x90
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_arch_init()

    .global obj_arch_init
obj_arch_init:
    mov.l       @r14, r0
    mov.l       @(0x0,r0), r1
    mov.l       @(0xc,r0), r2
    mov.l       r1, @(0x20,r14)
    mov.l       r2, @(0x28,r14)
    mov.l       PTR_obj_arch_main_SONICWORLD__0605dba8, r0
    rts         
    mov.l       r0, @-r4

    .global PTR_obj_arch_main_SONICWORLD__0605dba8
PTR_obj_arch_main_SONICWORLD__0605dba8:
    .long       obj_arch_main
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_arch_main()

    .global obj_arch_main
obj_arch_main:
    mov.l       @(0x1e0,gbr), r0
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    sts         mach, r3
    add         #-0x10, r4
    mov.w       @(0xa,r4), r0
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605dbe8
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605dbf8, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605dbfc, r0
    jmp         @r0

    .global LAB_SONICWORLD__0605dbe8
LAB_SONICWORLD__0605dbe8:
    add         #0x10, r4
    sts.l       pr, @-r15
    mov.l       PTR_FUN_SONICWORLD__060433ec_SONICWORLD__0605dc00, r0
    jsr         @r0
    mov.l       @(0x28,r14), r11
    bra         LAB_SONICWORLD__0605d640
    nop         
    .byte       0x00
    .byte       0x00

    .global PTR_DAT_SONICWORLD__0605dbf8
PTR_DAT_SONICWORLD__0605dbf8:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605dbfc
PTR_obj_destructor_SONICWORLD__0605dbfc:
    .long       obj_destructor

    .global PTR_FUN_SONICWORLD__060433ec_SONICWORLD__0605dc00
PTR_FUN_SONICWORLD__060433ec_SONICWORLD__0605dc00:
    .long       FUN_SONICWORLD__060433ec
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_happa_init()

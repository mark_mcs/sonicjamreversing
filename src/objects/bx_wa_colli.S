! *******************************************************************************
! * GENERATED FILE                                                              *
! * Changes to this file will be overwritten by the Ghidra export               *
! *******************************************************************************

    .include "bios.S"
    .include "defines.S"
    .include "kernel_exports.S"
    .include "platform.S"
    .include "sound_ids.S"
    .include "system_variables.S"
    .include "variables.S"

    .global obj_bxcolli_init
obj_bxcolli_init:
    mov.b       @(0x9,r14), r0
    shll16      r0
    shll2       r0
    shll2       r0
    mov.l       r0, @(0x24,r14)
    mov.l       PTR_obj_bxcolli_main_SONICWORLD__0605cd14, r0

    .global LAB_SONICWORLD__0605ccf0
LAB_SONICWORLD__0605ccf0:
    mov.l       r0, @-r15
    mov.l       @r14, r0
    mov.l       @(0xc,r0), r2
    mov.l       r2, @(0x0,r14)
    mov.b       @(0x8,r14), r0
    shll16      r0
    shll2       r0
    shll2       r0
    mov.l       r0, @(0x20,r14)
    mov.b       @(0xa,r14), r0
    shll16      r0
    shll2       r0
    shll2       r0
    mov.l       r0, @(0x28,r14)
    mov.l       @r15+, r0
    rts         
    mov.l       r0, @-r4
    .byte       0x00
    .byte       0x00

    .global PTR_obj_bxcolli_main_SONICWORLD__0605cd14
PTR_obj_bxcolli_main_SONICWORLD__0605cd14:
    .long       obj_bxcolli_main
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_bxcolli_main()

    .global obj_bxcolli_main
obj_bxcolli_main:
    mov.l       @(GBR_CAM_SCENE_TARGET_OBJ,gbr), r0
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    sts         mach, r3
    add         #-0x10, r4
    mov.w       @(0xa,r4), r0
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605cd54
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605cd68, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605cd6c, r0
    jmp         @r0

    .global LAB_SONICWORLD__0605cd54
LAB_SONICWORLD__0605cd54:
    add         #0x10, r4
    mov.l       @(0x0,r14), r7
    mov.l       @(0x20,r14), r0
    mov.l       @(0x24,r14), r2
    mov.l       @(0x28,r14), r3
    mov.l       r0, @(0x8,r7)
    mov.l       r2, @(0x14,r7)
    mov.l       PTR_FUN_SONICWORLD__06043390_SONICWORLD__0605cd70, r0
    jmp         @r0
    mov.l       r3, @(0xc,r7)

    .global PTR_DAT_SONICWORLD__0605cd68
PTR_DAT_SONICWORLD__0605cd68:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605cd6c
PTR_obj_destructor_SONICWORLD__0605cd6c:
    .long       obj_destructor

    .global PTR_FUN_SONICWORLD__06043390_SONICWORLD__0605cd70
PTR_FUN_SONICWORLD__06043390_SONICWORLD__0605cd70:
    .long       FUN_SONICWORLD__06043390
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_wacolli_init()

    .global obj_wacolli_init
obj_wacolli_init:
    mov.b       @(0x9,r14), r0
    shll16      r0
    shll2       r0
    shll2       r0
    mov.l       r0, @(0x24,r14)
    mov.l       PTR_obj_wacolli_main_SONICWORLD__0605cd84, r0
    bra         LAB_SONICWORLD__0605ccf0
    nop         

    .global PTR_obj_wacolli_main_SONICWORLD__0605cd84
PTR_obj_wacolli_main_SONICWORLD__0605cd84:
    .long       obj_wacolli_main
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined obj_wacolli_main()

    .global obj_wacolli_main
obj_wacolli_main:
    mov.l       @(0x1e0,gbr), r0
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    sts         mach, r3
    add         #-0x10, r4
    mov.w       @(0xa,r4), r0
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605cdc4
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605cdd8, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605cddc, r0
    jmp         @r0

    .global LAB_SONICWORLD__0605cdc4
LAB_SONICWORLD__0605cdc4:
    add         #0x10, r4
    mov.l       @(0x0,r14), r7
    mov.l       @(0x20,r14), r0
    mov.l       @(0x28,r14), r3
    neg         r0, r4
    neg         r3, r6
    mov.l       r0, @(0x8,r7)
    mov.l       PTR_LAB_SONICWORLD__0604344a_SONICWORLD__0605cde0, r0
    jmp         @r0
    mov.l       r3, @(0xc,r7)

    .global PTR_DAT_SONICWORLD__0605cdd8
PTR_DAT_SONICWORLD__0605cdd8:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605cddc
PTR_obj_destructor_SONICWORLD__0605cddc:
    .long       obj_destructor

    .global PTR_LAB_SONICWORLD__0604344a_SONICWORLD__0605cde0
PTR_LAB_SONICWORLD__0604344a_SONICWORLD__0605cde0:
    .long       LAB_SONICWORLD__0604344a
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined FUN_SONICWORLD__0605cde4()

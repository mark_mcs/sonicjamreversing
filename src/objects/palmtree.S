! *******************************************************************************
! * GENERATED FILE                                                              *
! * Changes to this file will be overwritten by the Ghidra export               *
! *******************************************************************************

    .include "bios.S"
    .include "defines.S"
    .include "kernel_exports.S"
    .include "platform.S"
    .include "sound_ids.S"
    .include "system_variables.S"
    .include "variables.S"

    .global obj_palmtree_init
obj_palmtree_init:
    mov.l       @r14, r0
    mov.l       @(0x0,r0), r1
    mov.l       @(0x4,r0), r2
    mov.l       @(0xc,r0), r3
    mov.l       r1, @(0x20,r14)
    mov.l       r2, @(0x24,r14)
    mov.l       r3, @(0x0,r14)
    xor         r0, r0
    mov.w       r0, @(0x4,r14)
    mov.b       r0, @(0x6,r14)
    mov.l       PTR_obj_palmtree_main_SONICWORLD__0605c80c, r0
    rts         
    mov.l       r0, @-r4
    .byte       0x00
    .byte       0x00

    .global PTR_obj_palmtree_main_SONICWORLD__0605c80c
PTR_obj_palmtree_main_SONICWORLD__0605c80c:
    .long       obj_palmtree_main
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! void __stdcall obj_palmtree_main(ObjLevelObject * obj)

    .global obj_palmtree_main
obj_palmtree_main:
    mov.l       @(GBR_CAM_SCENE_TARGET_OBJ,gbr), r0
    ! calculate the distance from the target object
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    ! r3 = x^2 + y^2
    sts         mach, r3
    add         #-0x10, r4
    mov.w       @(0xa,r4), r0
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605c84c
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605c864, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605c868, r0
    jmp         @r0

    .global LAB_SONICWORLD__0605c84c
LAB_SONICWORLD__0605c84c:
    add         #0x10, r4
    sts.l       pr, @-r15
    mov.l       PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605c86c, r0
    jsr         @r0
    mov.l       @(0x0,r14), r7
    cmp/eq      #0x1, r0
    bt          LAB_SONICWORLD__0605c870
    xor         r0, r0
    mov.b       r0, @(0x6,r14)
    bra         LAB_SONICWORLD__0605c880
    xor         r2, r2
    .byte       0x00
    .byte       0x00

    .global PTR_DAT_SONICWORLD__0605c864
PTR_DAT_SONICWORLD__0605c864:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605c868
PTR_obj_destructor_SONICWORLD__0605c868:
    .long       obj_destructor

    .global PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605c86c
PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605c86c:
    .long       sw_obj_collide_and_adjust_target

    .global LAB_SONICWORLD__0605c870
LAB_SONICWORLD__0605c870:
    mov.b       @(0x6,r14), r0
    tst         r0, r0
    bf          LAB_SONICWORLD__0605c87e
    mov         #0x1, r0
    mov.b       r0, @(0x6,r14)
    mov.w       DAT_SONICWORLD__0605c908, r0
    mov.w       r0, @(0x4,r14)

    .global LAB_SONICWORLD__0605c87e
LAB_SONICWORLD__0605c87e:
    mov         #0x1, r2

    .global LAB_SONICWORLD__0605c880
LAB_SONICWORLD__0605c880:
    mov.b       @(GBR_026,gbr), r0
    or          r2, r0
    mov.b       r0, @(GBR_026,gbr)
    mov.b       @(GBR_FRAME_PHASE,gbr), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__0605c934
    mov.l       DAT_SONICWORLD__0605c90c, r0
    mov.l       @(0x30,r14), r4
    mov.l       @(0x38,r14), r5
    add         r0, r4
    add         r0, r5
    shlr16      r4
    shlr8       r4
    shlr2       r4
    shlr16      r5
    shlr8       r5
    shlr2       r5
    shll        r5
    mov.l       PTR_visible_map_regions_SONICWORLD__0605c910, r3
    add         r5, r3
    mov.w       @r3+, r0
    cmp/pz      r0
    bf          LAB_SONICWORLD__0605c934
    exts.b      r0, r2
    swap.b      r0, r0
    exts.b      r0, r0
    cmp/hs      r0, r4
    bf          LAB_SONICWORLD__0605c934
    cmp/hs      r4, r2
    bf          LAB_SONICWORLD__0605c934
    mov.l       r14, @-r15
    mov.l       PTR_slPushMatrix_SONICWORLD__0605c914, r0
    jsr         @r0
    mov         r14, r1
    mov.l       @(0x30,r1), r4
    mov.l       @(0x34,r1), r5
    mov.l       PTR_slTranslate_SONICWORLD__0605c918, r13
    jsr         @r13
    mov.l       @(0x38,r1), r6
    mov.l       PTR_sw_render_billboard_obj_SONICWORLD__0605c91c, r0
    jsr         @r0
    mov.l       @(0x24,r1), r2
    mov.w       @(0x4,r1), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__0605c8fa
    mov.l       PTR_get_sin_r10_SONICWORLD__0605c920, r2
    jsr         @r2
    shll        r0
    shll2       r10
    mov.l       DAT_SONICWORLD__0605c924, r0
    dmuls.l     r10, r0
    sts         mach, r10
    sts         macl, r0
    xtrct       r10, r0
    mov.l       PTR_slRotY_SONICWORLD__0605c928, r2
    jsr         @r2
    nop         
    mov.w       @(0x4,r1), r0
    mov.w       DAT_SONICWORLD__0605c90a, r2
    sub         r2, r0
    mov.w       r0, @(0x4,r1)

    .global LAB_SONICWORLD__0605c8fa
LAB_SONICWORLD__0605c8fa:
    mov.l       PTR_sw_render_model_small_SONICWORLD__0605c92c, r0
    jsr         @r0
    mov.l       @(0x20,r1), r2
    mov.l       @r15+, r14
    mov.l       PTR_slPopMatrix_SONICWORLD__0605c930, r13
    jmp         @r13
    lds.l       @r15+, pr

    .global DAT_SONICWORLD__0605c908
DAT_SONICWORLD__0605c908:
    .word       0xF000

    .global DAT_SONICWORLD__0605c90a
DAT_SONICWORLD__0605c90a:
    .word       0x1000

    .global DAT_SONICWORLD__0605c90c
DAT_SONICWORLD__0605c90c:
    .long       0x10000000

    .global PTR_visible_map_regions_SONICWORLD__0605c910
PTR_visible_map_regions_SONICWORLD__0605c910:
    .long       visible_map_regions

    .global PTR_slPushMatrix_SONICWORLD__0605c914
PTR_slPushMatrix_SONICWORLD__0605c914:
    .long       slPushMatrix

    .global PTR_slTranslate_SONICWORLD__0605c918
PTR_slTranslate_SONICWORLD__0605c918:
    .long       slTranslate

    .global PTR_sw_render_billboard_obj_SONICWORLD__0605c91c
PTR_sw_render_billboard_obj_SONICWORLD__0605c91c:
    .long       sw_render_billboard_obj

    .global PTR_get_sin_r10_SONICWORLD__0605c920
PTR_get_sin_r10_SONICWORLD__0605c920:
    .long       get_sin_r10

    .global DAT_SONICWORLD__0605c924
DAT_SONICWORLD__0605c924:
    .long       0x00000200

    .global PTR_slRotY_SONICWORLD__0605c928
PTR_slRotY_SONICWORLD__0605c928:
    .long       slRotY

    .global PTR_sw_render_model_small_SONICWORLD__0605c92c
PTR_sw_render_model_small_SONICWORLD__0605c92c:
    .long       sw_render_model_small

    .global PTR_slPopMatrix_SONICWORLD__0605c930
PTR_slPopMatrix_SONICWORLD__0605c930:
    .long       slPopMatrix

    .global LAB_SONICWORLD__0605c934
LAB_SONICWORLD__0605c934:
    lds.l       @r15+, pr
    rts         
    nop         
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! void __stdcall obj_totem_init(ObjLevelObject * obj)

    .global obj_totem_init
obj_totem_init:
    sts.l       pr, @-r15
    mov.l       @r14, r0
    mov.l       @(0x0,r0), r1
    mov.l       @(0x4,r0), r2
    mov.l       @(0xc,r0), r3
    mov.l       r1, @(0x24,r14)
    mov.l       r2, @(0x20,r14)
    mov.l       r3, @(0x28,r14)
    mov.l       PTR_sw_random_int_SONICWORLD__0605c968, r0
    jsr         @r0
    nop         
    mov.w       r0, @(0x0,r14)
    ! random rotation speed
    mov.l       PTR_sw_random_int_SONICWORLD__0605c968, r0
    jsr         @r0
    nop         
    extu.w      r0, r0
    mov.l       r0, @(0x4,r14)
    ! collision flag?
    xor         r0, r0
    mov.b       r0, @(0xa,r14)
    lds.l       @r15+, pr
    mov.l       PTR_obj_totem_main_SONICWORLD__0605c96c, r0
    rts         
    mov.l       r0, @-r4

    .global PTR_sw_random_int_SONICWORLD__0605c968
PTR_sw_random_int_SONICWORLD__0605c968:
    .long       sw_random_int

    .global PTR_obj_totem_main_SONICWORLD__0605c96c
PTR_obj_totem_main_SONICWORLD__0605c96c:
    .long       obj_totem_main
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! void __stdcall obj_totem_main(ObjLevelObject * obj_r4, O

    .global obj_totem_main
obj_totem_main:
    mov.l       @(GBR_CAM_SCENE_TARGET_OBJ,gbr), r0
    ! get x distance
    mov.l       @(0x30,r0), r1
    mov.l       @(0x30,r14), r2
    sub         r1, r2
    mov.l       r2, @-r15
    ! get z distance
    mov.l       @(0x38,r14), r2
    mov.l       @(0x38,r0), r0
    sub         r0, r2
    mov.l       r2, @-r15
    clrmac      
    mov         r15, r3
    mov         r15, r2
    mac.l       @r3+, @r2+
    mac.l       @r3+, @r2+
    add         #0x8, r15
    ! r3 = x*x + y*y
    sts         mach, r3
    ! r4 = pointer to the object header
    add         #-0x10, r4
    ! r0 = object->activation_radius
    mov.w       @(0xa,r4), r0
    ! is the target object within the activation radius?
    shll16      r0
    cmp/hi      r0, r3
    bf          LAB_SONICWORLD__0605c9ac
    ! outside the radius - unload this object
    ! r0 = offset to this object in the level layout data
    mov.w       @(0x8,r4), r0
    mov.l       PTR_DAT_SONICWORLD__0605c9c0, r3
    extu.w      r0, r0
    add         r0, r3
    mov.b       @(0x0,r3), r0
    ! clear the active flag
    and         #0xfe, r0
    mov.b       r0, @(0x0,r3)
    mov.l       PTR_obj_destructor_SONICWORLD__0605c9c4, r0
    jmp         @r0
    ! the target object is still within the activation radius

    .global LAB_SONICWORLD__0605c9ac
LAB_SONICWORLD__0605c9ac:
    add         #0x10, r4
    sts.l       pr, @-r15
    mov.l       PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605c9c8, r0
    jsr         @r0
    mov.l       @(0x28,r14), r7
    cmp/eq      #0x1, r0
    bt          LAB_SONICWORLD__0605c9cc
    bra         LAB_SONICWORLD__0605c9ce
    mov         #0x0, r0
    .byte       0x00
    .byte       0x00

    .global PTR_DAT_SONICWORLD__0605c9c0
PTR_DAT_SONICWORLD__0605c9c0:
    .long       DAT_060c0000

    .global PTR_obj_destructor_SONICWORLD__0605c9c4
PTR_obj_destructor_SONICWORLD__0605c9c4:
    .long       obj_destructor

    .global PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605c9c8
PTR_sw_obj_collide_and_adjust_target_SONICWORLD__0605c9c8:
    .long       sw_obj_collide_and_adjust_target

    .global LAB_SONICWORLD__0605c9cc
LAB_SONICWORLD__0605c9cc:
    mov         #0x1, r0

    .global LAB_SONICWORLD__0605c9ce
LAB_SONICWORLD__0605c9ce:
    mov.b       r0, @(0xa,r14)
    mov.b       @(GBR_FRAME_PHASE,gbr), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__0605ca5e
    mov.l       DAT_SONICWORLD__0605ca38, r0
    mov.l       @(0x30,r14), r4
    mov.l       @(0x38,r14), r5
    add         r0, r4
    add         r0, r5
    shlr16      r4
    shlr8       r4
    shlr2       r4
    shlr16      r5
    shlr8       r5
    shlr2       r5
    shll        r5
    mov.l       PTR_visible_map_regions_SONICWORLD__0605ca3c, r3
    add         r5, r3
    mov.w       @r3+, r0
    cmp/pz      r0
    bf          LAB_SONICWORLD__0605ca58
    exts.b      r0, r2
    swap.b      r0, r0
    exts.b      r0, r0
    cmp/hs      r0, r4
    bf          LAB_SONICWORLD__0605ca58
    cmp/hs      r4, r2
    bf          LAB_SONICWORLD__0605ca58
    mov.l       r14, @-r15
    mov.l       PTR_slPushMatrix_SONICWORLD__0605ca40, r0
    jsr         @r0
    nop         
    mov.l       @(0x30,r14), r4
    mov.l       @(0x34,r14), r5
    mov.l       PTR_slTranslate_SONICWORLD__0605ca44, r0
    jsr         @r0
    mov.l       @(0x38,r14), r6
    mov.l       r14, @-r15
    ! render bottom part as a billboard
    mov.l       @(0x20,r14), r2
    mov.l       PTR_sw_render_billboard_obj_SONICWORLD__0605ca48, r0
    jsr         @r0
    mov         r14, r1
    ! render the spinning top part
    mov.l       @r15+, r14
    mov.l       PTR_slRotY_SONICWORLD__0605ca4c, r2
    jsr         @r2
    mov.w       @(0x2,r14), r0
    mov.l       PTR_sw_render_model_small_SONICWORLD__0605ca50, r0
    jsr         @r0
    mov.l       @(0x24,r14), r2
    mov.l       @r15+, r14
    mov.l       PTR_slPopMatrix_SONICWORLD__0605ca54, r0
    jmp         @r0
    lds.l       @r15+, pr

    .global DAT_SONICWORLD__0605ca38
DAT_SONICWORLD__0605ca38:
    .long       0x10000000

    .global PTR_visible_map_regions_SONICWORLD__0605ca3c
PTR_visible_map_regions_SONICWORLD__0605ca3c:
    .long       visible_map_regions

    .global PTR_slPushMatrix_SONICWORLD__0605ca40
PTR_slPushMatrix_SONICWORLD__0605ca40:
    .long       slPushMatrix

    .global PTR_slTranslate_SONICWORLD__0605ca44
PTR_slTranslate_SONICWORLD__0605ca44:
    .long       slTranslate

    .global PTR_sw_render_billboard_obj_SONICWORLD__0605ca48
PTR_sw_render_billboard_obj_SONICWORLD__0605ca48:
    .long       sw_render_billboard_obj

    .global PTR_slRotY_SONICWORLD__0605ca4c
PTR_slRotY_SONICWORLD__0605ca4c:
    .long       slRotY

    .global PTR_sw_render_model_small_SONICWORLD__0605ca50
PTR_sw_render_model_small_SONICWORLD__0605ca50:
    .long       sw_render_model_small

    .global PTR_slPopMatrix_SONICWORLD__0605ca54
PTR_slPopMatrix_SONICWORLD__0605ca54:
    .long       slPopMatrix

    .global LAB_SONICWORLD__0605ca58
LAB_SONICWORLD__0605ca58:
    lds.l       @r15+, pr
    rts         
    nop         

    .global LAB_SONICWORLD__0605ca5e
LAB_SONICWORLD__0605ca5e:
    mov.w       DAT_SONICWORLD__0605cad8, r2
    ! update the rotation angle
    mov.l       @(0x4,r14), r3
    dmuls.l     r3, r2
    sts         mach, r3
    sts         macl, r2
    xtrct       r3, r2
    mov.w       @(0x2,r14), r0
    add         r2, r0
    mov.w       r0, @(0x2,r14)
    ! is the player standing on the totem?
    mov.b       @(0xa,r14), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__0605cad2
    mov.l       @(GBR_CAM_SCENE_TARGET_OBJ,gbr), r0
    mov         r0, r3
    mov.l       @(0x30,r3), r4
    mov.l       @(0x38,r3), r5
    mov.l       @(0x30,r14), r6
    mov.l       @(0x38,r14), r7
    ! update the player model rotation...
    mov.w       @(0x1a,r3), r0
    sub         r2, r0
    mov.w       r0, @(0x1a,r3)
    ! ...and position on the totem
    sub         r6, r4
    sub         r7, r5
    mov.l       PTR__slGetSinCos2_SONICWORLD__0605cadc, r3
    jsr         @r3
    neg         r2, r0
    shll2       r10
    shll2       r11
    neg         r10, r12
    mov.l       r4, @-r15
    mov.l       r5, @-r15
    mov         r15, r1
    mov.l       r11, @-r15
    mov.l       r12, @-r15
    mov         r15, r2
    clrmac      
    mac.l       @r1+, @r2+
    mac.l       @r1+, @r2+
    mov         r2, r15
    sts         mach, r0
    sts         macl, r4
    xtrct       r0, r4
    add         #-0x8, r1
    mov.l       r10, @-r15
    mov.l       r11, @-r15
    mov         r15, r2
    clrmac      
    mac.l       @r1+, @r2+
    mac.l       @r1+, @r2+
    sts         mach, r0
    sts         macl, r5
    xtrct       r0, r5
    add         #0x10, r15
    mov.l       @(GBR_CAM_SCENE_TARGET_OBJ,gbr), r0
    add         r6, r4
    add         r7, r5
    mov.l       r4, @(0x30,r0)
    mov.l       r5, @(0x38,r0)

    .global LAB_SONICWORLD__0605cad2
LAB_SONICWORLD__0605cad2:
    lds.l       @r15+, pr
    rts         
    nop         

    .global DAT_SONICWORLD__0605cad8
DAT_SONICWORLD__0605cad8:
    .word       0x0800
    .byte       0x00
    .byte       0x00

    .global PTR__slGetSinCos2_SONICWORLD__0605cadc
PTR__slGetSinCos2_SONICWORLD__0605cadc:
    .long       _slGetSinCos2

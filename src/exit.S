! *******************************************************************************
! * GENERATED FILE                                                              *
! * Changes to this file will be overwritten by the Ghidra export               *
! *******************************************************************************

    .include "bios.S"
    .include "defines.S"
    .include "kernel_exports.S"
    .include "platform.S"
    .include "sound_ids.S"
    .include "system_variables.S"
    .include "variables.S"

    .global sw_exit
sw_exit:
    sts.l       pr, @-r15
    mov.l       PTR___thunk_sw_turn_off_slave_cpu_SONICWORLD__06040178, r0
    jsr         @r0
    nop         
    mov.l       PTR_VDP1_REG_TVMR_SONICWORLD__0604017c, r4
    mov         #0x0, r0
    mov.w       r0, @r4
    add         #0x2, r0
    mov         #0x2, r0
    mov.w       r0, @r4
    mov.b       @(GBR_MISSION_ACTIVE_FLAG,gbr), r0
    cmp/eq      #0x0, r0
    bt          LAB_SONICWORLD__06040188
    mov.b       @(GBR_049,gbr), r0
    tst         r0, r0
    bt          LAB_SONICWORLD__06040188
    mov.l       PTR_saved_stack_ptr_SONICWORLD__06040180, r0
    mov.l       @r0, r15
    lds.l       @r15+, pr
    mov.l       PTR__start_SONICWORLD__06040184, r0
    jmp         @r0
    xor         r4, r4

    .global PTR___thunk_sw_turn_off_slave_cpu_SONICWORLD__06040178
PTR___thunk_sw_turn_off_slave_cpu_SONICWORLD__06040178:
    .long       __thunk_sw_turn_off_slave_cpu

    .global PTR_VDP1_REG_TVMR_SONICWORLD__0604017c
PTR_VDP1_REG_TVMR_SONICWORLD__0604017c:
    .long       VDP1_REG_TVMR

    .global PTR_saved_stack_ptr_SONICWORLD__06040180
PTR_saved_stack_ptr_SONICWORLD__06040180:
    .long       saved_stack_ptr

    .global PTR__start_SONICWORLD__06040184
PTR__start_SONICWORLD__06040184:
    .long       _start

    .global LAB_SONICWORLD__06040188
LAB_SONICWORLD__06040188:
    mov.l       PTR_saved_stack_ptr_SONICWORLD__06040194, r0
    mov.l       @r0, r15
    lds.l       @r15+, pr
    rts         
    mov.b       @(GBR_049,gbr), r0
    .byte       0x00
    .byte       0x00

    .global PTR_saved_stack_ptr_SONICWORLD__06040194
PTR_saved_stack_ptr_SONICWORLD__06040194:
    .long       saved_stack_ptr

    .global LAB_SONICWORLD__06040198
LAB_SONICWORLD__06040198:
    mov.l       PTR_DAT_SONICWORLD__060401cc, r4
    mov.b       @r4+, r0
    mov.b       @r4, r1
    or          r1, r0
    tst         r0, r0
    bt          LAB_SONICWORLD__060401b0
    mov.l       PTR_FUN_SONICWORLD__060401d0, r0
    jsr         @r0
    nop         
    mov.l       PTR_FUN_SONICWORLD__060401d4, r0
    jsr         @r0
    nop         

    .global LAB_SONICWORLD__060401b0
LAB_SONICWORLD__060401b0:
    mov.l       PTR___thunk_sw_turn_off_slave_cpu_SONICWORLD__060401d8, r0
    jsr         @r0
    nop         
    mov.l       PTR_write_save_file_SONICWORLD__060401dc, r0
    jsr         @r0
    nop         
    mov.l       r0, @-r15
    stc         sr, r0
    or          #0xf0, r0
    ldc         r0, sr
    mov.l       @r15+, r0
    mov.l       PTR_LAB_SONICWORLD__060401e0, r0
    jmp         @r0
    nop         

    .global PTR_DAT_SONICWORLD__060401cc
PTR_DAT_SONICWORLD__060401cc:
    .long       DAT_06018112

    .global PTR_FUN_SONICWORLD__060401d0
PTR_FUN_SONICWORLD__060401d0:
    .long       FUN_0600d68c

    .global PTR_FUN_SONICWORLD__060401d4
PTR_FUN_SONICWORLD__060401d4:
    .long       FUN_0600d67a

    .global PTR___thunk_sw_turn_off_slave_cpu_SONICWORLD__060401d8
PTR___thunk_sw_turn_off_slave_cpu_SONICWORLD__060401d8:
    .long       __thunk_sw_turn_off_slave_cpu

    .global PTR_write_save_file_SONICWORLD__060401dc
PTR_write_save_file_SONICWORLD__060401dc:
    .long       write_save_file

    .global PTR_LAB_SONICWORLD__060401e0
PTR_LAB_SONICWORLD__060401e0:
    .long       LAB_06028028
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined sw_update_main_object_lists()

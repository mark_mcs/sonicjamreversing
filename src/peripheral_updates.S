    .include "bios.S"
    .include "defines.S"
    .include "input_defs.S"
    .include "kernel_exports.S"
    .include "platform.S"
    .include "sound_ids.S"
    .include "system_variables.S"
    .include "variables.S"

    .text

    .global FUN_SONICWORLD__06040fe0
FUN_SONICWORLD__06040fe0:
    mov.l       r13, @-r15
    mov         #0x0, r4
    mov.l       PTR_DAT_SONICWORLD__0604117c, r1
    mov         r4, r5
    mov.l       PTR__menu_input_flags_SONICWORLD__06041178, r13
    mov.l       r12, @-r15
    mov.l       r11, @-r15
    mov.l       r10, @-r15
    mov.l       r9, @-r15
    mov.l       r8, @-r15
    mov.l       PTR_DAT_SONICWORLD__06041164, r8
    mov.l       PTR_DAT_SONICWORLD__06041168, r9
    mov.l       PTR_DAT_SONICWORLD__0604116c, r10
    mov.l       PTR_DAT_SONICWORLD__06041170, r11
    mov.l       PTR_DAT_SONICWORLD__06041174, r12

    .global LAB_SONICWORLD__06040ffe
LAB_SONICWORLD__06040ffe:
    extu.b      r5, r7
    shll        r7
    mov         r1, r0
    mov.w       r4, @(r0,r7)
    extu.b      r5, r6
    mov         r13, r0
    mov.w       r4, @(r0,r7)
    mov         r12, r0
    mov.w       r4, @(r0,r7)
    mov         r11, r0
    mov.b       r4, @(r0,r6)
    mov         r10, r0
    mov.w       r4, @(r0,r7)
    mov         r9, r0
    mov.b       r4, @(r0,r6)
    mov         r8, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041180, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041184, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041188, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__0604118c, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041190, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041194, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041198, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__0604119c, r0
    add         #0x1, r5
    mov.b       r4, @(r0,r6)
    extu.b      r5, r7
    mov.l       PTR_DAT_SONICWORLD__060411a0, r0
    shll        r7
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__060411a4, r0
    mov.b       r4, @(r0,r6)
    mov         r1, r0
    mov.w       r4, @(r0,r7)
    extu.b      r5, r6
    mov         r13, r0
    mov.w       r4, @(r0,r7)
    mov         r12, r0
    mov.w       r4, @(r0,r7)
    mov         r11, r0
    mov.b       r4, @(r0,r6)
    mov         r10, r0
    mov.w       r4, @(r0,r7)
    mov         r9, r0
    mov.b       r4, @(r0,r6)
    mov         r8, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041180, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041184, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041188, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__0604118c, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041190, r0
    add         #0x1, r5
    mov.b       r4, @(r0,r6)
    extu.b      r5, r7
    mov.l       PTR_DAT_SONICWORLD__06041194, r0
    shll        r7
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041198, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__0604119c, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__060411a0, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__060411a4, r0
    mov.b       r4, @(r0,r6)
    mov         r1, r0
    mov.w       r4, @(r0,r7)
    extu.b      r5, r6
    mov         r13, r0
    mov.w       r4, @(r0,r7)
    mov         r12, r0
    mov.w       r4, @(r0,r7)
    mov         r11, r0
    mov.b       r4, @(r0,r6)
    mov         r10, r0
    mov.w       r4, @(r0,r7)
    mov         r9, r0
    mov.b       r4, @(r0,r6)
    mov         r8, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041180, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041184, r0
    add         #0x1, r5
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041188, r0
    mov.w       r4, @(r0,r7)
    extu.b      r5, r7
    mov.l       PTR_DAT_SONICWORLD__0604118c, r0
    shll        r7
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041190, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041194, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041198, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__0604119c, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__060411a0, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__060411a4, r0
    mov.b       r4, @(r0,r6)
    mov         r1, r0
    mov.w       r4, @(r0,r7)
    extu.b      r5, r6
    mov         r13, r0
    mov.w       r4, @(r0,r7)
    mov         r12, r0
    mov.w       r4, @(r0,r7)
    mov         r11, r0
    mov.b       r4, @(r0,r6)
    mov         r10, r0
    mov.w       r4, @(r0,r7)
    mov         r9, r0
    add         #0x1, r5
    mov.b       r4, @(r0,r6)
    mov         #0xc, r2
    mov         r8, r0
    extu.b      r5, r3
    mov.w       r4, @(r0,r7)
    cmp/ge      r2, r3
    mov.l       PTR_DAT_SONICWORLD__06041180, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041184, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__06041188, r0
    mov.w       r4, @(r0,r7)
    mov.l       PTR_DAT_SONICWORLD__0604118c, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041190, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041194, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__06041198, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__0604119c, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__060411a0, r0
    mov.b       r4, @(r0,r6)
    mov.l       PTR_DAT_SONICWORLD__060411a4, r0
    bt/s        LAB_SONICWORLD__0604113a
    mov.b       r4, @(r0,r6)
    bra         LAB_SONICWORLD__06040ffe
    nop         

    .global LAB_SONICWORLD__0604113a
LAB_SONICWORLD__0604113a:
    mov         r4, r0
    mov.l       PTR_peripheral_update_in_progress_SONICWORLD__060411ac, r3
    mov.l       PTR_DAT_SONICWORLD__060411a8, r5
    mov.b       r4, @r5
    mov.b       r0, @(0x1,r5)
    mov.b       r0, @r3
    mov.l       @r15+, r8
    mov.l       @r15+, r9
    mov.l       @r15+, r10
    mov.l       @r15+, r11
    mov.l       @r15+, r12
    rts         
    mov.l       @r15+, r13
    .byte       0xD5
    .byte       0x14
    .byte       0xE4
    .byte       0x00
    .byte       0xD3
    .byte       0x14
    .byte       0x60
    .byte       0x43
    .byte       0x25
    .byte       0x40
    .byte       0x80
    .byte       0x51
    .byte       0x00
    .byte       0x0B
    .byte       0x23
    .byte       0x00

    .global PTR_DAT_SONICWORLD__06041164
PTR_DAT_SONICWORLD__06041164:
    .long       DAT_060f4aa2

    .global PTR_DAT_SONICWORLD__06041168
PTR_DAT_SONICWORLD__06041168:
    .long       DAT_060f4a96

    .global PTR_DAT_SONICWORLD__0604116c
PTR_DAT_SONICWORLD__0604116c:
    .long       DAT_060f4a7e

    .global PTR_DAT_SONICWORLD__06041170
PTR_DAT_SONICWORLD__06041170:
    .long       DAT_060f4a72

    .global PTR_DAT_SONICWORLD__06041174
PTR_DAT_SONICWORLD__06041174:
    .long       DAT_060f4a5a

    .global PTR__menu_input_flags_SONICWORLD__06041178
PTR__menu_input_flags_SONICWORLD__06041178:
    .long       _menu_input_flags

    .global PTR_DAT_SONICWORLD__0604117c
PTR_DAT_SONICWORLD__0604117c:
    .long       DAT_060f4a12

    .global PTR_DAT_SONICWORLD__06041180
PTR_DAT_SONICWORLD__06041180:
    .long       DAT_060f4aba

    .global PTR_DAT_SONICWORLD__06041184
PTR_DAT_SONICWORLD__06041184:
    .long       DAT_060f4ad2

    .global PTR_DAT_SONICWORLD__06041188
PTR_DAT_SONICWORLD__06041188:
    .long       DAT_060f4aea

    .global PTR_DAT_SONICWORLD__0604118c
PTR_DAT_SONICWORLD__0604118c:
    .long       DAT_060f4b02

    .global PTR_DAT_SONICWORLD__06041190
PTR_DAT_SONICWORLD__06041190:
    .long       DAT_060f4b0e

    .global PTR_DAT_SONICWORLD__06041194
PTR_DAT_SONICWORLD__06041194:
    .long       DAT_060f4b1a

    .global PTR_DAT_SONICWORLD__06041198
PTR_DAT_SONICWORLD__06041198:
    .long       DAT_060f4b26

    .global PTR_DAT_SONICWORLD__0604119c
PTR_DAT_SONICWORLD__0604119c:
    .long       DAT_060f4b32

    .global PTR_DAT_SONICWORLD__060411a0
PTR_DAT_SONICWORLD__060411a0:
    .long       DAT_060f4b3e

    .global PTR_DAT_SONICWORLD__060411a4
PTR_DAT_SONICWORLD__060411a4:
    .long       DAT_060f4b4a

    .global PTR_DAT_SONICWORLD__060411a8
PTR_DAT_SONICWORLD__060411a8:
    .long       DAT_060f4b56

    .global PTR_peripheral_update_in_progress_SONICWORLD__060411ac
PTR_peripheral_update_in_progress_SONICWORLD__060411ac:
    .long       peripheral_update_in_progress
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! void __stdcall smpc_start_pad_data_fetch(void)
    ! Issues an INTBACK request to the SMPC to fetch pad data.
    ! Optionally includes SMPC status info if a global flag is set.

    .global smpc_start_pad_data_fetch
smpc_start_pad_data_fetch:
    ! check if there's already a peripheral update in progress
    mov.l       .L_peripheral_update_in_progress__60412a0, r5
    mov.b       @r5, r0
    extu.b      r0, r0
    cmp/eq      #0x1, r0
    ! bail out if there is
    bt          LAB_SONICWORLD__06041214
    ! no update in progress so we'll start one.
    mov         #0x1, r4
    mov.l       PTR__pad_input_buffer_block_idx_SONICWORLD__060412a4, r2
    mov.b       r4, @r5
    mov.w       DAT_SONICWORLD__06041298, r6
    mov         #0x0, r5
    mov.b       r5, @r2
    ! waste some time
    ! for (int i=1000; i>0; --i);

    .global LAB_SONICWORLD__060411c6
LAB_SONICWORLD__060411c6:
    add         #-0x1, r6
    extu.w      r6, r0
    tst         r0, r0
    bf          LAB_SONICWORLD__060411c6
    ! wait for the SMPC to finish its last command
    mov.l       PTR_SMPC_REG_SF_SONICWORLD__060412a8, r6

    .global LAB_SONICWORLD__060411d0
LAB_SONICWORLD__060411d0:
    mov.b       @r6, r2
    extu.b      r2, r2
    and         r4, r2
    cmp/eq      r4, r2
    bt          LAB_SONICWORLD__060411d0
    ! start a new SMPC command
    mov.b       r4, @r6
    ! set control ports 1 & 2 to SMPC control mode
    mov.l       PTR_SMPC_IOSEL_SONICWORLD__060412ac, r2
    mov.b       r5, @r2
    mov.l       .L_intback_last_ireg0, r3
    mov.b       r5, @r3
    mov.l       .L_SMPC_IREG0__060412b4, r7
    ! check the flag to see if we need to include SMPC status info
    mov.l       .L_g_smpc_request_status_update, r6
    mov.l       @r6, r0
    tst         r0, r0
    bt/s        LAB_SONICWORLD__06041202
    mov         #0x8, r3
    mov.l       @r6, r1
    add         #-0x1, r1
    mov.l       r1, @r6
    ! set IREG0 = 0x1: we want SMPC status info + pad data
    mov.b       r4, @r7
    mov.l       .L_g_smpc_status_fetch_in_progress, r4
    mov.l       @r4, r0
    add         #0x1, r0
    bra         LAB_SONICWORLD__06041204
    mov.l       r0, @r4

    .global LAB_SONICWORLD__06041202
LAB_SONICWORLD__06041202:
    mov.b       r5, @r7

    .global LAB_SONICWORLD__06041204
LAB_SONICWORLD__06041204:
    mov.l       PTR_SMPC_IREG1_SONICWORLD__060412c0, r2
    ! set IREG1 to include pad data
    mov.b       r3, @r2
    mov.w       DAT_SONICWORLD__0604129a, r1
    mov         #SMPC_CMD_INTBACK, r3
    ! IREG2 must be set to 0xF0
    mov.l       PTR_SMPC_IREG2_SONICWORLD__060412c4, r0
    mov.b       r1, @r0
    ! issue the command
    mov.l       PTR_SMPC_REG_COMREG_SONICWORLD__060412c8, r1
    mov.b       r3, @r1

    .global LAB_SONICWORLD__06041214
LAB_SONICWORLD__06041214:
    rts         
    nop         
! **************************************************************
! *                                                            *
! *  FUNCTION                                                  *
! **************************************************************
    ! undefined sw_smpc_int_handler()

! TODO: this looks like it might have been written in C.
    .global sw_smpc_int_handler
sw_smpc_int_handler:
    mov.l       r14, @-r15
    mov.l       r13, @-r15
    mov.l       r12, @-r15
    mov.l       r11, @-r15
    mov.l       r10, @-r15
    mov.l       r9, @-r15
    mov.l       r8, @-r15
    add         #-0x44, r15
    ! check to see if there's a system manager interrupt in progress
    mov.l       DAT_SONICWORLD__060412cc, r4
    mov.w       DAT_SONICWORLD__0604129c, r10
    mov.b       @r4, r3
    extu.b      r3, r3
    and         r10, r3
    cmp/eq      r10, r3
    bt          LAB_SONICWORLD__0604123a
    ! SM interrupt is not asserted - bail out
    bra         .L_smpc_int__return
    nop         

LAB_SONICWORLD__0604123a:
    mov.b       @r4, r0
    mov.l       .L_intback_last_ireg0, r7
    ! clear the SM interrupt bit
    and         #0x7f, r0
    mov.b       r0, @r4
    ! is this interrupt due to an INTBACK with SMPC status info?
    mov.l       .L_g_smpc_status_fetch_in_progress r4
    mov.l       @r4, r0
    tst         r0, r0
    bt          .L_smpc_int__process_pad_data

    ! yes - this is a status update interrupt. parse the response
    mov.l       @r4, r3
    mov.l       .L_g_smpg_date_time, r6
    ! clear the status fetch flag
    add         #-0x1, r3
    mov.l       r3, @r4
    ! copy the date info out of the OREG block into the global var
    mov.l       .L_OREG1, r4
    bra         .L_smpc_int__copy_datetime
    mov         #0x7, r5

    ! copy loop
1:  mov.b       @r4, r2
    add         #-0x1, r5   ! decrement counter
    mov.b       r2, @r6
    add         #0x2, r4    ! move to the next OREG
    add         #0x1, r6    ! move to the next write pos

.L_smpc_int__copy_datetime:                                     ! $06041262
    tst         r5, r5
    bf          1b

    ! read the area code
    add         #0x2, r4
    mov.l       PTR_g_smpc_area_code_SONICWORLD__060412d8, r2
    mov         #0x4, r5
    mov.l       PTR_g_smpc_smem_shadow_SONICWORLD__060412dc, r6
    mov.b       @r4, r3
    mov.b       r3, @r2
    ! read the 4 byte SMEM block
    add         #0x6, r4

    .global LAB_SONICWORLD__06041274
LAB_SONICWORLD__06041274:
    mov.b       @r4, r2
    add         #-0x2, r5
    mov.b       r2, @r6
    add         #0x2, r4
    mov.b       @r4, r2
    tst         r5, r5
    add         #0x1, r6
    mov.b       r2, @r6
    add         #0x1, r6
    bf/s        LAB_SONICWORLD__06041274
    add         #0x2, r4

    ! flip IREG0 MSB to issue a continue command
    mov.b       @r7, r0
    mov.l       .L_SMPC_IREG0__060412b4, r2
    xor         #0x80, r0
    mov.b       r0, @r7
    mov.b       @r7, r3
    bra         .L_smpc_int__return
    mov.b       r3, @r2

DAT_SONICWORLD__06041298:
    .word       0x03E8

DAT_SONICWORLD__0604129a:
    .word       0x00F0

DAT_SONICWORLD__0604129c:
    .word       0x0080
    .byte       0xFF
    .byte       0xFF

.L_peripheral_update_in_progress__60412a0:
    .long       peripheral_update_in_progress

PTR__pad_input_buffer_block_idx_SONICWORLD__060412a4:
    .long       _pad_input_buffer_block_idx

PTR_SMPC_REG_SF_SONICWORLD__060412a8:
    .long       SMPC_REG_SF

PTR_SMPC_IOSEL_SONICWORLD__060412ac:
    .long       SMPC_IOSEL

.L_intback_last_ireg0:
    .long       _intback_last_ireg0

.L_SMPC_IREG0__060412b4:
    .long       SMPC_IREG0

.L_g_smpc_request_status_update:
    .long       g_smpc_request_status_update

.L_g_smpc_status_fetch_in_progress:
    .long       g_smpc_status_fetch_in_progress

PTR_SMPC_IREG1_SONICWORLD__060412c0:
    .long       SMPC_IREG1

PTR_SMPC_IREG2_SONICWORLD__060412c4:
    .long       SMPC_IREG2

PTR_SMPC_REG_COMREG_SONICWORLD__060412c8:
    .long       SMPC_REG_COMREG

DAT_SONICWORLD__060412cc:
    .long       0x25FE00A7

.L_OREG1:
    .long       SMPC_OREG1

.L_g_smpg_date_time:
    .long       g_smpc_datetime

PTR_g_smpc_area_code_SONICWORLD__060412d8:
    .long       g_smpc_area_code

PTR_g_smpc_smem_shadow_SONICWORLD__060412dc:
    .long       g_smpc_smem_shadow


.L_smpc_int__process_pad_data:           ! $60412e0
    ! no - this is a pad data interrupt
    mov.l       .L_SMPC_OREG0, r5
    ! fetch the buffer pointer
    mov.l       .L_pad_input_buffer, r6
    ! and the counter to work out how many blocks have been processed so far
    mov.l       .L_pad_input_buffer_block_idx__060413f0, r4
    mov         r5, r3
    ! fetch the current block index into r13
    mov.b       @r4, r13
    ! Read the status register and check the NPE bit that tells us if there's
    ! any more data to read
    add         #0x40, r3   ! adjust r3 to point to the SR
    mov.b       @r3, r0
    mov         r13, r14
    shll2       r14
    shll2       r14
    extu.b      r0, r0
    and         #SMPC_SR_NPE, r0
    cmp/eq      #SMPC_SR_NPE, r0
    bf/s        .L_smpc_int__last_pad_data
    shll2       r14
    bra         LAB_SONICWORLD__0604130c
    mov         r14, r13

    ! read the next block of OREG data into the pad input buffer
1:  extu.b      r13, r0
    mov.b       @r5, r3
    add         #0x2, r13
    mov.b       r3, @(r0,r6)
    add         #0x2, r5

LAB_SONICWORLD__0604130c:
    mov.b       @r4, r3
    extu.b      r13, r2
    extu.b      r3, r3
    shll2       r3
    shll2       r3
    shll2       r3
    add         #0x40, r3
    cmp/ge      r3, r2
    bf          1b

    ! increment the buffer block pointer
    mov.b       @r4, r3
    add         #0x1, r3
    mov.b       r3, @r4
    ! send the CONTINUE commamnd to the SMPC
    mov.b       @r7, r0
    xor         #0x80, r0
    mov.b       r0, @r7
    mov.b       @r7, r2
    mov.l       PTR_SMPC_IREG0_SONICWORLD__060413f4, r3
    bra         .L_smpc_int__return
    mov.b       r2, @r3

.L_smpc_int__last_pad_data:                                     ! $06041332
    ! at this point we've read all of the OREG packets for this command.
    ! r4 = current block index
    ! r5 = OREG base pointer
    ! r6 = buffer base pointer
    ! r13 = pad input data block index
    ! r14 = offset into the dest buffer for the new data
    extu.b      r13, r13
    ! do we already have pad data from a previous INTBACK?
    tst         r13, r13
    bt          .L_smpc_int__pad_buffer_complete
    ! yes - we have pad data. copy the last block of OREG data into the buffer
    bra         .L_smpc_int__copy_last_pad_data_block
    mov         r14, r7

    ! copy loop
1:  mov.b       @r5, r3
    extu.b      r7, r0
    mov.b       r3, @(r0,r6)
    add         #0x2, r5
    add         #0x2, r7

.L_smpc_int__copy_last_pad_data_block:                          ! $06041346
    extu.b      r7, r2
    ! check to see if r7 (dest ptr) has gone past the end of this data block
    mov.b       @r4, r3     ! r3 = block index
    extu.b      r3, r3
    shll2       r3
    shll2       r3
    shll2       r3
    add         #0x40, r3   ! r3 = offset to end of block
    cmp/ge      r3, r2
    bf          1b

    mov         r6, r5
.L_smpc_int__pad_buffer_complete:                               ! $0604135a
    mov         #0x0, r4
    mov.w       DAT_SONICWORLD__060413e6, r12   ! r12 = 0xF0
    mov         r15, r6
    mov.l       PTR_BYTE_ARRAY_ARRAY_SONICWORLD__060413f8, r7
    mov         r4, r0
    mov.b       r0, @(input_tmp.field08, r15)
    add         #input_tmp.field34, r6
    mov         r6, r8
    mov         r6, r9
    mov         r6, r3
    add         #0x2, r3
    mov.l       r3, @(input_tmp.field14, r15)
    mov         r8, r2
    add         #0x3, r2
    mov         r8, r1
    mov.l       r2, @(input_tmp.field10, r15)
    add         #0x5, r6
    add         #0x4, r1
    mov.l       r1, @(input_tmp.field0c, r15)
    mov.l       r6, @(input_tmp.field18, r15)
    bra         LAB_SONICWORLD__060416c4
    add         #0x1, r9

LAB_SONICWORLD__06041386:
    mov         r4, r13
    mov.b       @(input_tmp.field08, r15), r0
    extu.b      r0, r0
    mov         r0, r14
    mov         r0, r3
    shll        r14
    shll2       r3
    add         r3, r14
    mov         r14, r6
    add         r7, r6
    ! zero out 6 bytes

LAB_SONICWORLD__0604139a:
    add         #0x3, r13
    mov.b       r4, @r6
    mov         #0x6, r3
    add         #0x1, r6
    extu.b      r13, r2
    mov.b       r4, @r6
    cmp/ge      r3, r2
    add         #0x1, r6
    mov.b       r4, @r6
    bf/s        LAB_SONICWORLD__0604139a
    add         #0x1, r6

    ! read the port status byte
    mov.b       @r5, r0
    extu.b      r0, r0
    ! check for a 6 controller multitap?
    cmp/eq      #PERIPH_TYPE_MULTITAP6P, r0
    bf          LAB_SONICWORLD__060413ca

    mov.l       PTR__num_pads_on_port_SONICWORLD__060413fc, r3
    mov         #0x6, r2
    mov.l       PTR_DAT_SONICWORLD__06041400, r1
    mov.b       r2, @r3
    mov.b       @(0x8,r15), r0
    extu.b      r0, r0
    add         r0, r1
    bra         LAB_SONICWORLD__060413d0
    mov.b       r2, @r1
    ! else we only have 1 pad on this port

    .global LAB_SONICWORLD__060413ca
LAB_SONICWORLD__060413ca:
    mov         #0x1, r3
    mov.l       PTR__num_pads_on_port_SONICWORLD__060413fc, r0
    mov.b       r3, @r0
    ! read the peripheral type byte

LAB_SONICWORLD__060413d0:
    mov.b       @r5, r2
    extu.b      r2, r2
    ! Is the pad connected directly (tap ID = 0xF_)?
    cmp/eq      r12, r2
    bf/s        LAB_SONICWORLD__06041404
    mov         r4, r6
    ! fetch the loop counter (port_idx)
    mov.b       @(0x8,r15), r0
    mov.l       PTR_DAT_SONICWORLD__06041400, r3
    extu.b      r0, r0
    add         r0, r3
    bra         LAB_SONICWORLD__06041406
    mov.b       r4, @r3

DAT_SONICWORLD__060413e6:
    .word       0x00F0

.L_SMPC_OREG0:
    .long       SMPC_OREG0

.L_pad_input_buffer:
    .long       _pad_input_buffer

.L_pad_input_buffer_block_idx__060413f0:
    .long       _pad_input_buffer_block_idx

PTR_SMPC_IREG0_SONICWORLD__060413f4:
    .long       SMPC_IREG0

PTR_BYTE_ARRAY_ARRAY_SONICWORLD__060413f8:
    .long       BYTE_ARRAY_ARRAY_060f4b58

PTR__num_pads_on_port_SONICWORLD__060413fc:
    .long       _num_pads_on_port

PTR_DAT_SONICWORLD__06041400:
    .long       DAT_060f4b56
    ! we're dealing with a multitap so move past the tap ID byte to 
    ! the first connected pad peripheral id

    .global LAB_SONICWORLD__06041404
LAB_SONICWORLD__06041404:
    add         #0x2, r5
    ! r14 = loop index * 6

LAB_SONICWORLD__06041406:
    mov         r14, r3
    mov.l       PTR_DAT_SONICWORLD__060414a4, r1
    mov         r14, r2
    mov.l       PTR_DAT_SONICWORLD__060414ac, r11
    ! r3 = pointer to the 6 byte array
    ! stash it in local var 0x24
    add         r7, r3
    mov.l       r3, @(0x24,r15)
    ! r2 = loop index * 12
    shll        r2
    mov.l       PTR_DAT_SONICWORLD__060414a8, r3
    add         r1, r2
    ! stash the pointer in local var 0x20
    mov.l       r2, @(0x20,r15)
    add         r14, r3
    mov.l       r3, @(0x1c,r15)
    mov.b       @(0x8,r15), r0
    extu.b      r0, r0
    bra         LAB_SONICWORLD__060416ae
    add         r0, r11

LAB_SONICWORLD__06041426:
    mov.b       @r5, r0
    extu.b      r0, r0
    ! is this a saturn pad?
    cmp/eq      #PERIPH_TYPE_SATURN_PAD, r0
    bt          .L_smpc_int__process_saturn_pad_data
    ! another chained tap?
    cmp/eq      #PERIPH_TYPE_MULTITAP6P, r0
    bt          LAB_SONICWORLD__060414b8
    mov.w       .L_peripheral_id_no_tap_unknown, r1
    cmp/eq      r1, r0
    bt          .L_smpc_int__process_unknown_pad
    mov.w       .L_peripheral_id_tap_unconnected, r1
    cmp/eq      r1, r0
    bt          .L_smpc_int__process_unknown_pad
    bra         LAB_SONICWORLD__06041564
    nop         

.L_smpc_int__process_unknown_pad:                               ! $06041442
    mov.l       DAT_SONICWORLD__060414b0, r13
    bra         LAB_SONICWORLD__06041566
    add         #0x2, r5

.L_smpc_int__process_saturn_pad_data:               ! $06041448
    ! dealing with a saturn pad. read the 2 bytes of input data
    add         #0x2, r5
    mov.b       @r5, r2
    mov.b       r2, @r8
    add         #0x2, r5
    mov.b       @r5, r3
    mov         #0xf, r2
    mov.b       r3, @r9
    add         #0x2, r5
    mov.b       @r8, r13
    extu.b      r13, r13
    mov.b       @r9, r1
    ! extract the 4 bits for Start,A,B,C and put them
    ! in the high nybble
    and         r13, r2
    shll2       r2
    shll2       r2
    ! extract the d-pad inputs and put them in the low nybble
    mov         r13, r3
    and         r12, r3
    shar        r3
    shar        r3
    shar        r3
    shar        r3
    add         r3, r2
    ! extract RT, X, Y, Z and put them into bits 11-7
    extu.b      r1, r1
    mov.l       r1, @r15
    and         r12, r1
    mov.l       @r15, r0
    shll2       r1
    shll2       r1
    add         r1, r2
    ! extract LT bit and shift into bit 12
    and         #0x8, r0
    shll8       r0
    shll        r0
    ! r2 now contains:
    ! LT,RT,X,Y,Z,Start,A,B,C,R,L,D,U
    add         r0, r2
    mov.l       DAT_SONICWORLD__060414b4, r13
    ! is this a 6P multitap?
    mov.b       @r11, r0
    cmp/eq      #0x6, r0
    bt/s        LAB_SONICWORLD__06041496
    or          r2, r13
    mov         #0x3, r2
    mov.b       r2, @r11

LAB_SONICWORLD__06041496:
    extu.b      r6, r0
    add         r14, r0
    mov         #0x3, r3
    bra         LAB_SONICWORLD__0604156c
    mov.b       r3, @(r0,r7)

.L_peripheral_id_no_tap_unknown:
    .word       0x00F0

.L_peripheral_id_tap_unconnected:
    .word       0x00FF

PTR_DAT_SONICWORLD__060414a4:
    .long       DAT_060f4a7e

PTR_DAT_SONICWORLD__060414a8:
    .long       DAT_060f4a72

PTR_DAT_SONICWORLD__060414ac:
    .long       DAT_060f4b56

DAT_SONICWORLD__060414b0:
    .long       0x0000FFFF

DAT_SONICWORLD__060414b4:
    .long       0x0000E000

LAB_SONICWORLD__060414b8:
    add         #0x2, r5
    mov.b       @r5, r3
    mov.b       r3, @r8
    add         #0x2, r5
    mov.b       @r5, r2
    add         #0x2, r5
    mov.b       r2, @r9
    mov.l       @(input_tmp.field14, r15), r3
    mov.b       @r5, r2
    add         #0x2, r5
    mov.b       r2, @r3
    mov.l       @(input_tmp.field10, r15), r3
    mov.b       @r5, r2
    add         #0x2, r5
    mov.b       r2, @r3
    mov.l       @(input_tmp.field0c, r15), r3
    mov.b       @r5, r2
    add         #0x2, r5
    mov.b       r2, @r3
    mov.l       @(input_tmp.field18,r15), r3
    mov.b       @r5, r2
    mov.b       r2, @r3
    mov.b       @r8, r0
    extu.b      r0, r0
    mov.l       r0, @r15
    and         #0xf, r0
    mov.l       @r15, r3
    shll2       r0
    shll2       r0
    and         r12, r3
    shar        r3
    shar        r3
    mov.b       @r9, r13
    mov         #0x8, r2
    shar        r3
    shar        r3
    add         r3, r0
    extu.b      r13, r13
    mov         r13, r3
    and         r12, r3
    shll2       r3
    shll2       r3
    add         r3, r0
    and         r13, r2
    mov.l       DAT_SONICWORLD__060416e4, r13
    shll8       r2
    shll        r2
    add         r2, r0
    or          r0, r13
    mov.b       @r11, r0
    cmp/eq      #0x6, r0
    bt/s        LAB_SONICWORLD__06041526
    add         #0x2, r5
    mov         #0x7, r2
    mov.b       r2, @r11

    .global LAB_SONICWORLD__06041526
LAB_SONICWORLD__06041526:
    extu.b      r6, r0
    mov.l       r0, @r15
    mov         #0x7, r3
    add         r14, r0
    mov.b       r3, @(r0,r7)
    mov.l       @r15, r2
    add         r14, r2
    mov.l       PTR_DAT_SONICWORLD__060416e8, r0
    mov.l       @(0x14,r15), r3
    mov.b       @r3, r3
    mov.b       r3, @(r0,r2)
    mov.l       @r15, r2
    add         r14, r2
    mov.l       PTR_DAT_SONICWORLD__060416ec, r0
    mov.l       @(0x10,r15), r3
    mov.b       @r3, r3
    mov.b       r3, @(r0,r2)
    mov.l       @r15, r2
    add         r14, r2
    mov.l       PTR_DAT_SONICWORLD__060416f0, r0
    mov.l       @(0xc,r15), r3
    mov.b       @r3, r3
    mov.b       r3, @(r0,r2)
    mov.l       @r15, r2
    add         r14, r2
    mov.l       PTR_DAT_SONICWORLD__060416f4, r0
    mov.l       @(0x18,r15), r3
    mov.b       @r3, r3
    mov.b       r3, @(r0,r2)
    bra         LAB_SONICWORLD__0604156c
    nop         

    .global LAB_SONICWORLD__06041564
LAB_SONICWORLD__06041564:
    mov         r4, r13

    .global LAB_SONICWORLD__06041566
LAB_SONICWORLD__06041566:
    extu.b      r6, r0
    add         r14, r0
    mov.b       r4, @(r0,r7)

    .global LAB_SONICWORLD__0604156c
LAB_SONICWORLD__0604156c:
    mov.l       PTR_DAT_SONICWORLD__060416f8, r3
    ! pad bits are active low so invert here
    not         r13, r13
    ! r6 == 0 here
    extu.b      r6, r2
    mov.l       r2, @r15
    add         r14, r2
    shll        r2
    mov.l       r2, @(0x4,r15)
    add         r3, r2
    mov.l       r2, @(0x30,r15)
    mov.l       @r15, r0
    add         r14, r0
    mov.b       @(r0,r7), r1
    extu.b      r1, r1
    mov         #0x28, r0
    mov.w       r1, @r2
    mov.l       PTR__menu_input_flags_SONICWORLD__060416fc, r2
    mov.l       @(0x4,r15), r1
    add         r2, r1
    mov.l       r1, @r15
    mov.w       @r1, r3
    mov.w       r3, @(r0,r15)
    mov         r15, r1
    mov.l       PTR_DAT_SONICWORLD__06041700, r3
    add         #0x28, r1
    mov.l       @(0x4,r15), r0
    add         r3, r0
    mov.w       @r1, r2
    extu.w      r2, r2
    mov.l       r2, @(0x2c,r15)
    not         r2, r2
    and         r13, r2
    mov.w       r2, @r0
    mov.l       @r15, r0
    mov.w       r13, @r0
    mov.l       @r15, r2
    mov.w       @r2, r1
    mov.w       r1, @r15
    mov.l       @(0x30,r15), r0
    mov.w       @r0, r0
    mov.w       r0, @(0x4,r15)
    mov.w       @(0x4,r15), r0
    mov         r0, r13
    mov         r13, r0
    cmp/eq      #0x4, r0
    bt          LAB_SONICWORLD__060415ea
    extu.b      r6, r1
    mov.l       PTR_DAT_SONICWORLD__06041704, r0
    add         r14, r1
    mov.l       PTR_DAT_SONICWORLD__06041708, r3
    shll        r1
    mov.l       r1, @(0x4,r15)
    mov.w       r4, @(r0,r1)
    mov.l       @(0x4,r15), r2
    add         r3, r2
    mov.w       r4, @r2
    mov.l       PTR_DAT_SONICWORLD__0604170c, r2
    mov.l       @(0x4,r15), r1
    add         r2, r1
    mov.w       r4, @r1
    mov.l       PTR_DAT_SONICWORLD__06041710, r3
    mov.l       @(0x4,r15), r1
    add         r3, r1
    mov.w       r4, @r1

    .global LAB_SONICWORLD__060415ea
LAB_SONICWORLD__060415ea:
    mov         r13, r0
    cmp/eq      #0x5, r0
    bt          LAB_SONICWORLD__06041616
    mov         r13, r0
    cmp/eq      #0x7, r0
    bt          LAB_SONICWORLD__06041616
    mov         r13, r0
    cmp/eq      #0x8, r0
    bt          LAB_SONICWORLD__06041616
    mov.l       PTR_DAT_SONICWORLD__060416e8, r0
    extu.b      r6, r2
    mov.l       r2, @(0x4,r15)
    add         r14, r2
    mov.b       r10, @(r0,r2)
    mov.l       @(0x4,r15), r3
    mov.l       PTR_DAT_SONICWORLD__060416ec, r0
    add         r14, r3
    mov.b       r10, @(r0,r3)
    mov.l       @(0x4,r15), r3
    mov.l       PTR_DAT_SONICWORLD__06041714, r0
    add         r14, r3
    mov.b       r10, @(r0,r3)

    .global LAB_SONICWORLD__06041616
LAB_SONICWORLD__06041616:
    mov         r13, r0
    cmp/eq      #0x7, r0
    bt          LAB_SONICWORLD__06041634
    mov         r13, r0
    cmp/eq      #0x8, r0
    bt          LAB_SONICWORLD__06041634
    extu.b      r6, r3
    mov.l       PTR_DAT_SONICWORLD__060416f0, r0
    mov.l       r3, @(0x4,r15)
    add         r14, r3
    mov.b       r4, @(r0,r3)
    mov.l       @(0x4,r15), r3
    add         r14, r3
    mov.l       PTR_DAT_SONICWORLD__060416f4, r0
    mov.b       r4, @(r0,r3)

    .global LAB_SONICWORLD__06041634
LAB_SONICWORLD__06041634:
    mov         r13, r0
    cmp/eq      #0x8, r0
    bt          LAB_SONICWORLD__0604164a
    extu.b      r6, r13
    mov.l       PTR_DAT_SONICWORLD__06041718, r0
    mov         r14, r2
    add         r13, r2
    add         r14, r13
    mov.b       r4, @(r0,r2)
    mov.l       PTR_DAT_SONICWORLD__0604171c, r0
    mov.b       r4, @(r0,r13)

    .global LAB_SONICWORLD__0604164a
LAB_SONICWORLD__0604164a:
    mov.w       @r15, r3
    mov.l       @(0x2c,r15), r2
    extu.w      r3, r3
    cmp/eq      r2, r3
    bf          LAB_SONICWORLD__06041686
    mov.l       PTR_DAT_SONICWORLD__06041720, r0
    extu.b      r6, r1
    add         r14, r1
    mov.b       @(r0,r1), r3
    tst         r3, r3
    bt          LAB_SONICWORLD__06041676
    extu.b      r6, r13
    mov         r14, r3
    add         r13, r3
    add         r14, r13
    mov.b       @(r0,r3), r2
    shll        r13
    add         #-0x1, r2
    mov.b       r2, @(r0,r3)
    mov.l       PTR_DAT_SONICWORLD__06041724, r0
    bra         LAB_SONICWORLD__06041692
    mov.w       r4, @(r0,r13)

    .global LAB_SONICWORLD__06041676
LAB_SONICWORLD__06041676:
    extu.b      r6, r2
    mov.l       PTR_DAT_SONICWORLD__06041724, r0
    add         r14, r2
    mov.w       @r15, r3
    shll        r2
    mov.w       r3, @(r0,r2)
    bra         LAB_SONICWORLD__06041692
    nop         

    .global LAB_SONICWORLD__06041686
LAB_SONICWORLD__06041686:
    mov         #0x20, r3
    mov.l       @(0x1c,r15), r0
    mov.b       r3, @r0
    mov.l       @(0x20,r15), r2
    mov.w       @r15, r3
    mov.w       r3, @r2

    .global LAB_SONICWORLD__06041692
LAB_SONICWORLD__06041692:
    mov.l       @(0x24,r15), r2
    mov.b       @r2, r3
    tst         r3, r3
    bf          LAB_SONICWORLD__060416be
    add         #0x1, r6
    mov.l       @(0x24,r15), r3
    add         #0x1, r3
    mov.l       r3, @(0x24,r15)
    mov.l       @(0x20,r15), r2
    add         #0x2, r2
    mov.l       r2, @(0x20,r15)
    mov.l       @(0x1c,r15), r1
    add         #0x1, r1
    mov.l       r1, @(0x1c,r15)

    .global LAB_SONICWORLD__060416ae
LAB_SONICWORLD__060416ae:
    extu.b      r6, r3
    mov.l       PTR__num_pads_on_port_SONICWORLD__06041728, r1
    mov.b       @r1, r2
    extu.b      r2, r2
    cmp/ge      r2, r3
    bt          LAB_SONICWORLD__060416be
    bra         LAB_SONICWORLD__06041426
    nop         

    .global LAB_SONICWORLD__060416be
LAB_SONICWORLD__060416be:
    mov.b       @(0x8,r15), r0
    add         #0x1, r0
    mov.b       r0, @(0x8,r15)

LAB_SONICWORLD__060416c4:
    mov.b       @(input_tmp.field08, r15), r0
    mov         #0x2, r3
    extu.b      r0, r0
    cmp/ge      r3, r0
    bt          .L_smpc_int__return
    bra         LAB_SONICWORLD__06041386
    nop         

.L_smpc_int__return:                                            ! $60416d2
    add         #0x44, r15
    mov.l       @r15+, r8
    mov.l       @r15+, r9
    mov.l       @r15+, r10
    mov.l       @r15+, r11
    mov.l       @r15+, r12
    mov.l       @r15+, r13
    rts         
    mov.l       @r15+, r14

    .global DAT_SONICWORLD__060416e4
DAT_SONICWORLD__060416e4:
    .long       0x0000E000

    .global PTR_DAT_SONICWORLD__060416e8
PTR_DAT_SONICWORLD__060416e8:
    .long       DAT_060f4b02

    .global PTR_DAT_SONICWORLD__060416ec
PTR_DAT_SONICWORLD__060416ec:
    .long       DAT_060f4b0e

    .global PTR_DAT_SONICWORLD__060416f0
PTR_DAT_SONICWORLD__060416f0:
    .long       DAT_060f4b26

    .global PTR_DAT_SONICWORLD__060416f4
PTR_DAT_SONICWORLD__060416f4:
    .long       DAT_060f4b32

    .global PTR_DAT_SONICWORLD__060416f8
PTR_DAT_SONICWORLD__060416f8:
    .long       DAT_060f4a12

    .global PTR__menu_input_flags_SONICWORLD__060416fc
PTR__menu_input_flags_SONICWORLD__060416fc:
    .long       _menu_input_flags

    .global PTR_DAT_SONICWORLD__06041700
PTR_DAT_SONICWORLD__06041700:
    .long       DAT_060f4a5a

    .global PTR_DAT_SONICWORLD__06041704
PTR_DAT_SONICWORLD__06041704:
    .long       DAT_060f4aa2

    .global PTR_DAT_SONICWORLD__06041708
PTR_DAT_SONICWORLD__06041708:
    .long       DAT_060f4aba

    .global PTR_DAT_SONICWORLD__0604170c
PTR_DAT_SONICWORLD__0604170c:
    .long       DAT_060f4ad2

    .global PTR_DAT_SONICWORLD__06041710
PTR_DAT_SONICWORLD__06041710:
    .long       DAT_060f4aea

    .global PTR_DAT_SONICWORLD__06041714
PTR_DAT_SONICWORLD__06041714:
    .long       DAT_060f4b1a

    .global PTR_DAT_SONICWORLD__06041718
PTR_DAT_SONICWORLD__06041718:
    .long       DAT_060f4b3e

    .global PTR_DAT_SONICWORLD__0604171c
PTR_DAT_SONICWORLD__0604171c:
    .long       DAT_060f4b4a

    .global PTR_DAT_SONICWORLD__06041720
PTR_DAT_SONICWORLD__06041720:
    .long       DAT_060f4a72

    .global PTR_DAT_SONICWORLD__06041724
PTR_DAT_SONICWORLD__06041724:
    .long       DAT_060f4a7e

    .global PTR__num_pads_on_port_SONICWORLD__06041728
PTR__num_pads_on_port_SONICWORLD__06041728:
    .long       _num_pads_on_port


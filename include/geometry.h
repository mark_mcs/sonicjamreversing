#pragma once

#include <stddef.h>
#include <stdint.h>

#define FLOAT_TO_FIXED(x) ((fixed) ((x) * 65536.0))
#define POS_TO_FIXED(x, y, z) {FLOAT_TO_FIXED(x), FLOAT_TO_FIXED(y), FLOAT_TO_FIXED(z)}

typedef int32_t fixed;
typedef int16_t angle;

typedef fixed vec3[3];
// a 'compressed' vector that only stores the fractional part of a fixed value.
typedef int16_t vec3s[3];
typedef fixed matrix[4][3];


typedef struct polygon_attr {
    uint8_t flags;
    uint8_t sort;
    uint16_t char_pattern;
    uint16_t cmdpmod;
    uint16_t colour;
    uint16_t gouraud_table;
    uint16_t cmdctrl;
    uint16_t field7;
    uint16_t field8;
} polygon_attr;

typedef struct polygon {
    int16_t indices[4];
} polygon;


typedef struct model {
    vec3 *vertices;
    uint32_t vertex_count;
    polygon *faces;
    uint32_t face_count;
    polygon_attr *attrs;
} model;


typedef struct xmodel {
    vec3 *vertices;
    uint32_t vertex_count;
    polygon *faces;
    uint32_t face_count;
    polygon_attr *attrs;
    vec3 *normals;
} xmodel;


typedef struct model16 {
    vec3s *vertices;
    uint32_t vertex_count;
    polygon *faces;
    uint32_t face_count;
    polygon_attr *attrs;
} model16;


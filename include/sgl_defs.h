#pragma once
#include <cstdint>

typedef int16_t ANGLE;
typedef int32_t FIXED;

typedef FIXED MATRIX[4][3];
typedef FIXED VECTOR[3];
typedef FIXED POINT[3];

typedef struct ATTR {
    uint8_t flags;
    uint8_t sort;
    uint16_t char_pattern;
    uint16_t cmdpmod;
    uint16_t colour;
    uint16_t gouraud_table;
    uint16_t cmdctrl;
    uint16_t field7;
    uint16_t field8;
} ATTR;

typedef struct POLYGON {
    uint16_t indices[4];
} POLYGON;

typedef struct PDATA {
    POINT *vertices;
    uint32_t vertex_count;
    POLYGON *polygons;
    uint32_t polygon_count;
    
} PDATA;

typedef struct XPDATA {
    POINT *vertices;
    uint32_t vertex_count;
    POLYGON *polygons;
    uint32_t polygon_count;
    VECTOR *normals;
} XPDATA;


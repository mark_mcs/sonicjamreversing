.equ CONTROLLER_TYPE_NORMAL,                        3
.equ CONTROLLER_TYPE_3DPAD,                         7

! direction bits get put into GBR 0xE0, 0xE8
.equ INPUT_BIT_UP,                                  0x0001
.equ INPUT_BIT_DOWN,                                0x0002
.equ INPUT_BIT_LEFT,                                0x0004
.equ INPUT_BIT_RIGHT,                               0x0008
.equ INPUT_BIT_B,                                   0x0010
.equ INPUT_BIT_C,                                   0x0020
.equ INPUT_BIT_A,                                   0x0040
.equ INPUT_BIT_START,                               0x0080

.equ INPUT_BIT_Z,                                   0x0100
.equ INPUT_BIT_Y,                                   0x0200
.equ INPUT_BIT_X,                                   0x0400
.equ INPUT_BIT_R,                                   0x0800
.equ INPUT_BIT_L,                                   0x1000


! struct input_flags {
    .struct 0
input_flags.controller_type:

!   uint8_t field0a;
    .struct 0x0a
input_flags.field0a:
!   uint8_t field0b;
    .struct 0x0b
input_flags.field0b:
! }


    .struct 0
input_tmp.field00:

//  uint8_t field08;
    .struct 8
input_tmp.field08:

//  uint32_t field0c
    .struct 0x0c
input_tmp.field0c:
//  uint32_t field10
    .struct 0x10
input_tmp.field10:
//  uint16_t* pointer to end of buffer?
    .struct 0x14
input_tmp.field14
//  uint32_t field18
    .struct 0x18
input_tmp.field18:

//  some sort of nested struct
    .struct 0x34
input_tmp.field34:

    .struct 0x44
sizeof__input_tmp:

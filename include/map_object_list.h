#pragma once

#include <stdint.h>

#include "game_component.h"

typedef struct map_object_def {
    /* The handler function to be called for the object during the physics and
     * render phases
     */
    component_handler handler;

    uint16_t field1;

    /* The distance from the camera at which the object is spawned/despawned */
    int16_t activation_radius;

    /* A short name for the object. Not used by the engine */
    char name[8];
    
    /* 4 longwords that can be used as arbitrary parameters that get passed
     * to the object handler */
    void *params[4];

} map_object_def;


extern map_object_def g_object_definitions_table[];

!
! struct object_list_node {
!   object_list_node* next;
    .struct 0
object_list_node.next:
!   object_list_node* prev;
    .struct 4
object_list_node.prev:
!   ushort level_layout_offset;
    .struct 8
object_list_node.level_layout_offset:
!   ushort activation_radius;
    .struct 0x0A
object_list_node.activation_radius:
!   void (*handler)(void *private_data);
    .struct 0x0C
object_list_node.handler:
!   char private_data[80];
    .struct 0x10
object_list_node.private_data:
! }
    .struct 0x18
sizeof__object_list_node:

!
! struct object {
    .struct 0
object.model_data:

!   model_anim_state *anim_state;
    .struct 0x08
object.anim_state:

!   uint8_t render_flags;
    .struct 0x0D
object.render_flags:
    .struct 0x0E
object.anim_seq_index:
    .struct 0x10
object.model_positions:

    .struct 0x1E
object.gouraud_table:

    .struct 0x30
object.position.x:
    .struct 0x34
object.position.y:
    .struct 0x38
object.position.z:

    .struct 0x44
object.path_state:

! ------------------------------------------------------------------------------
! struct object2d {
!   uint16_t field_00;
    .struct 0
object2d.field_00:

!   uint8_t anim_seq_index;
    .struct 0x0e
object2d.anim_seq_index:

!   // the index of the frame to display
!   uint16_t anim_frame_number;
    .struct 0x12
object2d.anim_frame_number:
!   // the multipler value used to scale the sprites
!   uint16_t sprite_scale_factor;
    .struct 0x14
object2d.sprite_scale_factor:

    .struct 0x2c
object2d.anim_frame_table:
    .struct 0x30
object2d.position.x:
    .struct 0x34
object2d.position.y:
    .struct 0x38
object2d.position.z:
    .struct 0x3c
object2d.anim_seq_table:
! }


! Options for the render_flags bitfield
.equ RENDER_BIT0,           1
.equ RENDER_SHADING_MODE,   2       ! 0 = flat, 1 = gouraud
.equ RENDER_LIGHTING,       4

!
! struct submodel_position {
!     Vec3D pos;
!     ANGLE rot[16][3];
!     Vec3D pos_lerp_value;
!     ANGLE rot_lerp_value[16][3];
! }
! 
    .struct 0
submodel_position.pos:
    .struct 0x0C
submodel_position.rot:
    .struct 0x6C
submodel_position.pos_lerp_value:
    .struct 0x78
submodel_position.rot_lerp_value:
    .struct 0xD8
sizeof__submodel_position:


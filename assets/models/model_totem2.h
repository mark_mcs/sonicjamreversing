#pragma once

#include "geometry.h"

extern model16 model_totem2_model_00;
extern model16 model_totem2_model_01;

extern char model_totem2_data_00[];
extern model16 * model_totem2_group[];
extern vec3 model_totem2_positions[];
#include "model_0x060a3dd4.h"

vec3 model_0x060a3dd4_00_verts[] = {
    POS_TO_FIXED(-2144.0, -288.0, 0.0),
    POS_TO_FIXED(-1984.0, -320.0, 159.99996948242188),
    POS_TO_FIXED(-1984.0, -320.0, 0.0),
    POS_TO_FIXED(-1824.0, -288.0, 319.9999694824219),
    POS_TO_FIXED(-1824.0, -368.0, 159.99996948242188),
    POS_TO_FIXED(-1824.0, -368.0, 0.0),
    POS_TO_FIXED(-1664.0001068115234, -288.0, 384.00001525878906),
    POS_TO_FIXED(-944.0, -320.0, 0.0),
    POS_TO_FIXED(-1664.0001068115234, -368.0, 159.99996948242188),
    POS_TO_FIXED(-1664.0001068115234, -368.0, 0.0),
    POS_TO_FIXED(-1504.0, -320.0, 399.9999694824219),
    POS_TO_FIXED(-1504.0, -368.0, 159.99996948242188),
    POS_TO_FIXED(-1504.0, -368.0, 0.0),
    POS_TO_FIXED(-1344.0001068115234, -288.0, 384.00001525878906),
    POS_TO_FIXED(-1344.0001068115234, -368.0, 159.99996948242188),
    POS_TO_FIXED(-1344.0001068115234, -368.0, 0.0),
    POS_TO_FIXED(-1184.0, -288.0, 319.9999694824219),
    POS_TO_FIXED(-1184.0, -320.0, 159.99996948242188),
    POS_TO_FIXED(-1184.0, -320.0, 0.0),
    POS_TO_FIXED(-1024.0, -320.0, 159.99996948242188),
    POS_TO_FIXED(-1024.0, -320.0, 0.0)
};

polygon model_0x060a3dd4_00_faces[] = {
    { 64, 72, 96, 88 },
    { 48, 64, 88, 80 },
    { 32, 40, 72, 64 },
    { 24, 32, 64, 48 },
    { 80, 88, 112, 104 },
    { 8, 16, 40, 32 },
    { 160, 160, 56, 152 },
    { 24, 8, 32, 24 },
    { 88, 96, 120, 112 },
    { 8, 0, 16, 8 },
    { 104, 112, 136, 128 },
    { 136, 144, 160, 152 },
    { 128, 136, -144, 152 },
    { 112, 120, 144, 136 }};

polygon_attr model_0x060a3dd4_00_attrs[] = {
    { 0x00, 0x06, 0x0038, 0x10cc, 0x8000, 0x1ebf, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x002a, 0x10cc, 0x8000, 0x1ec0, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0035, 0x10cc, 0x8000, 0x1ec1, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x0159, 0x10cc, 0x8000, 0x1ec2, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x015a, 0x10cc, 0x8000, 0x1ec3, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x015b, 0x10cc, 0x8000, 0x1ec4, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x0038, 0x10cc, 0x8000, 0x1ec5, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x002a, 0x10cc, 0x8000, 0x1ec6, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x015c, 0x10cc, 0x8000, 0x1ec7, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0035, 0x10cc, 0x8000, 0x1ec8, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x015d, 0x10cc, 0x8000, 0x1ec9, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0035, 0x10cc, 0x8000, 0x1eca, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0031, 0x10cc, 0x8000, 0x1ecb, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x015e, 0x10cc, 0x8000, 0x1ecc, 0x0022, 0x0000, 0x0000 }
};

model model_0x060a3dd4_model_00 = {
    model_0x060a3dd4_00_verts,
    21,
    model_0x060a3dd4_00_faces,
    14,
    model_0x060a3dd4_00_attrs
};


char model_0x060a3dd4_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_0x060a3dd4_group[] = {
    &model_0x060a3dd4_model_00
};

__attribute__((section(".data")))
vec3 model_0x060a3dd4_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

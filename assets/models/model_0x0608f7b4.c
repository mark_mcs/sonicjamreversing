#include "model_0x0608f7b4.h"

vec3 model_0x0608f7b4_00_verts[] = {
    POS_TO_FIXED(60.0, 55.99998474121094, -52.0),
    POS_TO_FIXED(18.994949340820312, -42.99494934082031, -52.0),
    POS_TO_FIXED(-36.73762512207031, 55.99998474121094, 81.14791870117188),
    POS_TO_FIXED(-49.40887451171875, -42.99494934082031, 42.1497802734375)
};

polygon model_0x0608f7b4_00_faces[] = {
    { 0, 16, 24, 8 }};

polygon_attr model_0x0608f7b4_00_attrs[] = {
    { 0x00, 0x04, 0x000a, 0x10cc, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_00 = {
    model_0x0608f7b4_00_verts,
    4,
    model_0x0608f7b4_00_faces,
    1,
    model_0x0608f7b4_00_attrs
};

vec3 model_0x0608f7b4_01_verts[] = {
    POS_TO_FIXED(67.26235961914062, 55.99998474121094, 41.147918701171875),
    POS_TO_FIXED(54.59111022949219, -42.99494934082031, 2.1497802734375),
    POS_TO_FIXED(-89.26237487792969, 55.99998474121094, -9.710052490234375),
    POS_TO_FIXED(-56.088592529296875, -42.99494934082031, -33.812225341796875)
};

polygon model_0x0608f7b4_01_faces[] = {
    { 0, 16, 24, 8 }};

polygon_attr model_0x0608f7b4_01_attrs[] = {
    { 0x00, 0x04, 0x000b, 0x10cc, 0x8000, 0x0001, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_01 = {
    model_0x0608f7b4_01_verts,
    4,
    model_0x0608f7b4_01_faces,
    1,
    model_0x0608f7b4_01_attrs
};

vec3 model_0x0608f7b4_02_verts[] = {
    POS_TO_FIXED(-21.262374877929688, 55.99998474121094, 82.2899169921875),
    POS_TO_FIXED(11.911407470703125, -42.99494934082031, 58.18775939941406),
    POS_TO_FIXED(-21.262374877929688, 55.99998474121094, -82.28994750976562),
    POS_TO_FIXED(11.911407470703125, -42.99494934082031, -58.187774658203125)
};

polygon model_0x0608f7b4_02_faces[] = {
    { 16, 24, 8, 0 }};

polygon_attr model_0x0608f7b4_02_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x0002, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_02 = {
    model_0x0608f7b4_02_verts,
    4,
    model_0x0608f7b4_02_faces,
    1,
    model_0x0608f7b4_02_attrs
};

vec3 model_0x0608f7b4_03_verts[] = {
    POS_TO_FIXED(-85.26235961914062, 55.99998474121094, 9.710037231445312),
    POS_TO_FIXED(-52.08857727050781, -42.99494934082031, 33.81221008300781),
    POS_TO_FIXED(71.26240539550781, 55.99998474121094, -41.147918701171875),
    POS_TO_FIXED(58.59112548828125, -42.99494934082031, -2.1497802734375)
};

polygon model_0x0608f7b4_03_faces[] = {
    { 0, 16, 24, 8 }};

polygon_attr model_0x0608f7b4_03_attrs[] = {
    { 0x00, 0x04, 0x000a, 0x10cc, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_03 = {
    model_0x0608f7b4_03_verts,
    4,
    model_0x0608f7b4_03_faces,
    1,
    model_0x0608f7b4_03_attrs
};

vec3 model_0x0608f7b4_04_verts[] = {
    POS_TO_FIXED(56.0, 55.99998474121094, 56.0),
    POS_TO_FIXED(14.994949340820312, -42.99494934082031, 56.0),
    POS_TO_FIXED(-40.73759460449219, 55.99998474121094, -77.14794921875),
    POS_TO_FIXED(-53.40885925292969, -42.99494934082031, -38.14979553222656)
};

polygon model_0x0608f7b4_04_faces[] = {
    { 16, 0, 8, 24 }};

polygon_attr model_0x0608f7b4_04_attrs[] = {
    { 0x00, 0x04, 0x000b, 0x10cc, 0x8000, 0x0004, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_04 = {
    model_0x0608f7b4_04_verts,
    4,
    model_0x0608f7b4_04_faces,
    1,
    model_0x0608f7b4_04_attrs
};

vec3 model_0x0608f7b4_05_verts[] = {
    POS_TO_FIXED(-40.0, -20.0, 28.0),
    POS_TO_FIXED(58.99494934082031, 21.005035400390625, 28.0),
    POS_TO_FIXED(-9.408859252929688, 21.005035400390625, -66.14981079101562)
};

polygon model_0x0608f7b4_05_faces[] = {
    { 16, 8, -16, 0 }};

polygon_attr model_0x0608f7b4_05_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x0005, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_05 = {
    model_0x0608f7b4_05_verts,
    3,
    model_0x0608f7b4_05_faces,
    1,
    model_0x0608f7b4_05_attrs
};

vec3 model_0x0608f7b4_06_verts[] = {
    POS_TO_FIXED(-40.0, -20.0, -32.0),
    POS_TO_FIXED(58.99494934082031, 21.005035400390625, -32.0),
    POS_TO_FIXED(-9.40887451171875, 21.005035400390625, 62.14979553222656)
};

polygon model_0x0608f7b4_06_faces[] = {
    { 16, 0, -8, 8 }};

polygon_attr model_0x0608f7b4_06_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x0006, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_06 = {
    model_0x0608f7b4_06_verts,
    3,
    model_0x0608f7b4_06_faces,
    1,
    model_0x0608f7b4_06_attrs
};

vec3 model_0x0608f7b4_07_verts[] = {
    POS_TO_FIXED(16.0, -20.0, -52.0),
    POS_TO_FIXED(46.591094970703125, 21.005035400390625, 42.14976501464844),
    POS_TO_FIXED(-64.08857727050781, 21.005035400390625, 6.1877593994140625)
};

polygon model_0x0608f7b4_07_faces[] = {
    { 8, 16, -24, 0 }};

polygon_attr model_0x0608f7b4_07_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x0007, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_07 = {
    model_0x0608f7b4_07_verts,
    3,
    model_0x0608f7b4_07_faces,
    1,
    model_0x0608f7b4_07_attrs
};

vec3 model_0x0608f7b4_08_verts[] = {
    POS_TO_FIXED(12.0, -20.0, 48.00001525878906),
    POS_TO_FIXED(-68.08860778808594, 21.005035400390625, -10.187774658203125),
    POS_TO_FIXED(42.59112548828125, 21.005035400390625, -46.1497802734375)
};

polygon model_0x0608f7b4_08_faces[] = {
    { 16, 0, -8, 8 }};

polygon_attr model_0x0608f7b4_08_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x0008, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_08 = {
    model_0x0608f7b4_08_verts,
    3,
    model_0x0608f7b4_08_faces,
    1,
    model_0x0608f7b4_08_attrs
};

vec3 model_0x0608f7b4_09_verts[] = {
    POS_TO_FIXED(40.0, -20.0, 0.0),
    POS_TO_FIXED(-40.088592529296875, 21.005035400390625, 58.18775939941406),
    POS_TO_FIXED(-40.08857727050781, 21.005035400390625, -58.187774658203125)
};

polygon model_0x0608f7b4_09_faces[] = {
    { 16, 0, -8, 8 }};

polygon_attr model_0x0608f7b4_09_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x0009, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_09 = {
    model_0x0608f7b4_09_verts,
    3,
    model_0x0608f7b4_09_faces,
    1,
    model_0x0608f7b4_09_attrs
};

vec3 model_0x0608f7b4_10_verts[] = {
    POS_TO_FIXED(60.00001525878906, -55.99998474121094, -52.00001525878906),
    POS_TO_FIXED(18.994949340820312, 42.99494934082031, -52.00001525878906),
    POS_TO_FIXED(-36.73760986328125, -55.999969482421875, 81.14788818359375),
    POS_TO_FIXED(-49.40887451171875, 42.994964599609375, 42.14976501464844)
};

polygon model_0x0608f7b4_10_faces[] = {
    { 8, 24, 16, 0 }};

polygon_attr model_0x0608f7b4_10_attrs[] = {
    { 0x00, 0x04, 0x000d, 0x10cc, 0x8000, 0x000a, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_10 = {
    model_0x0608f7b4_10_verts,
    4,
    model_0x0608f7b4_10_faces,
    1,
    model_0x0608f7b4_10_attrs
};

vec3 model_0x0608f7b4_11_verts[] = {
    POS_TO_FIXED(67.2623291015625, -55.999969482421875, 41.147918701171875),
    POS_TO_FIXED(54.59111022949219, 42.994964599609375, 2.1497650146484375),
    POS_TO_FIXED(-89.26240539550781, -55.999969482421875, -9.710067749023438),
    POS_TO_FIXED(-56.088592529296875, 42.994964599609375, -33.81224060058594)
};

polygon model_0x0608f7b4_11_faces[] = {
    { 8, 24, 16, 0 }};

polygon_attr model_0x0608f7b4_11_attrs[] = {
    { 0x00, 0x04, 0x000e, 0x10cc, 0x8000, 0x000b, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_11 = {
    model_0x0608f7b4_11_verts,
    4,
    model_0x0608f7b4_11_faces,
    1,
    model_0x0608f7b4_11_attrs
};

vec3 model_0x0608f7b4_12_verts[] = {
    POS_TO_FIXED(12.0, 20.0, 47.999969482421875),
    POS_TO_FIXED(-68.08863830566406, -21.005050659179688, -10.187789916992188),
    POS_TO_FIXED(42.591094970703125, -21.005050659179688, -46.14976501464844)
};

polygon model_0x0608f7b4_12_faces[] = {
    { 16, 8, -16, 0 }};

polygon_attr model_0x0608f7b4_12_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x000c, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_12 = {
    model_0x0608f7b4_12_verts,
    3,
    model_0x0608f7b4_12_faces,
    1,
    model_0x0608f7b4_12_attrs
};

vec3 model_0x0608f7b4_13_verts[] = {
    POS_TO_FIXED(-40.0, 20.0, 27.999969482421875),
    POS_TO_FIXED(58.99494934082031, -21.005035400390625, 27.999969482421875),
    POS_TO_FIXED(-9.408859252929688, -21.005050659179688, -66.14979553222656)
};

polygon model_0x0608f7b4_13_faces[] = {
    { 16, 0, -8, 8 }};

polygon_attr model_0x0608f7b4_13_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x000d, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_13 = {
    model_0x0608f7b4_13_verts,
    3,
    model_0x0608f7b4_13_faces,
    1,
    model_0x0608f7b4_13_attrs
};

vec3 model_0x0608f7b4_14_verts[] = {
    POS_TO_FIXED(-21.262374877929688, -55.999969482421875, 82.2899169921875),
    POS_TO_FIXED(11.911392211914062, 42.99494934082031, 58.187744140625),
    POS_TO_FIXED(-21.262374877929688, -56.0, -82.28994750976562),
    POS_TO_FIXED(11.911392211914062, 42.99494934082031, -58.18778991699219)
};

polygon model_0x0608f7b4_14_faces[] = {
    { 24, 16, 0, 8 }};

polygon_attr model_0x0608f7b4_14_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x000e, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_14 = {
    model_0x0608f7b4_14_verts,
    4,
    model_0x0608f7b4_14_faces,
    1,
    model_0x0608f7b4_14_attrs
};

vec3 model_0x0608f7b4_15_verts[] = {
    POS_TO_FIXED(40.0, 20.0, -1.52587890625e-05),
    POS_TO_FIXED(-40.088592529296875, -21.005035400390625, 58.187744140625),
    POS_TO_FIXED(-40.08857727050781, -21.005050659179688, -58.18778991699219)
};

polygon model_0x0608f7b4_15_faces[] = {
    { 16, 8, -16, 0 }};

polygon_attr model_0x0608f7b4_15_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x000f, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_15 = {
    model_0x0608f7b4_15_verts,
    3,
    model_0x0608f7b4_15_faces,
    1,
    model_0x0608f7b4_15_attrs
};

vec3 model_0x0608f7b4_16_verts[] = {
    POS_TO_FIXED(-40.0, 20.0, -32.00001525878906),
    POS_TO_FIXED(58.99494934082031, -21.005035400390625, -32.00001525878906),
    POS_TO_FIXED(-9.40887451171875, -21.005020141601562, 62.14979553222656)
};

polygon model_0x0608f7b4_16_faces[] = {
    { 16, 8, -16, 0 }};

polygon_attr model_0x0608f7b4_16_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x0010, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_16 = {
    model_0x0608f7b4_16_verts,
    3,
    model_0x0608f7b4_16_faces,
    1,
    model_0x0608f7b4_16_attrs
};

vec3 model_0x0608f7b4_17_verts[] = {
    POS_TO_FIXED(-85.26235961914062, -48.0, 9.710037231445312),
    POS_TO_FIXED(-52.08860778808594, 50.99494934082031, 33.81219482421875),
    POS_TO_FIXED(71.26240539550781, -48.00001525878906, -41.14787292480469),
    POS_TO_FIXED(58.59112548828125, 50.99493408203125, -2.149810791015625)
};

polygon model_0x0608f7b4_17_faces[] = {
    { 8, 24, 16, 0 }};

polygon_attr model_0x0608f7b4_17_attrs[] = {
    { 0x00, 0x04, 0x000d, 0x10cc, 0x8000, 0x0011, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_17 = {
    model_0x0608f7b4_17_verts,
    4,
    model_0x0608f7b4_17_faces,
    1,
    model_0x0608f7b4_17_attrs
};

vec3 model_0x0608f7b4_18_verts[] = {
    POS_TO_FIXED(16.0, 20.0, -52.00001525878906),
    POS_TO_FIXED(46.591094970703125, -21.005035400390625, 42.14976501464844),
    POS_TO_FIXED(-64.08860778808594, -21.005035400390625, 6.1877593994140625)
};

polygon model_0x0608f7b4_18_faces[] = {
    { 8, 0, -8, 16 }};

polygon_attr model_0x0608f7b4_18_attrs[] = {
    { 0x00, 0x04, 0x000c, 0x10cc, 0x8000, 0x0012, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_18 = {
    model_0x0608f7b4_18_verts,
    3,
    model_0x0608f7b4_18_faces,
    1,
    model_0x0608f7b4_18_attrs
};

vec3 model_0x0608f7b4_19_verts[] = {
    POS_TO_FIXED(56.0, -48.0, 56.0),
    POS_TO_FIXED(14.994949340820312, 50.99494934082031, 55.999969482421875),
    POS_TO_FIXED(-40.737579345703125, -48.00001525878906, -77.14791870117188),
    POS_TO_FIXED(-53.40885925292969, 50.99494934082031, -38.14979553222656)
};

polygon model_0x0608f7b4_19_faces[] = {
    { 24, 8, 0, 16 }};

polygon_attr model_0x0608f7b4_19_attrs[] = {
    { 0x00, 0x04, 0x000e, 0x10cc, 0x8000, 0x0013, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_19 = {
    model_0x0608f7b4_19_verts,
    4,
    model_0x0608f7b4_19_faces,
    1,
    model_0x0608f7b4_19_attrs
};

vec3 model_0x0608f7b4_20_verts[] = {
    POS_TO_FIXED(2.743988037109375, -35.615936279296875, 4.7527313232421875),
    POS_TO_FIXED(44.60649108886719, -15.903778076171875, 55.98075866699219),
    POS_TO_FIXED(0.0, -15.791976928710938, 0.0),
    POS_TO_FIXED(28.947418212890625, -35.39161682128906, 70.97042846679688),
    POS_TO_FIXED(2.7440032958984375, -35.615936279296875, -4.7527313232421875),
    POS_TO_FIXED(26.177490234375, -15.903778076171875, -66.62074279785156),
    POS_TO_FIXED(0.0, -15.791976928710938, 0.0),
    POS_TO_FIXED(46.98847961425781, -35.39161682128906, -60.55442810058594),
    POS_TO_FIXED(-5.48797607421875, -35.615936279296875, 0.0),
    POS_TO_FIXED(-70.78399658203125, -15.903778076171875, 10.639984130859375),
    POS_TO_FIXED(0.0, -15.791976928710938, 0.0),
    POS_TO_FIXED(-75.93589782714844, -35.39161682128906, -10.416015625),
    POS_TO_FIXED(1.52587890625e-05, 3.2852935791015625, 0.0),
    POS_TO_FIXED(-19.43756103515625, -16.314697265625, 0.0),
    POS_TO_FIXED(3.0517578125e-05, -16.314697265625, 19.437576293945312),
    POS_TO_FIXED(19.437606811523438, -16.314697265625, 0.0),
    POS_TO_FIXED(1.52587890625e-05, -16.314697265625, -19.437576293945312)
};

polygon model_0x0608f7b4_20_faces[] = {
    { 0, 24, 8, 16 },
    { 32, 56, 40, 48 },
    { 64, 88, 72, 80 },
    { 96, 96, 128, 104 },
    { 96, 96, 120, 128 },
    { 96, 96, 112, 120 },
    { 96, 96, 104, 112 },
    { 120, 112, 104, 128 }};

polygon_attr model_0x0608f7b4_20_attrs[] = {
    { 0xff, 0x04, 0x000f, 0x10cc, 0x8000, 0x0014, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x04, 0x000f, 0x10cc, 0x8000, 0x0015, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x04, 0x000f, 0x10cc, 0x8000, 0x0016, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x04, 0x0010, 0x10cc, 0x8000, 0x0017, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x04, 0x0010, 0x10cc, 0x8000, 0x0018, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x04, 0x0010, 0x10cc, 0x8000, 0x0019, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x04, 0x0010, 0x10cc, 0x8000, 0x001a, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xd738, 0x001b, 0x0004, 0x0000, 0x0000 }
};

model model_0x0608f7b4_model_20 = {
    model_0x0608f7b4_20_verts,
    17,
    model_0x0608f7b4_20_faces,
    8,
    model_0x0608f7b4_20_attrs
};


char model_0x0608f7b4_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0x00, 0x02,
    0xff, 0xff, 0x00, 0x03, 0xff, 0xff, 0x00, 0x04,
    0xff, 0xff, 0x00, 0x05, 0xff, 0xff, 0x00, 0x06,
    0xff, 0xff, 0x00, 0x07, 0xff, 0xff, 0x00, 0x08,
    0xff, 0xff, 0x00, 0x09, 0xff, 0xff, 0x00, 0x0a,
    0xff, 0xff, 0x00, 0x0b, 0xff, 0xff, 0x00, 0x0c,
    0xff, 0xff, 0x00, 0x0d, 0xff, 0xff, 0x00, 0x0e,
    0xff, 0xff, 0x00, 0x0f, 0xff, 0xff, 0x00, 0x10,
    0xff, 0xff, 0x00, 0x11, 0xff, 0xff, 0x00, 0x12,
    0xff, 0xff, 0x00, 0x13, 0xff, 0xff, 0x00, 0x14,
    0xff, 0xff, 0x00, 0x15, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xfe, 0xff, 0xff,
};

// force this into .data rather than .bss
__attribute__((section(".data")))
char DAT_SONICWORLD__0608f7b0[] = {
    0, 0, 0, 0
};

model * model_0x0608f7b4_group[] = {
    &model_0x0608f7b4_model_00,
    &model_0x0608f7b4_model_01,
    &model_0x0608f7b4_model_02,
    &model_0x0608f7b4_model_03,
    &model_0x0608f7b4_model_04,
    &model_0x0608f7b4_model_05,
    &model_0x0608f7b4_model_06,
    &model_0x0608f7b4_model_07,
    &model_0x0608f7b4_model_08,
    &model_0x0608f7b4_model_09,
    &model_0x0608f7b4_model_10,
    &model_0x0608f7b4_model_11,
    &model_0x0608f7b4_model_12,
    &model_0x0608f7b4_model_13,
    &model_0x0608f7b4_model_14,
    &model_0x0608f7b4_model_15,
    &model_0x0608f7b4_model_16,
    &model_0x0608f7b4_model_17,
    &model_0x0608f7b4_model_18,
    &model_0x0608f7b4_model_19,
    &model_0x0608f7b4_model_20
};

vec3 model_0x0608f7b4_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
    POS_TO_FIXED(80.0, -56.0, 52.0),
    POS_TO_FIXED(-24.0, -56.0, 92.0),
    POS_TO_FIXED(-92.0, -56.0, 0.0),
    POS_TO_FIXED(-28.0, -56.0, -92.0),
    POS_TO_FIXED(84.0, -56.0, -56.0),
    POS_TO_FIXED(40.0, -120.0, -28.0),
    POS_TO_FIXED(40.0, -120.0, 32.0),
    POS_TO_FIXED(-16.0, -120.0, 52.0),
    POS_TO_FIXED(-12.0, -120.0, -48.00001525878906),
    POS_TO_FIXED(-40.0, -120.0, 0.0),
    POS_TO_FIXED(80.0, 56.0, 52.00001525878906),
    POS_TO_FIXED(-24.0, 56.0, 92.0),
    POS_TO_FIXED(-12.0, 120.0, -48.0),
    POS_TO_FIXED(40.0, 120.0, -28.0),
    POS_TO_FIXED(-92.0, 56.0, 0.0),
    POS_TO_FIXED(-40.0, 120.0, 0.0),
    POS_TO_FIXED(40.0, 120.0, 32.0),
    POS_TO_FIXED(-28.0, 48.0, -92.0),
    POS_TO_FIXED(-16.0, 120.0, 52.0),
    POS_TO_FIXED(84.0, 48.0, -56.0),
    /* this one has an extra unused position vector */
    POS_TO_FIXED(0, -140.0, 0)
};

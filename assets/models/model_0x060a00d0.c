#include "model_0x060a00d0.h"

vec3 model_0x060a00d0_00_verts[] = {
    POS_TO_FIXED(1024.0, -272.0, -1280.0),
    POS_TO_FIXED(1030.4000091552734, -160.0, -1408.0),
    POS_TO_FIXED(1030.4000091552734, 0.0, -1531.622299194336),
    POS_TO_FIXED(1184.0, -320.0, -1120.0),
    POS_TO_FIXED(1206.4000091552734, -160.0, -1232.0),
    POS_TO_FIXED(1206.4000091552734, 0.0, -1340.1695556640625),
    POS_TO_FIXED(1360.0, -288.0, -960.0),
    POS_TO_FIXED(1400.0, -160.0, -1056.0),
    POS_TO_FIXED(1400.0, 0.0, -1148.716796875)
};

polygon model_0x060a00d0_00_faces[] = {
    { 40, 64, 56, 32 },
    { 32, 56, 48, 24 },
    { 16, 40, 32, 8 },
    { 8, 32, 24, 0 }};

polygon_attr model_0x060a00d0_00_attrs[] = {
    { 0xff, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1cb6, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00d1, 0x10cc, 0x8000, 0x1cb7, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1cb8, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00d0, 0x10cc, 0x8000, 0x1cb9, 0x0022, 0x0000, 0x0000 }
};

model model_0x060a00d0_model_00 = {
    model_0x060a00d0_00_verts,
    9,
    model_0x060a00d0_00_faces,
    4,
    model_0x060a00d0_00_attrs
};


char model_0x060a00d0_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_0x060a00d0_group[] = {
    &model_0x060a00d0_model_00
};

__attribute__((section(".data")))
vec3 model_0x060a00d0_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

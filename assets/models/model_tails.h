#pragma once

#include "geometry.h"

extern xmodel model_tails_model_00;
extern xmodel model_tails_model_01;
extern xmodel model_tails_model_02;
extern xmodel model_tails_model_03;
extern xmodel model_tails_model_04;
extern xmodel model_tails_model_05;
extern xmodel model_tails_model_06;
extern xmodel model_tails_model_07;
extern xmodel model_tails_model_08;
extern xmodel model_tails_model_09;
extern xmodel model_tails_model_10;
extern xmodel model_tails_model_11;
extern xmodel model_tails_model_12;
extern xmodel model_tails_model_13;
extern xmodel model_tails_model_14;

extern char model_tails_data_00[];
extern xmodel * model_tails_group[];
extern vec3 model_tails_positions[];
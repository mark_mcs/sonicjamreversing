#include "model_0x0609c5c0.h"

vec3 model_0x0609c5c0_00_verts[] = {
    POS_TO_FIXED(-2144.0, -288.0, 0.0),
    POS_TO_FIXED(-2107.199935913086, -160.0, 16.0),
    POS_TO_FIXED(-2171.199935913086, 0.0, 16.0),
    POS_TO_FIXED(-1984.0, -320.0, 159.99996948242188),
    POS_TO_FIXED(-2003.199935913086, -160.0, 183.99996948242188),
    POS_TO_FIXED(-2059.519515991211, 0.0, 218.39991760253906),
    POS_TO_FIXED(-1824.0, -288.0, 319.9999694824219),
    POS_TO_FIXED(-1835.199935913086, -160.0, 351.99993896484375),
    POS_TO_FIXED(-1874.7195892333984, 0.0, 467.1998596191406),
    POS_TO_FIXED(-1664.0001068115234, -288.0, 384.00001525878906),
    POS_TO_FIXED(-1667.2000579833984, -160.0, 419.1999969482422),
    POS_TO_FIXED(-1689.919677734375, 0.0, 541.1199188232422),
    POS_TO_FIXED(-1504.0, -320.0, 399.9999694824219),
    POS_TO_FIXED(-1499.199935913086, -160.0, 435.99993896484375),
    POS_TO_FIXED(-1505.11962890625, 0.0, 559.5998382568359),
    POS_TO_FIXED(-1344.0001068115234, -288.0, 384.00001525878906),
    POS_TO_FIXED(-1331.2000579833984, -160.0, 419.1999969482422),
    POS_TO_FIXED(-1320.319808959961, 0.0, 541.1199188232422),
    POS_TO_FIXED(-1184.0, -288.0, 319.9999694824219),
    POS_TO_FIXED(-1163.199935913086, -160.0, 351.99993896484375),
    POS_TO_FIXED(-1135.5196380615234, 0.0, 467.1998596191406),
    POS_TO_FIXED(-1024.0, -320.0, 159.99996948242188),
    POS_TO_FIXED(-995.1999359130859, -160.0, 183.99996948242188),
    POS_TO_FIXED(-950.7197265625, 0.0, 218.39991760253906),
    POS_TO_FIXED(-944.0, -320.0, 0.0),
    POS_TO_FIXED(-863.1999359130859, -160.0, 16.0),
    POS_TO_FIXED(-911.1999359130859, 0.0, 16.0)
};

polygon model_0x0609c5c0_00_faces[] = {
    { 208, 184, 176, 200 },
    { 200, 176, 168, 192 },
    { 32, 8, 0, 24 },
    { 40, 16, 8, 32 },
    { 56, 32, 24, 48 },
    { 64, 40, 32, 56 },
    { 80, 56, 48, 72 },
    { 88, 64, 56, 80 },
    { 104, 80, 72, 96 },
    { 112, 88, 80, 104 },
    { 128, 104, 96, 120 },
    { 136, 112, 104, 128 },
    { 152, 128, 120, 144 },
    { 160, 136, 128, 152 },
    { 176, 152, 144, 168 },
    { 184, 160, 152, 176 }};

polygon_attr model_0x0609c5c0_00_attrs[] = {
    { 0xff, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1b44, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00d0, 0x10cc, 0x8000, 0x1b45, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00d1, 0x10cc, 0x8000, 0x1b46, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1b47, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00d0, 0x10cc, 0x8000, 0x1b48, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1b49, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00d2, 0x10cc, 0x8000, 0x1b4a, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1b4b, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00d1, 0x10cc, 0x8000, 0x1b4c, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1b4d, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00d0, 0x10cc, 0x8000, 0x1b4e, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1b4f, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00d2, 0x10cc, 0x8000, 0x1b50, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1b51, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00d1, 0x10cc, 0x8000, 0x1b52, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1b53, 0x0022, 0x0000, 0x0000 }
};

model model_0x0609c5c0_model_00 = {
    model_0x0609c5c0_00_verts,
    27,
    model_0x0609c5c0_00_faces,
    16,
    model_0x0609c5c0_00_attrs
};


char model_0x0609c5c0_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_0x0609c5c0_group[] = {
    &model_0x0609c5c0_model_00
};

__attribute__((section(".data")))
vec3 model_0x0609c5c0_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

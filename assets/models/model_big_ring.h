#pragma once

#include "geometry.h"

extern xmodel model_big_ring;

extern char model_big_ring_data_00[];
extern xmodel * model_big_ring_group[];
extern vec3 model_big_ring_positions[];
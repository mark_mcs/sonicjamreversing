#pragma once

#include "geometry.h"

extern model16 model_palmtree_model_00;
extern model16 model_palmtree_model_01;

extern char model_palmtree_data_00[];
extern model16 * model_palmtree_group[];
extern vec3 model_palmtree_positions[];
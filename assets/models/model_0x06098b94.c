#include "model_0x06098b94.h"

vec3 model_0x06098b94_00_verts[] = {
    POS_TO_FIXED(16.730987548828125, -103.31413269042969, 64.04827880859375),
    POS_TO_FIXED(-17.951171875, -103.31413269042969, 64.04827880859375),
    POS_TO_FIXED(-17.812728881835938, -0.619415283203125, 111.23115539550781),
    POS_TO_FIXED(16.800201416015625, -0.619415283203125, 111.23114013671875)
};

polygon model_0x06098b94_00_faces[] = {
    { 16, 24, 0, 8 }};

polygon_attr model_0x06098b94_00_attrs[] = {
    { 0x00, 0x16, 0x008c, 0x10cc, 0x8000, 0x0000, 0x0032, 0x0000, 0x0000 }
};

model model_0x06098b94_model_00 = {
    model_0x06098b94_00_verts,
    4,
    model_0x06098b94_00_faces,
    1,
    model_0x06098b94_00_attrs
};

vec3 model_0x06098b94_01_verts[] = {
    POS_TO_FIXED(16.730987548828125, -103.31413269042969, 64.04827880859375),
    POS_TO_FIXED(16.800201416015625, -0.619415283203125, 111.23114013671875),
    POS_TO_FIXED(51.413177490234375, -103.31413269042969, 64.04827880859375),
    POS_TO_FIXED(51.41314697265625, -0.619415283203125, 111.23114013671875)
};

polygon model_0x06098b94_01_faces[] = {
    { 8, 24, 16, 0 }};

polygon_attr model_0x06098b94_01_attrs[] = {
    { 0x00, 0x06, 0x008c, 0x10cc, 0x8000, 0x0001, 0x0022, 0x0000, 0x0000 }
};

model model_0x06098b94_model_01 = {
    model_0x06098b94_01_verts,
    4,
    model_0x06098b94_01_faces,
    1,
    model_0x06098b94_01_attrs
};


char model_0x06098b94_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0x00, 0x02,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
    0x00,
    0x00,
    0x00,
    0x00,
};
model * model_0x06098b94_group[] = {
    &model_0x06098b94_model_00,
    &model_0x06098b94_model_01
};

vec3 model_0x06098b94_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
    POS_TO_FIXED(0.0, 0.0, -200.0),
    // duplicate position
    POS_TO_FIXED(0.0, 0.0, -200.0),
};

#pragma once

#include "geometry.h"

extern model16 model_ki_model_00;
extern model16 model_ki_model_01;

extern char model_ki_data_00[];
extern model16 * model_ki_group[];
extern vec3 model_ki_positions[];
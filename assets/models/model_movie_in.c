#include "model_movie_in.h"

vec3 model_movie_in_00_verts[] = {
    POS_TO_FIXED(-85.0, 0.0, -220.0),
    POS_TO_FIXED(-58.65000915527344, 0.0, -297.40000915527344),
    POS_TO_FIXED(-85.0, -139.99996948242188, -220.0),
    POS_TO_FIXED(-58.65000915527344, -96.59994506835938, -297.40000915527344),
    POS_TO_FIXED(85.0, 0.0, -220.0),
    POS_TO_FIXED(58.65000915527344, 0.0, -297.40000915527344),
    POS_TO_FIXED(85.0, -139.99996948242188, -220.0),
    POS_TO_FIXED(58.65000915527344, -96.59994506835938, -297.40000915527344)
};

polygon model_movie_in_00_faces[] = {
    { 8, 0, 16, 24 },
    { 24, 56, 40, 8 },
    { 32, 40, 56, 48 },
    { 56, 24, 16, 48 },
    { 8, 40, 32, 0 }};

polygon_attr model_movie_in_00_attrs[] = {
    { 0x00, 0x06, 0x00ab, 0x10cc, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x16, 0x00ab, 0x10cc, 0x8000, 0x0001, 0x0012, 0x0000, 0x0000 },
    { 0x00, 0x16, 0x00ab, 0x10cc, 0x8000, 0x0002, 0x0032, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00aa, 0x10cc, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00a9, 0x10cc, 0x8000, 0x0004, 0x0022, 0x0000, 0x0000 }
};

model model_movie_in_model_00 = {
    model_movie_in_00_verts,
    8,
    model_movie_in_00_faces,
    5,
    model_movie_in_00_attrs
};


char model_movie_in_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_movie_in_group[] = {
    &model_movie_in_model_00
};

vec3 model_movie_in_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 240.0),
};
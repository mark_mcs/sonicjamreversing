#pragma once

#include "geometry.h"

//
// blue
//
extern model model_flickyb_model_00;
extern model model_flickyb_model_01;
extern model model_flickyb_model_02;
extern model model_flickyb_model_03;
extern model model_flickyb_model_04;
extern model model_flickyb_model_05;
extern model model_flickyb_model_06;
extern model model_flickyb_model_07;
extern model model_flickyb_model_08;
extern model model_flickyb_model_09;

extern char model_flickyb_data_00[];
extern model * model_flickyb_group[];
extern vec3 model_flickyb_positions[];


//
// green
//
extern model model_flickyg_model_00;
extern model model_flickyg_model_01;
extern model model_flickyg_model_02;
extern model model_flickyg_model_03;
extern model model_flickyg_model_04;
extern model model_flickyg_model_05;
extern model model_flickyg_model_06;
extern model model_flickyg_model_07;
extern model model_flickyg_model_08;
extern model model_flickyg_model_09;

extern char model_flickyg_data_00[];
extern model * model_flickyg_group[];
extern vec3 model_flickyg_positions[];


//
// purple
//
extern model model_flickyp_model_00;
extern model model_flickyp_model_01;
extern model model_flickyp_model_02;
extern model model_flickyp_model_03;
extern model model_flickyp_model_04;
extern model model_flickyp_model_05;
extern model model_flickyp_model_06;
extern model model_flickyp_model_07;
extern model model_flickyp_model_08;
extern model model_flickyp_model_09;

extern char model_flickyp_data_00[];
extern model * model_flickyp_group[];
extern vec3 model_flickyp_positions[];


//
// red
//
extern model model_flickyr_model_00;
extern model model_flickyr_model_01;
extern model model_flickyr_model_02;
extern model model_flickyr_model_03;
extern model model_flickyr_model_04;
extern model model_flickyr_model_05;
extern model model_flickyr_model_06;
extern model model_flickyr_model_07;
extern model model_flickyr_model_08;
extern model model_flickyr_model_09;

extern char model_flickyr_data_00[];
extern model * model_flickyr_group[];
extern vec3 model_flickyr_positions[];


//
// yellow
//
extern model model_flickyy_model_00;
extern model model_flickyy_model_01;
extern model model_flickyy_model_02;
extern model model_flickyy_model_03;
extern model model_flickyy_model_04;
extern model model_flickyy_model_05;
extern model model_flickyy_model_06;
extern model model_flickyy_model_07;
extern model model_flickyy_model_08;
extern model model_flickyy_model_09;

extern char model_flickyy_data_00[];
extern model * model_flickyy_group[];
extern vec3 model_flickyy_positions[];


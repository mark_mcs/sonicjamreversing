#pragma once

#include "geometry.h"

extern model16 model_monitor;
extern model16 model_monitor_rotors;
extern model16 model_monitor_spike;

extern char model_monitor_data_00[];
extern model16 * model_monitor_group[];
extern vec3 model_monitor_positions[];

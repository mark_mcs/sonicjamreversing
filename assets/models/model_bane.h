#pragma once

#include "geometry.h"

extern model model_bane_model_00;
extern model model_bane_model_01;

extern char model_bane_data_00[];
extern model * model_bane_group[];
extern vec3 model_bane_positions[];
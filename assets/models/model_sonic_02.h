#pragma once

#include "geometry.h"

extern xmodel model_sonic_02_model_00;
extern xmodel model_sonic_02_model_01;
extern xmodel model_sonic_02_model_02;
extern xmodel model_sonic_02_model_03;
extern xmodel model_sonic_02_model_04;
extern xmodel model_sonic_02_model_05;
extern xmodel model_sonic_02_model_06;
extern xmodel model_sonic_02_model_07;
extern xmodel model_sonic_02_model_08;
extern xmodel model_sonic_02_model_09;
extern xmodel model_sonic_02_model_10;
extern xmodel model_sonic_02_model_11;
extern xmodel model_sonic_02_model_12;
extern xmodel model_sonic_02_model_13;
extern xmodel model_sonic_02_model_14;

extern xmodel * model_sonic_02_group[];
extern vec3 model_sonic_02_positions[];
#include "model_flicky.h"

//
// ===================== Blue Flicky & Common ===============================
//

vec3 model_flicky_common_00_verts[] = {
    POS_TO_FIXED(-0.0659942626953125, 4.4529876708984375, -4.7419891357421875),
    POS_TO_FIXED(-0.0659942626953125, -5.093994140625, -4.7419891357421875),
    POS_TO_FIXED(-0.0659942626953125, 4.4529876708984375, 4.7419891357421875),
    POS_TO_FIXED(-0.0659942626953125, -5.093994140625, 4.7419891357421875)
};

polygon model_flicky_common_00_faces[] = {
    { 8, 24, 16, 0 }};

polygon_attr model_flickyb_00_attrs[] = {
    { 0xff, 0x06, 0x013e, 0x108c, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model model_flickyb_model_00 = {
    model_flicky_common_00_verts,
    4,
    model_flicky_common_00_faces,
    1,
    model_flickyb_00_attrs
};

vec3 model_flicky_common_01_verts[] = {
    POS_TO_FIXED(-0.0219879150390625, -0.0059967041015625, -1.875),
    POS_TO_FIXED(7.0369873046875, -0.0059967041015625, -1.875),
    POS_TO_FIXED(-0.0219879150390625, -0.0059967041015625, 0.420989990234375),
    POS_TO_FIXED(7.0369873046875, -0.0059967041015625, 0.420989990234375)
};

polygon model_flicky_common_01_faces[] = {
    { 16, 0, 8, 24 }};

polygon_attr model_flickyb_01_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0001, 0x0002, 0x0000, 0x0000 }
};

model model_flickyb_model_01 = {
    model_flicky_common_01_verts,
    4,
    model_flicky_common_01_faces,
    1,
    model_flickyb_01_attrs
};

vec3 model_flicky_common_02_verts[] = {
    POS_TO_FIXED(-2.5959930419921875, 0.042999267578125, -2.3179931640625),
    POS_TO_FIXED(8.18798828125, -0.135986328125, -2.3179931640625),
    POS_TO_FIXED(-2.5959930419921875, 0.042999267578125, 2.2389984130859375),
    POS_TO_FIXED(8.18798828125, -0.135986328125, 2.2389984130859375)
};

polygon model_flicky_common_02_faces[] = {
    { 8, 24, 16, 0 }};

polygon_attr model_flickyb_02_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0002, 0x0002, 0x0000, 0x0000 }
};

model model_flickyb_model_02 = {
    model_flicky_common_02_verts,
    4,
    model_flicky_common_02_faces,
    1,
    model_flickyb_02_attrs
};

vec3 model_flicky_common_03_verts[] = {
    POS_TO_FIXED(-0.0219879150390625, -0.0059967041015625, 1.875),
    POS_TO_FIXED(7.0369873046875, -0.0059967041015625, 1.875),
    POS_TO_FIXED(-0.0219879150390625, -0.0059967041015625, -0.420989990234375),
    POS_TO_FIXED(7.0369873046875, -0.0059967041015625, -0.420989990234375)
};

polygon model_flicky_common_03_faces[] = {
    { 24, 8, 0, 16 }};

polygon_attr model_flickyb_03_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 }
};

model model_flickyb_model_03 = {
    model_flicky_common_03_verts,
    4,
    model_flicky_common_03_faces,
    1,
    model_flickyb_03_attrs
};

vec3 model_flicky_common_04_verts[] = {
    POS_TO_FIXED(-2.5959930419921875, 0.042999267578125, 2.3179931640625),
    POS_TO_FIXED(8.18798828125, -0.135986328125, 2.3179931640625),
    POS_TO_FIXED(-2.5959930419921875, 0.042999267578125, -2.2389984130859375),
    POS_TO_FIXED(8.18798828125, -0.135986328125, -2.2389984130859375)
};

polygon model_flicky_common_04_faces[] = {
    { 24, 8, 0, 16 }};

polygon_attr model_flickyb_04_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0004, 0x0002, 0x0000, 0x0000 }
};

model model_flickyb_model_04 = {
    model_flicky_common_04_verts,
    4,
    model_flicky_common_04_faces,
    1,
    model_flickyb_04_attrs
};

vec3 model_flicky_common_05_verts[] = {
    POS_TO_FIXED(-0.092987060546875, -0.032989501953125, -2.571990966796875),
    POS_TO_FIXED(7.0149993896484375, -0.157989501953125, -2.571990966796875),
    POS_TO_FIXED(-0.092987060546875, -0.032989501953125, 2.576995849609375),
    POS_TO_FIXED(7.0149993896484375, -0.157989501953125, 2.576995849609375)
};

polygon model_flicky_common_05_faces[] = {
    { 0, 8, 24, 16 }};

polygon_attr model_flickyb_05_attrs[] = {
    { 0xff, 0x04, 0x013f, 0x108c, 0x8000, 0x0005, 0x0022, 0x0000, 0x0000 }
};

model model_flickyb_model_05 = {
    model_flicky_common_05_verts,
    4,
    model_flicky_common_05_faces,
    1,
    model_flickyb_05_attrs
};

vec3 model_flicky_common_06_verts[] = {
    POS_TO_FIXED(-0.1079864501953125, -0.162994384765625, -2.6599884033203125),
    POS_TO_FIXED(7.0439910888671875, -0.003997802734375, -4.1209869384765625),
    POS_TO_FIXED(-0.1079864501953125, -0.162994384765625, 2.6519927978515625),
    POS_TO_FIXED(7.0439910888671875, -0.003997802734375, 4.1129913330078125)
};

polygon model_flicky_common_06_faces[] = {
    { 0, 8, 24, 16 }};

polygon_attr model_flickyb_06_attrs[] = {
    { 0xff, 0x04, 0x0140, 0x108c, 0x8000, 0x0006, 0x0022, 0x0000, 0x0000 }
};

model model_flickyb_model_06 = {
    model_flicky_common_06_verts,
    4,
    model_flicky_common_06_faces,
    1,
    model_flickyb_06_attrs
};

vec3 model_flicky_common_07_verts[] = {
    POS_TO_FIXED(-0.092987060546875, -0.032989501953125, 2.571990966796875),
    POS_TO_FIXED(7.0149993896484375, -0.157989501953125, 2.571990966796875),
    POS_TO_FIXED(-0.092987060546875, -0.032989501953125, -2.576995849609375),
    POS_TO_FIXED(7.0149993896484375, -0.157989501953125, -2.576995849609375)
};

polygon model_flicky_common_07_faces[] = {
    { 16, 24, 8, 0 }};

polygon_attr model_flickyb_07_attrs[] = {
    { 0xff, 0x04, 0x013f, 0x108c, 0x8000, 0x0007, 0x0002, 0x0000, 0x0000 }
};

model model_flickyb_model_07 = {
    model_flicky_common_07_verts,
    4,
    model_flicky_common_07_faces,
    1,
    model_flickyb_07_attrs
};

vec3 model_flicky_common_08_verts[] = {
    POS_TO_FIXED(-0.1079864501953125, -0.162994384765625, 2.6599884033203125),
    POS_TO_FIXED(7.0439910888671875, -0.003997802734375, 4.1209869384765625),
    POS_TO_FIXED(-0.1079864501953125, -0.162994384765625, -2.6519927978515625),
    POS_TO_FIXED(7.0439910888671875, -0.003997802734375, -4.1129913330078125)
};

polygon model_flicky_common_08_faces[] = {
    { 16, 24, 8, 0 }};

polygon_attr model_flickyb_08_attrs[] = {
    { 0xff, 0x04, 0x0140, 0x108c, 0x8000, 0x0008, 0x0002, 0x0000, 0x0000 }
};

model model_flickyb_model_08 = {
    model_flicky_common_08_verts,
    4,
    model_flicky_common_08_faces,
    1,
    model_flickyb_08_attrs
};

vec3 model_flicky_common_09_verts[] = {
    POS_TO_FIXED(3.10699462890625, -14.865997314453125, 5.545989990234375),
    POS_TO_FIXED(3.2859954833984375, 5.13299560546875, 5.545989990234375),
    POS_TO_FIXED(-3.1939849853515625, 5.1909942626953125, 5.545989990234375),
    POS_TO_FIXED(-3.3719940185546875, -14.808990478515625, 5.545989990234375),
    POS_TO_FIXED(-6.433990478515625, 5.2199859619140625, -0.0659942626953125),
    POS_TO_FIXED(-6.61199951171875, -14.779998779296875, -0.0659942626953125),
    POS_TO_FIXED(-3.1939849853515625, 5.1909942626953125, -5.6779937744140625),
    POS_TO_FIXED(-3.3719940185546875, -14.808990478515625, -5.6779937744140625),
    POS_TO_FIXED(3.2859954833984375, 5.13299560546875, -5.6779937744140625),
    POS_TO_FIXED(3.10699462890625, -14.865997314453125, -5.6779937744140625),
    POS_TO_FIXED(6.5259857177734375, 5.1039886474609375, -0.0659942626953125),
    POS_TO_FIXED(6.34698486328125, -14.894989013671875, -0.0659942626953125),
    POS_TO_FIXED(5.8969879150390625, -4.8909912109375, -10.353988647460938),
    POS_TO_FIXED(-11.922988891601562, -4.73199462890625, -0.0659942626953125),
    POS_TO_FIXED(5.8969879150390625, -4.8909912109375, 10.222991943359375),
    POS_TO_FIXED(-5.9829864501953125, -4.7849884033203125, 10.222991943359375),
    POS_TO_FIXED(-5.9829864501953125, -4.7849884033203125, -10.353988647460938),
    POS_TO_FIXED(11.83599853515625, -4.9439849853515625, -0.0659942626953125),
    POS_TO_FIXED(5.878997802734375, 1.3589935302734375, -4.6689910888671875),
    POS_TO_FIXED(8.969985961914062, 2.040985107421875, -3.1609954833984375),
    POS_TO_FIXED(9.772994995117188, -0.9539947509765625, -0.0659942626953125),
    POS_TO_FIXED(13.544998168945312, 1.97198486328125, -0.0659942626953125),
    POS_TO_FIXED(5.878997802734375, 1.3589935302734375, 4.537994384765625),
    POS_TO_FIXED(8.969985961914062, 2.040985107421875, 3.029998779296875)
};

polygon model_flicky_common_09_faces[] = {
    { 16, 8, 80, 32 },
    { 104, 40, 24, 120 },
    { 0, 24, 40, 88 },
    { 80, 64, 48, 32 },
    { 128, 56, 40, 104 },
    { 72, 88, 40, 56 },
    { 96, 72, 56, 128 },
    { 72, 96, 136, 88 },
    { 88, 136, 112, 0 },
    { 120, 24, 0, 112 },
    { 112, 8, 16, 120 },
    { 120, 16, 32, 104 },
    { 104, 32, 48, 128 },
    { 128, 48, 64, 96 },
    { 96, 64, 80, 136 },
    { 136, 80, 8, 112 },
    { 144, 152, 168, 160 },
    { 160, 168, 184, 176 }};

polygon_attr model_flickyb_09_attrs[] = {
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x0009, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x000a, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x000b, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x000c, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x000d, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x000e, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x000f, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0141, 0x10cc, 0x8000, 0x0010, 0x0002, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0141, 0x10cc, 0x8000, 0x0011, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x0012, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x0013, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x0014, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x0015, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xe9e0, 0x0016, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0142, 0x10cc, 0x8000, 0x0017, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0142, 0x10cc, 0x8000, 0x0018, 0x0002, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x0019, 0x0004, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x001a, 0x0004, 0x0000, 0x0000 }
};

model model_flickyb_model_09 = {
    model_flicky_common_09_verts,
    24,
    model_flicky_common_09_faces,
    18,
    model_flickyb_09_attrs
};


char model_flickyb_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x03, 0x00, 0x04, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x05, 0x00, 0x06, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x07, 0x00, 0x08, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x09, 0xff, 0xff, 0xff, 0xff,
    0xff,
    0xfe,
    0xff,
    0xff,
};
model * model_flickyb_group[] = {
    &model_flickyb_model_00,
    &model_flickyb_model_01,
    &model_flickyb_model_02,
    &model_flickyb_model_03,
    &model_flickyb_model_04,
    &model_flickyb_model_05,
    &model_flickyb_model_06,
    &model_flickyb_model_07,
    &model_flickyb_model_08,
    &model_flickyb_model_09
};

vec3 model_flickyb_positions[] = {
    POS_TO_FIXED(0.0659942626953125, 0.3209991455078125, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, 2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, -2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, 4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, -4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(0.0, 9.787994384765625, 0.0),
};


//
// ===================== Green Flicky ===============================
//

polygon_attr model_flickyg_00_attrs[] = {
    { 0xff, 0x06, 0x01c8, 0x108c, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model model_flickyg_model_00 = {
    model_flicky_common_00_verts,
    4,
    model_flicky_common_00_faces,
    1,
    model_flickyg_00_attrs
};

polygon_attr model_flickyg_01_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0001, 0x0002, 0x0000, 0x0000 }
};

model model_flickyg_model_01 = {
    model_flicky_common_01_verts,
    4,
    model_flicky_common_01_faces,
    1,
    model_flickyg_01_attrs
};

polygon_attr model_flickyg_02_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0002, 0x0002, 0x0000, 0x0000 }
};

model model_flickyg_model_02 = {
    model_flicky_common_02_verts,
    4,
    model_flicky_common_02_faces,
    1,
    model_flickyg_02_attrs
};

polygon_attr model_flickyg_03_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 }
};

model model_flickyg_model_03 = {
    model_flicky_common_03_verts,
    4,
    model_flicky_common_03_faces,
    1,
    model_flickyg_03_attrs
};

polygon_attr model_flickyg_04_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0004, 0x0002, 0x0000, 0x0000 }
};

model model_flickyg_model_04 = {
    model_flicky_common_04_verts,
    4,
    model_flicky_common_04_faces,
    1,
    model_flickyg_04_attrs
};

polygon_attr model_flickyg_05_attrs[] = {
    { 0xff, 0x04, 0x01c9, 0x108c, 0x8000, 0x0005, 0x0022, 0x0000, 0x0000 }
};

model model_flickyg_model_05 = {
    model_flicky_common_05_verts,
    4,
    model_flicky_common_05_faces,
    1,
    model_flickyg_05_attrs
};

polygon_attr model_flickyg_06_attrs[] = {
    { 0xff, 0x04, 0x01ca, 0x108c, 0x8000, 0x0006, 0x0022, 0x0000, 0x0000 }
};

model model_flickyg_model_06 = {
    model_flicky_common_06_verts,
    4,
    model_flicky_common_06_faces,
    1,
    model_flickyg_06_attrs
};

polygon_attr model_flickyg_07_attrs[] = {
    { 0xff, 0x04, 0x01c9, 0x108c, 0x8000, 0x0007, 0x0002, 0x0000, 0x0000 }
};

model model_flickyg_model_07 = {
    model_flicky_common_07_verts,
    4,
    model_flicky_common_07_faces,
    1,
    model_flickyg_07_attrs
};

polygon_attr model_flickyg_08_attrs[] = {
    { 0xff, 0x04, 0x01ca, 0x108c, 0x8000, 0x0008, 0x0002, 0x0000, 0x0000 }
};

model model_flickyg_model_08 = {
    model_flicky_common_08_verts,
    4,
    model_flicky_common_08_faces,
    1,
    model_flickyg_08_attrs
};

polygon_attr model_flickyg_09_attrs[] = {
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x0009, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x000a, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x000b, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x000c, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x000d, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x000e, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x000f, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01cb, 0x10cc, 0x8000, 0x0010, 0x0002, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01cb, 0x10cc, 0x8000, 0x0011, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x0012, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x0013, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x0014, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x0015, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x0016, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01cc, 0x10cc, 0x8000, 0x0017, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01cc, 0x10cc, 0x8000, 0x0018, 0x0002, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x0019, 0x0004, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x001a, 0x0004, 0x0000, 0x0000 }
};

model model_flickyg_model_09 = {
    model_flicky_common_09_verts,
    24,
    model_flicky_common_09_faces,
    18,
    model_flickyg_09_attrs
};


char model_flickyg_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x03, 0x00, 0x04, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x05, 0x00, 0x06, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x07, 0x00, 0x08, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x09, 0xff, 0xff, 0xff, 0xff,
    0xff,
    0xfe,
    0xff,
    0xff,
};
model * model_flickyg_group[] = {
    &model_flickyg_model_00,
    &model_flickyg_model_01,
    &model_flickyg_model_02,
    &model_flickyg_model_03,
    &model_flickyg_model_04,
    &model_flickyg_model_05,
    &model_flickyg_model_06,
    &model_flickyg_model_07,
    &model_flickyg_model_08,
    &model_flickyg_model_09
};

vec3 model_flickyg_positions[] = {
    POS_TO_FIXED(0.0659942626953125, 0.3209991455078125, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, 2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, -2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, 4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, -4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(0.0, 9.787994384765625, 0.0),
};

//
// ===================== Purple Flicky ===============================
//

polygon_attr model_flickyp_00_attrs[] = {
    { 0xff, 0x06, 0x01d2, 0x108c, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model model_flickyp_model_00 = {
    model_flicky_common_00_verts,
    4,
    model_flicky_common_00_faces,
    1,
    model_flickyp_00_attrs
};

polygon_attr model_flickyp_01_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0001, 0x0002, 0x0000, 0x0000 }
};

model model_flickyp_model_01 = {
    model_flicky_common_01_verts,
    4,
    model_flicky_common_01_faces,
    1,
    model_flickyp_01_attrs
};

polygon_attr model_flickyp_02_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0002, 0x0002, 0x0000, 0x0000 }
};

model model_flickyp_model_02 = {
    model_flicky_common_02_verts,
    4,
    model_flicky_common_02_faces,
    1,
    model_flickyp_02_attrs
};

polygon_attr model_flickyp_03_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 }
};

model model_flickyp_model_03 = {
    model_flicky_common_03_verts,
    4,
    model_flicky_common_03_faces,
    1,
    model_flickyp_03_attrs
};

polygon_attr model_flickyp_04_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0004, 0x0002, 0x0000, 0x0000 }
};

model model_flickyp_model_04 = {
    model_flicky_common_04_verts,
    4,
    model_flicky_common_04_faces,
    1,
    model_flickyp_04_attrs
};

polygon_attr model_flickyp_05_attrs[] = {
    { 0xff, 0x04, 0x01d3, 0x108c, 0x8000, 0x0005, 0x0022, 0x0000, 0x0000 }
};

model model_flickyp_model_05 = {
    model_flicky_common_05_verts,
    4,
    model_flicky_common_05_faces,
    1,
    model_flickyp_05_attrs
};

polygon_attr model_flickyp_06_attrs[] = {
    { 0xff, 0x04, 0x01d4, 0x108c, 0x8000, 0x0006, 0x0022, 0x0000, 0x0000 }
};

model model_flickyp_model_06 = {
    model_flicky_common_06_verts,
    4,
    model_flicky_common_06_faces,
    1,
    model_flickyp_06_attrs
};

polygon_attr model_flickyp_07_attrs[] = {
    { 0xff, 0x04, 0x01d3, 0x108c, 0x8000, 0x0007, 0x0002, 0x0000, 0x0000 }
};

model model_flickyp_model_07 = {
    model_flicky_common_07_verts,
    4,
    model_flicky_common_07_faces,
    1,
    model_flickyp_07_attrs
};

polygon_attr model_flickyp_08_attrs[] = {
    { 0xff, 0x04, 0x01d4, 0x108c, 0x8000, 0x0008, 0x0002, 0x0000, 0x0000 }
};

model model_flickyp_model_08 = {
    model_flicky_common_08_verts,
    4,
    model_flicky_common_08_faces,
    1,
    model_flickyp_08_attrs
};

polygon_attr model_flickyp_09_attrs[] = {
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x0009, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x000a, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x000b, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x000c, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x000d, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x000e, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x000f, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01d5, 0x10cc, 0x8000, 0x0010, 0x0002, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01d5, 0x10cc, 0x8000, 0x0011, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x0012, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x0013, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x0014, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x0015, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xc44f, 0x0016, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01d6, 0x10cc, 0x8000, 0x0017, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01d6, 0x10cc, 0x8000, 0x0018, 0x0002, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x0019, 0x0004, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x001a, 0x0004, 0x0000, 0x0000 }
};

model model_flickyp_model_09 = {
    model_flicky_common_09_verts,
    24,
    model_flicky_common_09_faces,
    18,
    model_flickyp_09_attrs
};


char model_flickyp_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x03, 0x00, 0x04, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x05, 0x00, 0x06, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x07, 0x00, 0x08, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x09, 0xff, 0xff, 0xff, 0xff,
    0xff,
    0xfe,
    0xff,
    0xff,
};

model * model_flickyp_group[] = {
    &model_flickyp_model_00,
    &model_flickyp_model_01,
    &model_flickyp_model_02,
    &model_flickyp_model_03,
    &model_flickyp_model_04,
    &model_flickyp_model_05,
    &model_flickyp_model_06,
    &model_flickyp_model_07,
    &model_flickyp_model_08,
    &model_flickyp_model_09
};

vec3 model_flickyp_positions[] = {
    POS_TO_FIXED(0.0659942626953125, 0.3209991455078125, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, 2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, -2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, 4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, -4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(0.0, 9.787994384765625, 0.0),
};


//
// ===================== Red Flicky ===============================
//

polygon_attr model_flickyr_00_attrs[] = {
    { 0xff, 0x06, 0x01d7, 0x108c, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model model_flickyr_model_00 = {
    model_flicky_common_00_verts,
    4,
    model_flicky_common_00_faces,
    1,
    model_flickyr_00_attrs
};

polygon_attr model_flickyr_01_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0001, 0x0002, 0x0000, 0x0000 }
};

model model_flickyr_model_01 = {
    model_flicky_common_01_verts,
    4,
    model_flicky_common_01_faces,
    1,
    model_flickyr_01_attrs
};

polygon_attr model_flickyr_02_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0002, 0x0002, 0x0000, 0x0000 }
};

model model_flickyr_model_02 = {
    model_flicky_common_02_verts,
    4,
    model_flicky_common_02_faces,
    1,
    model_flickyr_02_attrs
};

polygon_attr model_flickyr_03_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 }
};

model model_flickyr_model_03 = {
    model_flicky_common_03_verts,
    4,
    model_flicky_common_03_faces,
    1,
    model_flickyr_03_attrs
};

polygon_attr model_flickyr_04_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0004, 0x0002, 0x0000, 0x0000 }
};

model model_flickyr_model_04 = {
    model_flicky_common_04_verts,
    4,
    model_flicky_common_04_faces,
    1,
    model_flickyr_04_attrs
};

polygon_attr model_flickyr_05_attrs[] = {
    { 0xff, 0x04, 0x01d8, 0x108c, 0x8000, 0x0005, 0x0022, 0x0000, 0x0000 }
};

model model_flickyr_model_05 = {
    model_flicky_common_05_verts,
    4,
    model_flicky_common_05_faces,
    1,
    model_flickyr_05_attrs
};

polygon_attr model_flickyr_06_attrs[] = {
    { 0xff, 0x04, 0x01d9, 0x108c, 0x8000, 0x0006, 0x0022, 0x0000, 0x0000 }
};

model model_flickyr_model_06 = {
    model_flicky_common_06_verts,
    4,
    model_flicky_common_06_faces,
    1,
    model_flickyr_06_attrs
};

polygon_attr model_flickyr_07_attrs[] = {
    { 0xff, 0x04, 0x01d8, 0x108c, 0x8000, 0x0007, 0x0002, 0x0000, 0x0000 }
};

model model_flickyr_model_07 = {
    model_flicky_common_07_verts,
    4,
    model_flicky_common_07_faces,
    1,
    model_flickyr_07_attrs
};

polygon_attr model_flickyr_08_attrs[] = {
    { 0xff, 0x04, 0x01d9, 0x108c, 0x8000, 0x0008, 0x0002, 0x0000, 0x0000 }
};

model model_flickyr_model_08 = {
    model_flicky_common_08_verts,
    4,
    model_flicky_common_08_faces,
    1,
    model_flickyr_08_attrs
};

polygon_attr model_flickyr_09_attrs[] = {
    { 0x00, 0x04, 0x0000, 0x00ec, 0x9283, 0x0009, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x000a, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x000b, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x000c, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x000d, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x000e, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x000f, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01da, 0x10cc, 0x8000, 0x0010, 0x0002, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01da, 0x10cc, 0x8000, 0x0011, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x0012, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x0013, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x0014, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x0015, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x94b7, 0x0016, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01db, 0x10cc, 0x8000, 0x0017, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01db, 0x10cc, 0x8000, 0x0018, 0x0002, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x0019, 0x0004, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x001a, 0x0004, 0x0000, 0x0000 }
};

model model_flickyr_model_09 = {
    model_flicky_common_09_verts,
    24,
    model_flicky_common_09_faces,
    18,
    model_flickyr_09_attrs
};


char model_flickyr_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x03, 0x00, 0x04, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x05, 0x00, 0x06, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x07, 0x00, 0x08, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x09, 0xff, 0xff, 0xff, 0xff,
    0xff,
    0xfe,
    0xff,
    0xff,
};
model * model_flickyr_group[] = {
    &model_flickyr_model_00,
    &model_flickyr_model_01,
    &model_flickyr_model_02,
    &model_flickyr_model_03,
    &model_flickyr_model_04,
    &model_flickyr_model_05,
    &model_flickyr_model_06,
    &model_flickyr_model_07,
    &model_flickyr_model_08,
    &model_flickyr_model_09
};

vec3 model_flickyr_positions[] = {
    POS_TO_FIXED(0.0659942626953125, 0.3209991455078125, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, 2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, -2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, 4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, -4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(0.0, 9.787994384765625, 0.0),
};

//
// ===================== Yellow Flicky ===============================
//

polygon_attr model_flickyy_00_attrs[] = {
    { 0xff, 0x06, 0x01cd, 0x108c, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model model_flickyy_model_00 = {
    model_flicky_common_00_verts,
    4,
    model_flicky_common_00_faces,
    1,
    model_flickyy_00_attrs
};

polygon_attr model_flickyy_01_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0001, 0x0002, 0x0000, 0x0000 }
};

model model_flickyy_model_01 = {
    model_flicky_common_01_verts,
    4,
    model_flicky_common_01_faces,
    1,
    model_flickyy_01_attrs
};

polygon_attr model_flickyy_02_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0002, 0x0002, 0x0000, 0x0000 }
};

model model_flickyy_model_02 = {
    model_flicky_common_02_verts,
    4,
    model_flicky_common_02_faces,
    1,
    model_flickyy_02_attrs
};

polygon_attr model_flickyy_03_attrs[] = {
    { 0xff, 0x04, 0x013c, 0x108c, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 }
};

model model_flickyy_model_03 = {
    model_flicky_common_03_verts,
    4,
    model_flicky_common_03_faces,
    1,
    model_flickyy_03_attrs
};

polygon_attr model_flickyy_04_attrs[] = {
    { 0xff, 0x06, 0x013d, 0x108c, 0x8000, 0x0004, 0x0002, 0x0000, 0x0000 }
};

model model_flickyy_model_04 = {
    model_flicky_common_04_verts,
    4,
    model_flicky_common_04_faces,
    1,
    model_flickyy_04_attrs
};

polygon_attr model_flickyy_05_attrs[] = {
    { 0xff, 0x04, 0x01ce, 0x108c, 0x8000, 0x0005, 0x0022, 0x0000, 0x0000 }
};

model model_flickyy_model_05 = {
    model_flicky_common_05_verts,
    4,
    model_flicky_common_05_faces,
    1,
    model_flickyy_05_attrs
};

polygon_attr model_flickyy_06_attrs[] = {
    { 0xff, 0x04, 0x01cf, 0x108c, 0x8000, 0x0006, 0x0022, 0x0000, 0x0000 }
};

model model_flickyy_model_06 = {
    model_flicky_common_06_verts,
    4,
    model_flicky_common_06_faces,
    1,
    model_flickyy_06_attrs
};

polygon_attr model_flickyy_07_attrs[] = {
    { 0xff, 0x04, 0x01ce, 0x108c, 0x8000, 0x0007, 0x0002, 0x0000, 0x0000 }
};

model model_flickyy_model_07 = {
    model_flicky_common_07_verts,
    4,
    model_flicky_common_07_faces,
    1,
    model_flickyy_07_attrs
};

polygon_attr model_flickyy_08_attrs[] = {
    { 0xff, 0x04, 0x01cf, 0x108c, 0x8000, 0x0008, 0x0002, 0x0000, 0x0000 }
};

model model_flickyy_model_08 = {
    model_flicky_common_08_verts,
    4,
    model_flicky_common_08_faces,
    1,
    model_flickyy_08_attrs
};

polygon_attr model_flickyy_09_attrs[] = {
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x0009, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x000a, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x000b, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x000c, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x000d, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x000e, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x000f, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01d0, 0x10cc, 0x8000, 0x0010, 0x0002, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01d0, 0x10cc, 0x8000, 0x0011, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x0012, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x0013, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x0014, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x0015, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0x81bb, 0x0016, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01d1, 0x10cc, 0x8000, 0x0017, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x01d1, 0x10cc, 0x8000, 0x0018, 0x0002, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x0019, 0x0004, 0x0000, 0x0000 },
    { 0xff, 0x05, 0x0000, 0x00ec, 0x83ff, 0x001a, 0x0004, 0x0000, 0x0000 }
};

model model_flickyy_model_09 = {
    model_flicky_common_09_verts,
    24,
    model_flicky_common_09_faces,
    18,
    model_flickyy_09_attrs
};


char model_flickyy_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x03, 0x00, 0x04, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x05, 0x00, 0x06, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x07, 0x00, 0x08, 0xff, 0xff,
    0xff, 0xff, 0x00, 0x09, 0xff, 0xff, 0xff, 0xff,
    0xff,
    0xfe,
    0xff,
    0xff,
};
model * model_flickyy_group[] = {
    &model_flickyy_model_00,
    &model_flickyy_model_01,
    &model_flickyy_model_02,
    &model_flickyy_model_03,
    &model_flickyy_model_04,
    &model_flickyy_model_05,
    &model_flickyy_model_06,
    &model_flickyy_model_07,
    &model_flickyy_model_08,
    &model_flickyy_model_09
};

vec3 model_flickyy_positions[] = {
    POS_TO_FIXED(0.0659942626953125, 0.3209991455078125, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, 2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.058990478515625, -5.1609954833984375, -2.4239959716796875),
    POS_TO_FIXED(7.066986083984375, -0.0059967041015625, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, 4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(-0.0309906005859375, 4.4409942626953125, -4.9059906005859375),
    POS_TO_FIXED(7.157989501953125, 0.0, 0.0),
    POS_TO_FIXED(0.0, 9.787994384765625, 0.0),
};

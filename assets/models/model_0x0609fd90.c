#include "model_0x0609fd90.h"

vec3 model_0x0609fd90_00_verts[] = {
    POS_TO_FIXED(1024.0, -272.0, 576.0),
    POS_TO_FIXED(1030.4000091552734, -160.0, 569.6000366210938),
    POS_TO_FIXED(1030.4000091552734, 0.0, 689.2300262451172),
    POS_TO_FIXED(1184.0, -320.0, 480.0),
    POS_TO_FIXED(1206.4000091552734, -160.0, 464.0),
    POS_TO_FIXED(1206.4000091552734, 0.0, 574.3583984375),
    POS_TO_FIXED(1328.0, -288.0, 320.0),
    POS_TO_FIXED(1364.800048828125, -160.0, 352.0),
    POS_TO_FIXED(1364.800048828125, 0.0, 352.0),
    POS_TO_FIXED(1344.0, -288.0, 160.0),
    POS_TO_FIXED(1382.4000091552734, -160.0, 176.0),
    POS_TO_FIXED(1382.4000091552734, 0.0, 176.0),
    POS_TO_FIXED(1328.0, -288.0, 0.0),
    POS_TO_FIXED(1364.800048828125, -160.0, 0.0),
    POS_TO_FIXED(1364.800048828125, 0.0, 0.0)
};

polygon model_0x0609fd90_00_faces[] = {
    { 112, 88, 80, 104 },
    { 104, 80, 72, 96 },
    { 88, 64, 56, 80 },
    { 80, 56, 48, 72 },
    { 64, 40, 32, 56 },
    { 56, 32, 24, 48 },
    { 40, 16, 8, 32 },
    { 32, 8, 0, 24 }};

polygon_attr model_0x0609fd90_00_attrs[] = {
    { 0x00, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1ca2, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00d0, 0x10cc, 0x8000, 0x1ca3, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1ca4, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00d1, 0x10cc, 0x8000, 0x1ca5, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1ca6, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x00d2, 0x10cc, 0x8000, 0x1ca7, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00cf, 0x10cc, 0x8000, 0x1ca8, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00d0, 0x10cc, 0x8000, 0x1ca9, 0x0022, 0x0000, 0x0000 }
};

model model_0x0609fd90_model_00 = {
    model_0x0609fd90_00_verts,
    15,
    model_0x0609fd90_00_faces,
    8,
    model_0x0609fd90_00_attrs
};


char model_0x0609fd90_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_0x0609fd90_group[] = {
    &model_0x0609fd90_model_00
};

__attribute__((section(".data")))
vec3 model_0x0609fd90_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

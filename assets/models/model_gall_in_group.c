#include "model_gall_in_group.h"

vec3 model_gall_in_group_00_verts[] = {
    POS_TO_FIXED(-87.11495971679688, 0.0072479248046875, 308.42002868652344),
    POS_TO_FIXED(-87.11495971679688, 0.0072479248046875, 125.02012634277344),
    POS_TO_FIXED(-87.11495971679688, -143.66265869140625, 308.42002868652344),
    POS_TO_FIXED(-87.11495971679688, -143.66265869140625, 125.02012634277344),
    POS_TO_FIXED(87.11495971679688, 0.0072479248046875, 308.42002868652344),
    POS_TO_FIXED(87.11495971679688, 0.0072479248046875, 125.02012634277344),
    POS_TO_FIXED(87.11495971679688, -143.66265869140625, 308.42002868652344),
    POS_TO_FIXED(87.11495971679688, -143.66265869140625, 125.02012634277344)
};

polygon model_gall_in_group_00_faces[] = {
    { 8, 0, 16, 24 },
    { 56, 24, 16, 48 },
    { 32, 40, 56, 48 },
    { 0, 32, 48, 16 },
    { 8, 40, 32, 0 }};

polygon_attr model_gall_in_group_00_attrs[] = {
    { 0x00, 0x06, 0x004b, 0x10cc, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0042, 0x10cc, 0x8000, 0x0001, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x004b, 0x10cc, 0x8000, 0x0002, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x004b, 0x10cc, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0045, 0x10cc, 0x8000, 0x0004, 0x0022, 0x0000, 0x0000 }
};

model model_gall_in_group_model_00 = {
    model_gall_in_group_00_verts,
    8,
    model_gall_in_group_00_faces,
    5,
    model_gall_in_group_00_attrs
};


char model_gall_in_group_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_gall_in_group_group[] = {
    &model_gall_in_group_model_00
};

vec3 model_gall_in_group_positions[] = {
    POS_TO_FIXED(0.0, 0.0, -176.0),
};
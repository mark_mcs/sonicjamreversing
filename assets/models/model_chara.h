#pragma once

#include "geometry.h"

extern model model_chara_model_00;
extern model model_chara_model_01;
extern model model_chara_model_02;
extern model model_chara_model_03;
extern model model_chara_model_04;

extern char model_chara_data_00[];
extern model * model_chara_group[];
extern vec3 model_chara_positions[];
#pragma once

#include "geometry.h"

extern model model_mission_platform_model_00;
extern model model_mission_platform_model_01;

extern char model_mission_platform_data_00[];
extern model * model_mission_platform_group[];
extern vec3 model_mission_platform_positions[];
#pragma once

#include "geometry.h"

extern model model_history_model_00;
extern model model_history_model_01;
extern model model_history_model_02;

extern char model_history_data_00[];
extern model * model_history_group[];
extern vec3 model_history_positions[];
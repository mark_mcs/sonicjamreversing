#pragma once

#include "geometry.h"

extern model model_gallery_model_00;
extern model model_gallery_model_01;
extern model model_gallery_model_02;
extern model model_gallery_model_03;
extern model model_gallery_model_04;

extern char model_gallery_data_00[];
extern model * model_gallery_group[];
extern vec3 model_gallery_positions[];
#include "model_ring_shadow.h"

static vec3s vertices[] = {
    { 0xfff0, 0x0000, 0x0010 },
    { 0xfff0, 0x0000, 0xfff0 },
    { 0x0010, 0x0000, 0x0010 },
    { 0x0010, 0x0000, 0xfff0 }
};

static polygon face[] = {
    { 8, 24, 16, 0 }};

static polygon_attr attrs1[] = {
    { 0x00, 0x05, 0x00fd, 0x1188, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

static polygon_attr attrs2[] = {
    { 0x00, 0x06, 0x00fd, 0x1188, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model16 model_ring_shadow = {
    vertices,
    4,
    face,
    1,
    attrs1
};

model16 model_ring_shadow2 = {
    vertices,
    4,
    face,
    1,
    attrs2
};


#include "model_0x0608ff3c.h"

vec3 model_0x0608ff3c_00_verts[] = {
    POS_TO_FIXED(0.0, 0.0, -64.0),
    POS_TO_FIXED(-79.99998474121094, 0.0, -64.0),
    POS_TO_FIXED(0.0, -168.0, -64.0),
    POS_TO_FIXED(-79.99998474121094, -168.0, -64.0)
};

polygon model_0x0608ff3c_00_faces[] = {
    { 0, 8, 24, 16 }};

polygon_attr model_0x0608ff3c_00_attrs[] = {
    { 0x00, 0x04, 0x002c, 0x10cc, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608ff3c_model_00 = {
    model_0x0608ff3c_00_verts,
    4,
    model_0x0608ff3c_00_faces,
    1,
    model_0x0608ff3c_00_attrs
};

vec3 model_0x0608ff3c_01_verts[] = {
    POS_TO_FIXED(80.0, 0.0, -63.99998474121094),
    POS_TO_FIXED(0.0, 0.0, -64.0),
    POS_TO_FIXED(80.0, -168.0, -63.99998474121094),
    POS_TO_FIXED(0.0, -168.0, -64.0)
};

polygon model_0x0608ff3c_01_faces[] = {
    { 0, 8, 24, 16 }};

polygon_attr model_0x0608ff3c_01_attrs[] = {
    { 0x00, 0x04, 0x002c, 0x10cc, 0x8000, 0x0001, 0x0022, 0x0000, 0x0000 }
};

model model_0x0608ff3c_model_01 = {
    model_0x0608ff3c_01_verts,
    4,
    model_0x0608ff3c_01_faces,
    1,
    model_0x0608ff3c_01_attrs
};


char model_0x0608ff3c_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0x00, 0x02,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
    0x00,
    0x00,
    0x00,
    0x00,
};
model * model_0x0608ff3c_group[] = {
    &model_0x0608ff3c_model_00,
    &model_0x0608ff3c_model_01
};

__attribute__((section(".data")))
vec3 model_0x0608ff3c_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
    POS_TO_FIXED(0.0, 0.0, 0.0),
    /* extra unused position */
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

#pragma once

#include "geometry.h"

extern xmodel model_sonic_00_model_00;
extern xmodel model_sonic_00_model_01;
extern xmodel model_sonic_00_model_02;
extern xmodel model_sonic_00_model_03;
extern xmodel model_sonic_00_model_04;
extern xmodel model_sonic_00_model_05;
extern xmodel model_sonic_00_model_06;
extern xmodel model_sonic_00_model_07;
extern xmodel model_sonic_00_model_08;
extern xmodel model_sonic_00_model_09;
extern xmodel model_sonic_00_model_10;
extern xmodel model_sonic_00_model_11;
extern xmodel model_sonic_00_model_12;
extern xmodel model_sonic_00_model_13;
extern xmodel model_sonic_00_model_14;

extern xmodel * model_sonic_00_group[];
extern vec3 model_sonic_00_positions[];
#pragma once

#include "geometry.h"

extern model model_sound_model_00;
extern model model_sound_model_01;
extern model model_sound_model_02;

extern char model_sound_data_00[];
extern model * model_sound_group[];
extern vec3 model_sound_positions[];
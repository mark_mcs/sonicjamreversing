#include "model_0x060a454c.h"

vec3 model_0x060a454c_00_verts[] = {
    POS_TO_FIXED(320.0, 8.0, 1968.0),
    POS_TO_FIXED(481.09922790527344, 8.0, 1968.0),
    POS_TO_FIXED(281.09922790527344, 8.0, 2080.0),
    POS_TO_FIXED(320.0, 8.0, 2192.0),
    POS_TO_FIXED(393.09922790527344, 8.0, 2192.0),
    POS_TO_FIXED(393.09922790527344, 8.0, 2080.0),
    POS_TO_FIXED(281.09922790527344, 8.0, 2080.0),
    POS_TO_FIXED(392.5496063232422, 8.0, 1968.0),
    POS_TO_FIXED(480.0, 8.0, 2192.0),
    POS_TO_FIXED(505.09922790527344, 8.0, 2080.0),
    POS_TO_FIXED(505.09922790527344, 8.0, 2080.0)
};

polygon model_0x060a454c_00_faces[] = {
    { 56, 8, 72, 40 },
    { 8, 80, -88, 72 },
    { 0, 56, 40, 48 },
    { 48, 40, 32, 24 },
    { 16, 48, -56, 24 },
    { 80, 64, -72, 72 },
    { 40, 72, 64, 32 },
    { 48, 16, -24, 0 }};

polygon_attr model_0x060a454c_00_attrs[] = {
    { 0x00, 0x06, 0x0035, 0x10cc, 0x8000, 0x1ef4, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x83e0, 0x1ef5, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0031, 0x10cc, 0x8000, 0x1ef6, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x002a, 0x10cc, 0x8000, 0x1ef7, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x83e0, 0x1ef8, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x83e0, 0x1ef9, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0038, 0x10cc, 0x8000, 0x1efa, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x83e0, 0x1efb, 0x0004, 0x0000, 0x0000 }
};

model model_0x060a454c_model_00 = {
    model_0x060a454c_00_verts,
    11,
    model_0x060a454c_00_faces,
    8,
    model_0x060a454c_00_attrs
};


char model_0x060a454c_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_0x060a454c_group[] = {
    &model_0x060a454c_model_00
};

__attribute__((section(".data")))
vec3 model_0x060a454c_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

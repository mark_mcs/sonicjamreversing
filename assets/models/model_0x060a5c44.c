#include "model_0x060a5c44.h"

vec3 model_0x060a5c44_00_verts[] = {
    POS_TO_FIXED(-1784.0, 79.99993896484375, -3004.0),
    POS_TO_FIXED(-1808.0, 0.0, -2968.0),
    POS_TO_FIXED(-1760.0, 160.0, -3040.0),
    POS_TO_FIXED(-1600.0, 160.0, -3040.0),
    POS_TO_FIXED(-1600.0, 79.99993896484375, -4196.0),
    POS_TO_FIXED(-1640.0, 79.99993896484375, -4000.0),
    POS_TO_FIXED(-1679.9998779296875, 0.0, -4000.0),
    POS_TO_FIXED(-1600.0, 0.0, -4232.0),
    POS_TO_FIXED(-1600.0, 160.0, -4160.0),
    POS_TO_FIXED(-1600.0, 160.0, -4000.0),
    POS_TO_FIXED(-1440.0, 160.0, -3040.0),
    POS_TO_FIXED(-1280.0, 160.0, -3040.0),
    POS_TO_FIXED(-1399.9998779296875, 79.99993896484375, -4000.0),
    POS_TO_FIXED(-1440.0, 79.99993896484375, -4196.0),
    POS_TO_FIXED(-1440.0, 0.0, -4232.0),
    POS_TO_FIXED(-1359.9998779296875, 0.0001068115234375, -4000.0),
    POS_TO_FIXED(-1440.0, 160.00001525878906, -4000.0),
    POS_TO_FIXED(-1440.0, 160.00001525878906, -4160.0),
    POS_TO_FIXED(-1280.0, 79.99993896484375, -3084.0),
    POS_TO_FIXED(-1280.0, 0.0, -3128.0),
    POS_TO_FIXED(-1808.0, 79.99993896484375, -3188.0),
    POS_TO_FIXED(-1856.0, 0.0, -3176.0),
    POS_TO_FIXED(-1760.0, 160.0, -3200.0),
    POS_TO_FIXED(-1556.0, 79.99993896484375, -3840.0),
    POS_TO_FIXED(-1512.0, 0.0, -3840.0),
    POS_TO_FIXED(-1600.0, 160.0, -3840.0),
    POS_TO_FIXED(-1560.0, 79.99993896484375, -3679.999755859375),
    POS_TO_FIXED(-1520.0, 4.57763671875e-05, -3679.999755859375),
    POS_TO_FIXED(-1600.0, 160.00001525878906, -3680.0),
    POS_TO_FIXED(-1760.0, 160.0, -3840.0),
    POS_TO_FIXED(-1800.0, 79.99993896484375, -3840.0),
    POS_TO_FIXED(-1840.0, 0.0, -3840.0),
    POS_TO_FIXED(-1432.0, 79.99993896484375, -3086.0),
    POS_TO_FIXED(-1552.0, 79.99993896484375, -3208.0),
    POS_TO_FIXED(-1504.0, 0.0, -3216.0),
    POS_TO_FIXED(-1424.0, 0.0, -3132.0),
    POS_TO_FIXED(-1600.0, 160.0, -3200.0),
    POS_TO_FIXED(-1760.0, 160.0, -3680.0),
    POS_TO_FIXED(-1792.5713958740234, 79.99993896484375, -3680.0),
    POS_TO_FIXED(-1812.0, 79.99993896484375, -3520.0),
    POS_TO_FIXED(-1864.0, 0.0, -3520.0),
    POS_TO_FIXED(-1825.142807006836, 0.0, -3680.0),
    POS_TO_FIXED(-1760.0, 160.0, -3520.0),
    POS_TO_FIXED(-1532.0, 79.99993896484375, -3520.0),
    POS_TO_FIXED(-1480.0, 1.52587890625e-05, -3520.0),
    POS_TO_FIXED(-1584.0, 160.0, -3520.0),
    POS_TO_FIXED(-1552.0, 79.99993896484375, -3360.0),
    POS_TO_FIXED(-1504.0, 0.0, -3360.0),
    POS_TO_FIXED(-1600.0, 160.0, -3360.0),
    POS_TO_FIXED(-1784.0, 160.0, -3360.0),
    POS_TO_FIXED(-1836.0, 79.99993896484375, -3360.0),
    POS_TO_FIXED(-1888.0, 0.0, -3360.0)
};

polygon model_0x060a5c44_00_faces[] = {
    { 88, 80, 256, 144 },
    { 144, 256, 280, 152 },
    { 392, 176, 160, 400 },
    { 400, 160, 168, 408 },
    { 384, 288, 176, 392 },
    { 336, 392, 400, 312 },
    { 312, 400, 408, 320 },
    { 288, 384, 368, 264 },
    { 264, 368, 376, 272 },
    { 360, 384, 392, 336 },
    { 384, 360, 344, 368 },
    { 368, 344, 352, 376 },
    { 224, 360, 336, 296 },
    { 360, 224, 208, 344 },
    { 344, 208, 216, 352 },
    { 232, 296, 304, 240 },
    { 240, 304, 328, 248 },
    { 288, 24, 16, 176 },
    { 296, 336, 312, 304 },
    { 304, 312, 320, 328 },
    { 200, 224, 296, 232 },
    { 288, 80, 24, 288 },
    { 80, 288, 264, 256 },
    { 256, 264, 272, 280 },
    { 72, 232, 240, 40 },
    { 40, 240, 248, 48 },
    { 128, 200, 232, 72 },
    { 224, 200, 184, 208 },
    { 208, 184, 192, 216 },
    { 200, 128, 96, 184 },
    { 184, 96, 120, 192 },
    { 176, 16, 0, 160 },
    { 160, 0, 8, 168 },
    { 136, 64, 32, 104 },
    { 104, 32, 56, 112 },
    { 136, 128, 72, 64 },
    { 128, 136, 104, 96 },
    { 96, 104, 112, 120 },
    { 64, 72, 40, 32 },
    { 32, 40, 48, 56 }};

polygon_attr model_0x060a5c44_00_attrs[] = {
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d72, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00fe, 0x10cc, 0x8000, 0x1d73, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d74, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00fe, 0x10cc, 0x8000, 0x1d75, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0101, 0x10cc, 0x8000, 0x1d76, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d77, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0100, 0x10cc, 0x8000, 0x1d78, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d79, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0100, 0x10cc, 0x8000, 0x1d7a, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0101, 0x10cc, 0x8000, 0x1d7b, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d7c, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0102, 0x10cc, 0x8000, 0x1d7d, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0101, 0x10cc, 0x8000, 0x1d7e, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d7f, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0100, 0x10cc, 0x8000, 0x1d80, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d81, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00fe, 0x10cc, 0x8000, 0x1d82, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0101, 0x10cc, 0x8000, 0x1d83, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d84, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0102, 0x10cc, 0x8000, 0x1d85, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0101, 0x10cc, 0x8000, 0x1d86, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0101, 0x10cc, 0x8000, 0x1d87, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d88, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0102, 0x10cc, 0x8000, 0x1d89, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d8a, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0100, 0x10cc, 0x8000, 0x1d8b, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0101, 0x10cc, 0x8000, 0x1d8c, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d8d, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00fe, 0x10cc, 0x8000, 0x1d8e, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d8f, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0102, 0x10cc, 0x8000, 0x1d90, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d91, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00fe, 0x10cc, 0x8000, 0x1d92, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d93, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0103, 0x10cc, 0x8000, 0x1d94, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0101, 0x10cc, 0x8000, 0x1d95, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d96, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0103, 0x10cc, 0x8000, 0x1d97, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00ff, 0x10cc, 0x8000, 0x1d98, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0103, 0x10cc, 0x8000, 0x1d99, 0x0022, 0x0000, 0x0000 }
};

model model_0x060a5c44_model_00 = {
    model_0x060a5c44_00_verts,
    52,
    model_0x060a5c44_00_faces,
    40,
    model_0x060a5c44_00_attrs
};


char model_0x060a5c44_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_0x060a5c44_group[] = {
    &model_0x060a5c44_model_00
};

__attribute__((section(".data")))
vec3 model_0x060a5c44_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

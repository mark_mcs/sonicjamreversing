#pragma once

#include "geometry.h"

extern model model_0x0608f7b4_model_00;
extern model model_0x0608f7b4_model_01;
extern model model_0x0608f7b4_model_02;
extern model model_0x0608f7b4_model_03;
extern model model_0x0608f7b4_model_04;
extern model model_0x0608f7b4_model_05;
extern model model_0x0608f7b4_model_06;
extern model model_0x0608f7b4_model_07;
extern model model_0x0608f7b4_model_08;
extern model model_0x0608f7b4_model_09;
extern model model_0x0608f7b4_model_10;
extern model model_0x0608f7b4_model_11;
extern model model_0x0608f7b4_model_12;
extern model model_0x0608f7b4_model_13;
extern model model_0x0608f7b4_model_14;
extern model model_0x0608f7b4_model_15;
extern model model_0x0608f7b4_model_16;
extern model model_0x0608f7b4_model_17;
extern model model_0x0608f7b4_model_18;
extern model model_0x0608f7b4_model_19;
extern model model_0x0608f7b4_model_20;

extern char model_0x0608f7b4_data_00[];
extern char DAT_SONICWORLD__0608f7b0[];
extern model * model_0x0608f7b4_group[];
extern vec3 model_0x0608f7b4_positions[];

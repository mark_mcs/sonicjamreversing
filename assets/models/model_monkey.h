#pragma once

#include "geometry.h"

extern model model_monkey_model_00;
extern model model_monkey_model_01;
extern model model_monkey_model_02;
extern model model_monkey_model_03;
extern model model_monkey_model_04;
extern model model_monkey_model_05;
extern model model_monkey_model_06;
extern model model_monkey_model_07;
extern model model_monkey_model_08;
extern model model_monkey_model_09;

extern char model_monkey_data_00[];
extern model * model_monkey_group[];
extern vec3 model_monkey_positions[];
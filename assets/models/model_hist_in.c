#include "model_hist_in.h"

vec3 model_hist_in_00_verts[] = {
    POS_TO_FIXED(-91.69996643066406, 0.0072479248046875, 246.13079833984375),
    POS_TO_FIXED(-91.69996643066406, 0.0072479248046875, 108.82810974121094),
    POS_TO_FIXED(-91.69996643066406, -120.10398864746094, 246.13079833984375),
    POS_TO_FIXED(-91.69996643066406, -120.10398864746094, 108.82810974121094),
    POS_TO_FIXED(91.69996643066406, 0.0072479248046875, 246.13079833984375),
    POS_TO_FIXED(91.69996643066406, 0.0072479248046875, 108.82810974121094),
    POS_TO_FIXED(91.69996643066406, -120.10398864746094, 246.13079833984375),
    POS_TO_FIXED(91.69996643066406, -120.10398864746094, 108.82810974121094)
};

polygon model_hist_in_00_faces[] = {
    { 8, 0, 16, 24 },
    { 56, 24, 16, 48 },
    { 32, 40, 56, 48 },
    { 0, 32, 48, 16 },
    { 8, 40, 32, 0 }};

polygon_attr model_hist_in_00_attrs[] = {
    { 0x00, 0x06, 0x0066, 0x10cc, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x005f, 0x10cc, 0x8000, 0x0001, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0066, 0x10cc, 0x8000, 0x0002, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0066, 0x10cc, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x00b5, 0x10cc, 0x8000, 0x0004, 0x0002, 0x0000, 0x0000 }
};

model model_hist_in_model_00 = {
    model_hist_in_00_verts,
    8,
    model_hist_in_00_faces,
    5,
    model_hist_in_00_attrs
};


char model_hist_in_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_hist_in_group[] = {
    &model_hist_in_model_00
};

vec3 model_hist_in_positions[] = {
    POS_TO_FIXED(0.0, 0.0, -184.0),
};
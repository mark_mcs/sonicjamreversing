#include "model_0x060a4fb4.h"

vec3 model_0x060a4fb4_00_verts[] = {
    POS_TO_FIXED(1024.0, -272.0, 576.0),
    POS_TO_FIXED(1024.0, -320.0, 480.0),
    POS_TO_FIXED(1024.0, -347.1999053955078, 320.0),
    POS_TO_FIXED(1024.0, -347.1999053955078, 160.0),
    POS_TO_FIXED(1024.0, -347.1999053955078, 0.0),
    POS_TO_FIXED(1184.0, -320.0, 480.0),
    POS_TO_FIXED(1184.0, -320.0, 320.0),
    POS_TO_FIXED(1184.0, -320.0, 160.0),
    POS_TO_FIXED(1184.0, -320.0, 0.0),
    POS_TO_FIXED(1328.0, -288.0, 320.0),
    POS_TO_FIXED(1344.0, -288.0, 160.0),
    POS_TO_FIXED(1328.0, -288.0, 0.0)
};

polygon model_0x060a4fb4_00_faces[] = {
    { 40, 40, 0, 8 },
    { 48, 40, 8, 16 },
    { 56, 48, 16, 24 },
    { 64, 56, 24, 32 },
    { 72, 72, 40, 48 },
    { 80, 72, 48, 56 },
    { 88, 80, 56, 64 }};

polygon_attr model_0x060a4fb4_00_attrs[] = {
    { 0xff, 0x06, 0x0035, 0x10cc, 0x8000, 0x1f49, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x0031, 0x10cc, 0x8000, 0x1f4a, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x015e, 0x10cc, 0x8000, 0x1f4b, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x015d, 0x10cc, 0x8000, 0x1f4c, 0x0022, 0x0000, 0x0000 },
    { 0xff, 0x06, 0x002a, 0x10cc, 0x8000, 0x1f4d, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x015c, 0x10cc, 0x8000, 0x1f4e, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x015a, 0x10cc, 0x8000, 0x1f4f, 0x0022, 0x0000, 0x0000 }
};

model model_0x060a4fb4_model_00 = {
    model_0x060a4fb4_00_verts,
    12,
    model_0x060a4fb4_00_faces,
    7,
    model_0x060a4fb4_00_attrs
};


char model_0x060a4fb4_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_0x060a4fb4_group[] = {
    &model_0x060a4fb4_model_00
};

__attribute__((section(".data")))
vec3 model_0x060a4fb4_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

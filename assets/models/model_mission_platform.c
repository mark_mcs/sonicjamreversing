#include "model_mission_platform.h"

vec3 model_mission_platform_00_verts[] = {
    POS_TO_FIXED(43.265777587890625, -39.99993896484375, -74.93844604492188),
    POS_TO_FIXED(43.265777587890625, -17.14776611328125, -74.93844604492188),
    POS_TO_FIXED(86.53147888183594, -17.14776611328125, 0.0),
    POS_TO_FIXED(86.53147888183594, -39.99993896484375, 0.0),
    POS_TO_FIXED(43.265716552734375, -17.14776611328125, 74.93846130371094),
    POS_TO_FIXED(43.265716552734375, -39.99993896484375, 74.93846130371094),
    POS_TO_FIXED(-43.2657470703125, -17.14776611328125, 74.93846130371094),
    POS_TO_FIXED(-43.2657470703125, -39.99993896484375, 74.93846130371094),
    POS_TO_FIXED(-86.53147888183594, -17.14776611328125, -1.52587890625e-05),
    POS_TO_FIXED(-86.53147888183594, -39.99993896484375, -1.52587890625e-05),
    POS_TO_FIXED(-43.265716552734375, -17.14776611328125, -74.9384765625),
    POS_TO_FIXED(-43.265716552734375, -39.99993896484375, -74.9384765625),
    POS_TO_FIXED(0.0, -39.99993896484375, 0.0),
    POS_TO_FIXED(53.649505615234375, -39.99993896484375, 0.0),
    POS_TO_FIXED(26.82476806640625, -39.99993896484375, -46.46183776855469),
    POS_TO_FIXED(-26.824737548828125, -39.99993896484375, -46.46183776855469),
    POS_TO_FIXED(-53.649505615234375, -39.99993896484375, -1.52587890625e-05),
    POS_TO_FIXED(-26.824752807617188, -39.99993896484375, 46.46183776855469),
    POS_TO_FIXED(26.824737548828125, -39.99993896484375, 46.46183776855469)
};

polygon model_mission_platform_00_faces[] = {
    { 56, 72, 128, 136 },
    { 32, 40, 24, 16 },
    { 96, 128, -136, 120 },
    { 48, 56, 40, 32 },
    { 64, 72, 56, 48 },
    { 80, 88, 72, 64 },
    { 8, 0, 88, 80 },
    { 16, 24, 0, 8 },
    { 96, 104, -112, 144 },
    { 0, 24, 104, 112 },
    { 40, 56, 136, 144 },
    { 88, 0, 112, 120 },
    { 96, 120, -128, 112 },
    { 96, 112, -120, 104 },
    { 24, 40, 144, 104 },
    { 96, 144, -152, 136 },
    { 96, 136, -144, 128 },
    { 72, 88, 120, 128 }};

polygon_attr model_mission_platform_00_attrs[] = {
    { 0x00, 0x06, 0x0000, 0x00ec, 0x94bf, 0x0000, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x943f, 0x0001, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x0002, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x0003, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x94bf, 0x0004, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x0005, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x94bf, 0x0006, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x0007, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x94bf, 0x0008, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x0009, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x000a, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x94bf, 0x000b, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x94bf, 0x000c, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x000d, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x94bf, 0x000e, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x000f, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0x94bf, 0x0010, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x0000, 0x00ec, 0xffff, 0x0011, 0x0004, 0x0000, 0x0000 }
};

model model_mission_platform_model_00 = {
    model_mission_platform_00_verts,
    19,
    model_mission_platform_00_faces,
    18,
    model_mission_platform_00_attrs
};

vec3 model_mission_platform_01_verts[] = {
    POS_TO_FIXED(90.5592041015625, 0.0160064697265625, 46.00373840332031),
    POS_TO_FIXED(46.84698486328125, 0.0160064697265625, -88.92912292480469),
    POS_TO_FIXED(-46.84698486328125, 0.0160064697265625, 88.92912292480469),
    POS_TO_FIXED(-90.5592041015625, 0.0160064697265625, -46.00373840332031),
    POS_TO_FIXED(21.856094360351562, 0.0160064697265625, 67.46644592285156),
    POS_TO_FIXED(-68.70309448242188, 0.0160064697265625, 21.462692260742188),
    POS_TO_FIXED(68.70309448242188, 0.0160064697265625, -21.462692260742188),
    POS_TO_FIXED(-21.856094360351562, 0.0160064697265625, -67.46644592285156),
    POS_TO_FIXED(0.0, 0.0160064697265625, 0.0)
};

polygon model_mission_platform_01_faces[] = {
    { 56, 8, 48, 64 },
    { 40, 64, 32, 16 },
    { 64, 48, 0, 32 },
    { 24, 56, 64, 40 }};

polygon_attr model_mission_platform_01_attrs[] = {
    { 0x00, 0x16, 0x00b6, 0x108c, 0x8000, 0x0012, 0x0032, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00b7, 0x108c, 0x8000, 0x0013, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x16, 0x00b7, 0x108c, 0x8000, 0x0014, 0x0032, 0x0000, 0x0000 },
    { 0x00, 0x06, 0x00b6, 0x108c, 0x8000, 0x0015, 0x0022, 0x0000, 0x0000 }
};

model model_mission_platform_model_01 = {
    model_mission_platform_01_verts,
    9,
    model_mission_platform_01_faces,
    4,
    model_mission_platform_01_attrs
};


char model_mission_platform_data_00[] = {
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff,
    0xff,
    0xfe,
    0xff,
    0xff,
};
model * model_mission_platform_group[] = {
    &model_mission_platform_model_00,
    &model_mission_platform_model_01
};

__attribute__((section(".data")))
vec3 model_mission_platform_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

#include "model_card.h"

vec3 model_card_00_verts[] = {
    POS_TO_FIXED(-14.195999145507812, 0.0, 4.3218994140625),
    POS_TO_FIXED(-9.782485961914062, 0.0, -4.3218994140625),
    POS_TO_FIXED(-14.195999145507812, -14.566543579101562, 4.3218994140625),
    POS_TO_FIXED(-9.782485961914062, -14.566543579101562, -4.3218994140625),
    POS_TO_FIXED(14.315353393554688, 0.0, 4.3218994140625),
    POS_TO_FIXED(9.891098022460938, 0.0, -4.3218994140625),
    POS_TO_FIXED(14.315353393554688, -14.566543579101562, 4.3218994140625),
    POS_TO_FIXED(9.891098022460938, -14.566543579101562, -4.3218994140625),
    POS_TO_FIXED(-14.195999145507812, -40.24537658691406, 4.3218994140625),
    POS_TO_FIXED(-9.782485961914062, -40.24537658691406, -4.3218994140625),
    POS_TO_FIXED(14.315353393554688, -40.24537658691406, 4.3218994140625),
    POS_TO_FIXED(9.891098022460938, -40.24537658691406, -4.3218994140625),
    POS_TO_FIXED(-16.660186767578125, 0.0, 4.3218994140625),
    POS_TO_FIXED(-12.335693359375, 0.0, -2.23394775390625),
    POS_TO_FIXED(-16.660186767578125, -13.453567504882812, 4.3218994140625),
    POS_TO_FIXED(-12.335693359375, -13.453567504882812, -2.23394775390625),
    POS_TO_FIXED(16.767669677734375, 0.0, 4.3218994140625),
    POS_TO_FIXED(12.4334716796875, 0.0, -2.23394775390625),
    POS_TO_FIXED(16.767669677734375, -13.453567504882812, 4.3218994140625),
    POS_TO_FIXED(12.4334716796875, -13.453567504882812, -2.23394775390625)
};

polygon model_card_00_faces[] = {
    { 96, 104, 120, 112 },
    { 8, 40, 56, 24 },
    { 136, 128, 144, 152 },
    { 16, 48, 32, 0 },
    { 40, 8, 0, 32 },
    { 72, 88, 80, 64 },
    { 16, 24, 72, 64 },
    { 72, 24, 56, 88 },
    { 56, 48, 80, 88 },
    { 16, 64, 80, 48 },
    { 16, 112, 120, 24 },
    { 112, 16, 0, 96 },
    { 8, 104, 96, 0 },
    { 104, 8, 24, 120 },
    { 40, 136, 152, 56 },
    { 56, 152, 144, 48 },
    { 32, 128, 136, 40 },
    { 48, 144, 128, 32 }};

polygon_attr model_card_00_attrs[] = {
    { 0x00, 0x14, 0x0017, 0x10cc, 0x8000, 0x0000, 0x0032, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0018, 0x10cc, 0x8000, 0x0001, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0017, 0x10cc, 0x8000, 0x0002, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0019, 0x10cc, 0x8000, 0x0003, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xffdb, 0x0004, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xffdb, 0x0005, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x14, 0x001a, 0x10cc, 0x8000, 0x0006, 0x0032, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x001b, 0x10cc, 0x8000, 0x0007, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x001a, 0x10cc, 0x8000, 0x0008, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xffdb, 0x0009, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xffdb, 0x000a, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x14, 0x0019, 0x10cc, 0x8000, 0x000b, 0x0032, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xffdb, 0x000c, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x14, 0x001c, 0x10cc, 0x8000, 0x000d, 0x0032, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x001c, 0x10cc, 0x8000, 0x000e, 0x0022, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xffdb, 0x000f, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0000, 0x00ec, 0xffdb, 0x0010, 0x0004, 0x0000, 0x0000 },
    { 0x00, 0x04, 0x0019, 0x10cc, 0x8000, 0x0011, 0x0022, 0x0000, 0x0000 }
};

model model_card_model_00 = {
    model_card_00_verts,
    20,
    model_card_00_faces,
    18,
    model_card_00_attrs
};


char model_card_data_00[] = {
    0x00, 0x00, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xff,
};
model * model_card_group[] = {
    &model_card_model_00
};

__attribute__((section(".data")))
vec3 model_card_positions[] = {
    POS_TO_FIXED(0.0, 0.0, 0.0),
};

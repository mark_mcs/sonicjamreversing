#pragma once

#include "geometry.h"

extern model model_movie_model_00;
extern model model_movie_model_01;
extern model model_movie_model_02;

extern char model_movie_data_00[];
extern model * model_movie_group[];
extern vec3 model_movie_positions[];
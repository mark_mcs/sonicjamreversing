#include "model_tails_billboard.h"

vec3s model_tails_billboard_00_verts[] = {
    { 0xffe8, 0x0000, 0x0018 },
    { 0xffe8, 0x0000, 0xffe8 },
    { 0x0018, 0x0000, 0x0018 },
    { 0x0018, 0x0000, 0xffe8 }
};

polygon model_tails_billboard_00_faces[] = {
    { 8, 24, 16, 0 }};

polygon_attr model_tails_billboard_00_attrs[] = {
    { 0xff, 0x04, 0x00fd, 0x1188, 0x8000, 0x0000, 0x0022, 0x0000, 0x0000 }
};

model16 model_tails_billboard = {
    model_tails_billboard_00_verts,
    4,
    model_tails_billboard_00_faces,
    1,
    model_tails_billboard_00_attrs
};


@echo off

set BUILD_DIR=%~dp0

docker run --rm -it -v %BUILD_DIR%:/build markmcs/saturn:sonicjam-build-v1.0 %*
